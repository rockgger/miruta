<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\TeamController;



Route::middleware('keyValidate')->name('api.v1.')->prefix('v1')->group(function () {

        //listado de tipos de oportunidades
        Route::get('/home/resources', [HomeController::class, 'home']);

        //listado de equipo de trabajo
        Route::get('/home/teams/rutan', [TeamController::class, 'teams']);

        //listado áreas de equipo de trabajo
        Route::get('/home/teams/areas', [TeamController::class, 'areas']);

        
    
});

Route::get('/v1/home/demo', [HomeController::class, 'demo']);