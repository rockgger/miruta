<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\OpportunityController;


Route::middleware('keyValidate')->name('api.v1.')->prefix('v1')->group(function () {

        //listado de tipos de oportunidades
        Route::get('opportunities/type', [OpportunityController::class, 'opportunitiesType']);
    
        //listado de oportunidades
        Route::get('opportunities/', [OpportunityController::class, 'opportunities']);

        //lista de oportunidades según el tipo
        Route::get('opportunities/{type_id}', [OpportunityController::class, 'opportunityList']);

        //oportunidad detalles
        Route::get('opportunity/{type_id}/{opportunity_id}', [OpportunityController::class, 'opportunity']);

        //listado de oportunidade u offertas asociadas a la oport. multioffer
        Route::get('opportunity/{type_id}/{opportunity_id}/offers', [OpportunityController::class, 'opportunityOffers']);

        //listado de noticias asociadas a la oport.
        Route::get('opportunity/{type_id}/{opportunity_id}/news', [OpportunityController::class, 'opportunityNews']);
    
    
    
});