<?php

use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;




Route::middleware('keyValidate')->name('api.v1.')->prefix('v1')->group(function () {

    //
    Route::post('/auth/login', [AuthController::class, 'authenticate']);
    //
    Route::post('/auth/logout', [AuthController::class, 'logout']);

    //
    Route::post('/auth/user', [AuthController::class, 'getAuthenticatedUser'])->middleware('jwt.verify');
        
    
});