<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\NoticiaController;


Route::middleware('keyValidate')->name('api.v1.')->prefix('v1')->group(function () {
    
    //listado de noticias
    Route::get('/news', [NoticiaController::class, 'noticias']);

    //noticias detalles
    Route::get('/news/{new_id}', [NoticiaController::class, 'noticia']);

    
});