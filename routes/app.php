<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Aplication\AppController;

/**
 * 
 * RUTAS APLICACIÓN USUARIO
 * 
 */



Route::name('app.')->middleware('isUser')->prefix('app')->group(function () {

    //vista inicial
    Route::get('/', [AppController::class, 'home'])->name('home');

    //vista mi ruta
    Route::get('/mi-rutan', [AppController::class, 'myrutan'])->name('myrutan');

    //vista mi perfil
    Route::get('/mi-perfil', [AppController::class, 'myProfile'])->name('myprofile');
});