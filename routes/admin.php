<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\LabelController;
use App\Http\Controllers\Admin\UsersConctroller;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\NoticiasConctroller;
use App\Http\Controllers\Admin\OpportunityController;

/**
 * 
 * RUTAS ADMIN
 * 
 */



Route::name('admin.')->middleware('isAdmin')->prefix('admin')->group(function () {
    
    //LOG DE EVENTOS
    Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    //DASHBOARD
    Route::get('/', [DashboardController::class, 'home'])->name('home');



    //ETIQUETAS
    Route::get('/etiquetas', [LabelController::class, 'index'])->name('label.index');

    Route::get('/etiquetas/crear', [LabelController::class, 'create'])->name('label.create');

    //edita perfil
    Route::get('/etiquetas/editar-perfiles', [LabelController::class, 'editProfile'])->name('label.profile.edit');

    //editar grupo de etiquetas
    Route::get('/etiquetas/editar/{tag_group_id}', [LabelController::class, 'editGroup'])->name('label.edit');




    //EQUIPO DE TRABAJO
    Route::get('/equipo-de-trabajo', [TeamController::class, 'index'])->name('team.index');

    

    //OPORTUNIDADES ESTÁNDAR
    Route::get('/oportunidades/estandar', [OpportunityController::class, 'standard'])->name('opportunity.standard');
    Route::get('/oportunidades/estandar/crear', [OpportunityController::class, 'standardCreate'])->name('opportunity.standard.create');
    Route::get('/oportunidades/estandar/editar/{opportunity_id}', [OpportunityController::class, 'standardEdit'])->name('opportunity.standard.edit');

    //OPORTUNIDAD LANDING
    Route::get('/oportunidades/landing', [OpportunityController::class, 'landing'])->name('opportunity.landing');
    Route::get('/oportunidades/landing/crear', [OpportunityController::class, 'landingCreate'])->name('opportunity.landing.create');
    Route::get('/oportunidades/landing/editar/{opportunity_id}', [OpportunityController::class, 'landingEdit'])->name('opportunity.landing.edit');

    //OPORTUNIDAD CONTRATACIÓN
    Route::get('/oportunidades/contratacion', [OpportunityController::class, 'hiring'])->name('opportunity.hiring');
    Route::get('/oportunidades/contratacion/crear', [OpportunityController::class, 'hiringCreate'])->name('opportunity.hiring.create');
    Route::get('/oportunidades/contratacion/editar/{opportunity_id}', [OpportunityController::class, 'hiringEdit'])->name('opportunity.hiring.edit');

    //OPORTUNIDAD MULTIOFERTA
    Route::get('/oportunidades/multi-oferta', [OpportunityController::class, 'multioffer'])->name('opportunity.multioffer');
    Route::get('/oportunidades/multi-oferta/crear', [OpportunityController::class, 'multiofferCreate'])->name('opportunity.multioffer.create');
    Route::get('/oportunidades/multi-oferta/editar/{opportunity_id}', [OpportunityController::class, 'multiofferEdit'])->name('opportunity.multioffer.edit');


    //HOME EDIT
    Route::get('/editar-home', [HomeController::class, 'homeEdit'])->name('home.edit');


    //NOTICIAS
    Route::get('/noticias', [NoticiasConctroller::class, 'noticias'])->name('news');
    Route::get('/noticias/crear', [NoticiasConctroller::class, 'create'])->name('news.create');
    Route::get('/noticias/editar/{noticia_id}', [NoticiasConctroller::class, 'edit'])->name('news.edit');

    //USUARIOS
    Route::get('/usuarios', [UsersConctroller::class, 'users'])->name('users');
});