<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

/**
 * 
 * RUTAS DE AUTENTICACIÓN
 * 
 */

Route::name('auth.')->group(function () {

    //vista login
    Route::get('/ingresar', [LoginController::class, 'login'])->name('login');

    //vista registro
    Route::get('/registrate', [RegisterController::class, 'register'])->name('register');

    //logout
    Route::get('/logout', function (Request $request) {

        if (Auth::check()) {
            Auth::logout();
            
        }
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        session()->flush();
        $request->session()->flush();
        return redirect()->route('auth.login');

    })->name('logout');


    //url recuperar contraseña
    Route::get('/recuperar-contrasena/{token}', [LoginController::class, 'recoverPassword'])->name('recover.password');

});