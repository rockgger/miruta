<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('auth.login');
});




Route::get('/router-api', function(){
    return response()->json([
        "listado de tipos de oportunidades"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/opportunities/type",
            "description"=>"Devuelve listado de tipos de oportunidades",
            "parameters"=>null
        ],
        "listado de oportunidades"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/opportunities",
            "description"=>"Devuelve listado de oportunidades",
            "parameters"=>null,
            "querys"=>[
                '?per_page'=>'Integer, número de resultados a mostrar por página.',
                '?order'=>'String, orden de resultados (asc, desc)',
                '?page'=>'Integer, paginación de resultados (pag actual a mostrar)'
            ]
        ],
        "lista de oportunidades según el tipo"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/opportunities/{type_id}",
            "description"=>"Devuelve lista de oportunidades según el tipo",
            "parameters"=>[
                'type_id' => 'Integer, id del tipo de oportunidades a listar.'
            ],
            "querys"=>[
                '?per_page'=>'Integer, número de resultados a mostrar por página.',
                '?order'=>'String, orden de resultados (asc, desc)',
                '?page'=>'Integer, paginación de resultados (pag actual a mostrar)'
            ]
        ],
        "Detalles de la oportunidad"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/opportunity/{type_id}/{opportunity_id}",
            "description"=>"Devuelve Detalles de la oportunidad",
            "parameters"=>[
                'type_id' => 'Integer, id del tipo de oportunidad.',
                'opportunity_id' => 'Integer, id de la oportunidad a buscar.',
            ]
        ],
        "listado de oportunidade u offertas asociadas a la oport. multioffer"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/opportunity/{type_id}/{opportunity_id}/offers",
            "description"=>"Devuelve listado de oportunidade u offertas asociadas a la oport. multioffer",
            "parameters"=>[
                'type_id' => 'Integer, id del tipo de oportunidad.',
                'opportunity_id' => 'Integer, id de la oportunidad a buscar.',
            ],
            "querys"=>[
                '?per_page'=>'Integer, número de resultados a mostrar por página.',
                '?page'=>'Integer, paginación de resultados (pag actual a mostrar)'
            ]
        ],
        "listado de noticias asociadas a una oportunidad"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/opportunity/{type_id}/{opportunity_id}/news",
            "description"=>"Devuelve listado de noticias asociadas a una oportunidad",
            "parameters"=>[
                'type_id' => 'Integer, id del tipo de oportunidad.',
                'opportunity_id' => 'Integer, id de la oportunidad a buscar.',
            ],
            "querys"=>[
                '?per_page'=>'Integer, número de resultados a mostrar por página.',
                '?page'=>'Integer, paginación de resultados (pag actual a mostrar)'
            ]
        ],
        "Lista de recursos del Home"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/home/resources",
            "description"=>"Devuelve un listado de recursos para usar en el home landing",
            "parameters"=>null
        ],
        "listado del equipo de rutan"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/home/teams/rutan",
            "description"=>"Devuelve listado del equipo de rutan",
            "parameters"=>null,
            "querys"=>[
                '?per_page'=>'Integer, número de resultados a mostrar por página.',
                '?page'=>'Integer, paginación de resultados (pag actual a mostrar)',
                '?areas'=>'Array, filtra los miembros del equipo por el id del área al que pertenece. Ej: ?areas=[1,2,3]'
            ]
        ],
        "Lista de Áreas del equipo de trabajo ruta n"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/home/teams/areas",
            "description"=>"Devuelve un listado de Áreas del equipo de trabajo ruta n",
            "parameters"=>null
        ],
        "listado de noticias"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/news",
            "description"=>"Devuelve listado de noticias activas",
            "parameters"=>null,
            "querys"=>[
                '?per_page'=>'Integer, número de resultados a mostrar por página.',
                '?order'=>'String, orden de resultados (asc, desc)',
                '?page'=>'Integer, paginación de resultados (pag actual a mostrar)'
            ]
        ],
        "Detalles de la noticia"=>[
            "method"=>"GET",
            "type"=>"JSON",
            "url"=>url("/")."/api/v1/news/{new_id}",
            "description"=>"Devuelve el Detalles de una noticia",
            "parameters"=>[
                'new_id' => 'Integer, id de la noticia a buscar.',
            ]
        ]
        
    ]);
});

include('auth.php');

include('app.php');

include('admin.php');
