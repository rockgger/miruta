<?php

namespace App\View\Components\Admin\Modal;

use Illuminate\View\Component;

class CreateLabel extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $idlabel;
    public function __construct($idlabel)
    {
        $this->idlabel = $idlabel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */

    
    public function render()
    {
        return view('components.admin.modal.create-label');
    }
}
