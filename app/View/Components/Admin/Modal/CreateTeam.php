<?php

namespace App\View\Components\Admin\Modal;

use Illuminate\View\Component;

class CreateTeam extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $idmodal, $photo, $phototemp;
    public function __construct($idmodal, $photo, $phototemp = null)
    {
        $this->idmodal = $idmodal;
        $this->photo = $photo;
        $this->phototemp = $phototemp;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.modal.create-team');
    }
}
