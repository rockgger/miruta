<?php

namespace App\View\Components\Aplication\Modal;

use Illuminate\View\Component;

class Avatar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $idmodal;
    public function __construct($idmodal)
    {
        $this->idmodal = $idmodal;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.aplication.modal.avatar');
    }
}
