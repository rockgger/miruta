<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Team extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'teams';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';


    public function area()
    {
        $lg = app()->getLocale();
        return $this->belongsTo(Label::class, 'label_id')
        ->select('id', 'name_'.$lg.' as name');
    }

}
