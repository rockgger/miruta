<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Opportunity extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'opportunities';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';

    public function author()
    {   
        return $this->belongsTo(User::class, 'user_id')
        ->select('id', 'name', 'last_name');
    }

    public function status()
    {
        $lg = app()->getLocale();
        return $this->belongsTo(Status::class, 'status_id')
        ->select('id', 'name_'.$lg.' as name');
    }

    public function openstatus()
    {
        $lg = app()->getLocale();
        return $this->belongsTo(Status::class, 'open_status_id')
        ->select('id', 'name_'.$lg.' as name');
    }

    public function type()
    {
        $lg = app()->getLocale();
        return $this->belongsTo(TypeOpportunity::class, 'type_id')
        ->select('id', 'name_'.$lg.' as name');
    }

    public function standar()
    {
        return $this->hasOne(OpportunityStandard::class, 'opportunity_id');
    }

    public function landing()
    {
        return $this->hasOne(OpportunityLanding::class, 'opportunity_id');
    }

    public function hiring()
    {
        return $this->hasOne(OpportunityHiring::class, 'opportunity_id');
    }

    public function multioffer()
    {
        return $this->hasOne(OpportunityMultioffer::class, 'opportunity_id');
    }

    public function labels(){
        return $this->belongsToMany('App\Models\Label', 'opportunity_labels', 'opportunity_id', 'label_id');
    }

    public function actors(){
        return $this->belongsToMany('App\Models\Label', 'opportunity_labels', 'opportunity_id', 'label_id')
        ->where('type', 'actors');
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Label', 'opportunity_labels', 'opportunity_id', 'label_id');
    }

    public function targets(){
        return $this->belongsToMany('App\Models\Label', 'opportunity_labels', 'opportunity_id', 'label_id');
    }

    public function area(){
        return $this->belongsToMany('App\Models\Label', 'opportunity_labels', 'opportunity_id', 'label_id');
    }

    public function partners(){
        return $this->belongsToMany('App\Models\Label', 'opportunity_labels', 'opportunity_id', 'label_id')
        ->where('type', 'partners');
    }


    public function associated_noticias(){
        return $this->belongsToMany('App\Models\Noticia', 'opportunity_noticias', 'opportunity_id', 'noticia_id');
    }
    
    
}
