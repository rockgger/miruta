<?php

namespace App\Models;

use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
	    'email',
        'password',
        'name',
        'last_name',
        'status_id',
        'role_id',
	];

    protected $hidden = [
        'password', 'session', 'token', 'ip','habeas_data_at', 'treatment_data_at', 'email_verified_at',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::of($value)->ucfirst()->trim();
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = Str::of($value)->ucfirst()->trim();
    }


    public function user_detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id');
    }

    public function status()
    {   
        $lg = app()->getLocale();
        return $this->belongsTo(Status::class, 'status_id')
        ->select('id', 'name_'.$lg.' as name');
    }

    public function role()
    {   
        $lg = app()->getLocale();
        return $this->belongsTo(Role::class, 'role_id')
        ->select('id', 'name_'.$lg.' as name');
    }


    public function services()
    {   
        $lg = app()->getLocale();
        return $this->belongsToMany('App\Models\Services', 'user_services', 'user_id', 'service_id')
        ->select(
            'services.service_id', 
            'services.name',
            'services.image',
            'services.description_'.$lg.' as description',
            'services.uid',
            'user_services.created_at'
        )->orderBy('user_services.created_at','desc');
    }


    //Nombre completo
    public function fullName(){        
        return $this->name.' '.$this->last_name;       
    }

    //valida si es rol usuario
    public function is_user(){
        if($this->role_id == 1){
            return true;
        }
        return false;
    }
    //valida si es rol super admin
    public function is_sAdmin(){
        if($this->role_id == 2){
            return true;
        }
        return false;
    }

    //valida si es rol admin eventos
    public function is_admin(){
        if($this->role_id == 3){
            return true;
        }
        return false;
    }

    //valida si es rol soporte
    public function is_support(){
        if($this->role_id == 4){
            return true;
        }
        return false;
    }

    //valida si es rol contenido
    public function is_content(){
        if($this->role_id == 5){
            return true;
        }
        return false;
    }

    public function profile()
    {   
        $lg = app()->getLocale();
        return $this->belongsToMany('App\Models\Profile', 'user_details', 'user_id', 'profile_id')
        ->select(
            'profiles.id', 
            'profiles.name_'.$lg.' as name',
            'profiles.created_at'
        );
    }

    public function labels(){
        
        return $this->belongsToMany('App\Models\Label', 'user_labels', 'user_id', 'label_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
