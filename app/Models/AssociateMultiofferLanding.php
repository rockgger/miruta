<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssociateMultiofferLanding extends Model
{
    use HasFactory;
    protected $table = 'associate_multioffer_landings';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = 'id';

    
}
