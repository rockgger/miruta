<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OpportunityStandard extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'opportunity_standards';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';

    
}
