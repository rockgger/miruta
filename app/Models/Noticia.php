<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Noticia extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'noticias';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';

    public function author()
    {   
        return $this->belongsTo(User::class, 'user_id')
        ->select('id', 'name', 'last_name');
    }

    public function status()
    {
        $lg = app()->getLocale();
        return $this->belongsTo(Status::class, 'status_id')
        ->select('id', 'name_'.$lg.' as name');
    }

    public function labels(){
        return $this->belongsToMany('App\Models\Label', 'noticia_labels', 'new_id', 'label_id');
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Label', 'noticia_labels', 'new_id', 'label_id');
    }

    public function targets(){
        return $this->belongsToMany('App\Models\Label', 'noticia_labels', 'new_id', 'label_id');
    }

    public function area(){
        return $this->belongsToMany('App\Models\Label', 'noticia_labels', 'new_id', 'label_id');
    }
}
