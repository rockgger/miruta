<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TagGroup extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tag_groups';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';


    public function labels(){
        return $this->hasMany(Label::class, 'tag_group_id');
    }
}
