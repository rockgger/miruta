<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaLabel extends Model
{
    use HasFactory;    
    protected $table = 'noticia_labels';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = 'id';
}
