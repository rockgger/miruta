<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpportunityNoticia extends Model
{
    use HasFactory;
    protected $table = 'opportunity_noticias';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = 'id';
    
}
