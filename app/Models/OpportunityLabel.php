<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpportunityLabel extends Model
{
    use HasFactory;
    protected $table = 'opportunity_labels';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = 'id';
}
