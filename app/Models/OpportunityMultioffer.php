<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OpportunityMultioffer extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'opportunity_multioffers';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';

   

    public function associated_opportunities(){
        return $this->belongsToMany('App\Models\Opportunity', 'associate_multioffer_landings', 'opportunity_multioffer_id', 'opportunity_id');
    }
}
