<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserDetail extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'user_details';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = 'id';

    public function profile()
    {   
        $lg = app()->getLocale();
        return $this->belongsTo(Profile::class, 'profile_id')
        ->select('id', 'name_'.$lg.' as name');
    }

    public function document_type()
    {   
        $lg = app()->getLocale();
        return $this->belongsTo(DocumentType::class, 'document_type_id')
        ->select('id', 'code', 'name_'.$lg.' as name');
    }

    public function user()
    {   
        $lg = app()->getLocale();
        return $this->belongsTo(User::class, 'user_id');
    }
}
