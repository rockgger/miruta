<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLabel extends Model
{
    protected $table = 'user_labels';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = 'id';
}
