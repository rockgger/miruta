<?php 

/*=======================================
|
|ARCHIVO DE HELPERS
|
|
|=======================================
*/

namespace App\Helpers;

use App\Mail\InviteUsers;
use Carbon\Carbon;
use App\Mail\RegisterUser;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Dotenv\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Helper{
    
    //registra ERROR en el archivo de logs
    public static function logError($msn){
        Log::error($msn); 
    }

    //correo de registro
    public static function mail_register($name, $email){
        try {
            
            $name = Str::of($name)->ucfirst();
            $mail = new RegisterUser($name);
            $mail->subject($name . ", te damos la bienvenida a Mi Ruta.");
            Mail::to($email)
                ->send($mail);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //correo de invitar usuarios a mi ruta
    public static function mail_invite_users($email){
        try {
            $url = "wwww.rutan.co";
            
            $mail = new InviteUsers($url);
            $mail->subject("Tienes una invitación pendiente.");
            Mail::to($email)
                ->send($mail);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public static function diffForHumans($date){
        $d = new Carbon($date);
        return $d->diffForHumans();
    }


    public static function opportunityType($id){
        switch ($id) {
            case 1:
                return 'estándar';
            break;
            case 2:
                return 'multioferta';
            break;
            case 3:
                return 'contratación';
            break;
            case 4:
                return 'landing';
            break;
            
            default:
                return '';
                break;
        }
    }

    public static function opportunityTypeClass($id){
        switch ($id) {
            case 1:
                return 'standar';
            break;
            case 2:
                return 'multioffer';
            break;
            case 3:
                return 'hidern';
            break;
            case 4:
                return 'landing';
            break;
            
            default:
                return '';
                break;
        }
    }

    //devuelve ES o EN para el idioma
    public static function selectLenguage($lg = null){
        $lg = Str::lower($lg);
        switch ($lg) {
            case 'es':
                app()->setLocale($lg);
                return $lg;
            break;
            case 'en':
                app()->setLocale($lg);
                return $lg;
            break;            
            default:
                app()->setLocale('es');
                return 'es';
            break;
        }

    }

    //validar procentaje pefil completado
    public static function validateFullProfile($user){
        $profile = (object)[
            'msg'=>'Incompleto',
            'ptg'=>0,
        ];
        try{
            $detail = $user->user_detail;
           
            $contador = 0;
            $total = 10; //total de datos a validar
            if(isset($user->name)){
                $contador ++;
            }
            if(isset($user->last_name)){
                $contador ++;
            }
            if(isset($user->email)){
                $contador ++;
            }
            if(isset($user->photo)){
                $contador ++;
            }
            if(isset($detail->document_type_id)){
                $contador ++;
            }
            if(isset($detail->document_number)){
                $contador ++;
            }
            if(isset($detail->biography)){
                $contador ++;
            }
            if(isset($detail->cell_phone)){
                $contador ++;
            }
            if(isset($detail->country)){
                $contador ++;
            }
            if(isset($detail->city)){
                $contador ++;
            }
           
            $pr = ($contador/$total)*100;
            $pr = round($pr,1);
            $pr = ($pr>100)?100:$pr;

            $profile->ptg = $pr;

            if($pr == 100){
                $profile->msg = 'Completo';
            }
            return $profile;
        }catch(\Exception $err){
            $profile->msg = $profile->msg." | ".$err->getMessage();
            return $profile;
        }
    }
    

    //valida autenticidad de acceso al servico sin token
    public static function authServiceToken($info) {
        
        try{

            $resp =  [
                "status"=>false,
                "cod"=>403,
                "msg"=>"Autenticación webservice no válida",
                "data"=>null
            ];
            
            if (!$info->hasHeader('signature')) {
                
                return $resp;
            }

            if (!$info->hasHeader('uid')) {
               
                return $resp;
            }

            $signature = $info->header('signature');
            $uid = strtolower($info->header('uid'));

        
            $llave = hash('SHA256', env('KEY_LANDING', 'demo').env('UID_LANDING', 'demo'));
            //comparamos llave enviada con llave de la BD
            if(($llave == $signature) AND ($uid == env('UID_LANDING', 'demo'))){
                return  [
                    "status"=>true,
                    "cod"=>200,
                    "msg"=>"Autenticación válida"
                ];
            }
            return $resp;

        }catch(\Exception $err){
            //capturamos cualquier error 

            $resp = [
                "status"=>false,
                "cod"=>Response::HTTP_INTERNAL_SERVER_ERROR,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["cod"] = $code;
                $resp["msg"] = $message;  
                return $resp;              
            }

            if($err instanceof ModelNotFoundException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["cod"] = $code;
                $resp["msg"] = $message;
                return $resp;
            }
    
            if($err instanceof AuthorizationException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["cod"] = $code;
                $resp["msg"] = $message;
                return $resp;
            }
    
            if($err instanceof AuthenticationException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["cod"] = $code;
                $resp["msg"] = $message;
                return $resp;
            }
    
            if($err instanceof ValidationException){
                $code = Response::HTTP_UNPROCESSABLE_ENTITY;
                $message = $err->validator->errors()->getMessages();
                $resp["cod"] = $code;
                $resp["msg"] = $message;
                return $resp;
            }

            return $resp;

                     
        }        


    }
}