<?php

namespace App\Http\Controllers\Api;

use App\Models\Team;
use App\Models\Label;
use App\Helpers\Helper;
use App\Models\TagGroup;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

 /**
    *
    * @OA\Tag(
    *     name="Team",
    *     description="API Endpoints de equipo de trabajo ruta n"
    * )
*/

class TeamController extends Controller
{   
    /**
    * @OA\Get(
    *     path="/home/teams/rutan",
    *     summary="listado del equipo de rutan",
    *     tags={"Team"},
    *     description="Devuelve listado del equipo de rutan.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *      @OA\Parameter(
    *          name="areas",
    *          description="Array, filtra los miembros del equipo por el id del área al que pertenece. Ej: ?areas=[1,2,3]",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="per_page",
    *          description="número de resultados a mostrar por página.",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="page",
    *          description=" paginación de resultados (pag actual a mostrar).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    //lista de equipo de trabajo de rutan
    public function teams(Request $request){


        $lg = app()->currentLocale();

        $order = isset($request->order)?$request->order:'desc';
        $order = Str::lower($order);
        $pag = 10;
        $area_ids = isset($request->areas)?json_decode($request->areas):[];

        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }

        if(!is_array($area_ids)){
            $area_ids = [];
        }

        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = Team::with('area')
            ->select('id', 'photo', 'name', 'email', 'role', 'phrase', 'testimony', 'label_id','created_at')
            ->whereHas('area', function(Builder $q) use($area_ids){
                if(count($area_ids) > 0){
                    $q->whereIn('id',$area_ids);
                }
            })
            ->orderBy('created_at',$order)
            ->paginate($pag);

            if(isset($request->order)){
                $data->appends(['order' => $order]);
            }
            
            if(isset($request->per_page) AND is_numeric($request->per_page) ){
                $data->appends(['per_page' => $pag]);
            }

            if(isset($request->areas)){
                $data->appends(['areas' => json_encode($area_ids)]);
            }

            $resp['status'] = true;
            $resp['msg'] = 'Lista de equipo de trabajo ruta n';
            $resp['data'] = $data->total() > 0?$data:null;
            
            
            return response()->json($resp, 200);  

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        }
    }

    /**
    * @OA\Get(
    *     path="/home/teams/areas",
    *     summary="Lista de Áreas del equipo de trabajo ruta n",
    *     tags={"Team"},
    *     description="Devuelve un listado de Áreas del equipo de trabajo ruta n",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    //lista de áreas del equipo de trabajo de rutan
    public function areas(Request $request){

        $lg = app()->currentLocale();

        $order = isset($request->order)?$request->order:'desc';
        $order = Str::lower($order);
        $pag = 10;
        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }

        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = Label::where('tag_group_id', 1)
            ->select('id', 'name_'.$lg.' as name','created_at')
            ->get();

            $resp['status'] = true;
            $resp['msg'] = 'Lista de Áreas del equipo de trabajo ruta n';
            $resp['data'] = $data;
            
            
            return response()->json($resp, 200);  

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        }
    }
}
