<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
/**
    *
    * @OA\Tag(
    *     name="Autenticación",
    *     description="API Endpoints para autenticarse por JWT"
    * )
*/
class AuthController extends Controller
{   


    /**
    * @OA\Post(
    *     path="/auth/login",
    *     summary="Autentica al usuario por JWT",
    *     tags={"Autenticación"},
    *     description="Autentica al usuario por JWT.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\RequestBody(
    *         @OA\MediaType(
    *             mediaType="application/json",
    *             @OA\Schema(
    *                 @OA\Property(
    *                     property="email",
    *                     type="string"
    *                 ),
    *                 @OA\Property(
    *                     property="password",
    *                     type="string"
    *                 ),
    *             )
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    //LOGIN USUARIO PARA API
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false,'msg'=> 'Correo electrónico o contraseña incorrecta.'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => false,'msg'=> 'could_not_create_token'], 500);
        }
        return response()->json(["status"=>true, "token"=>$token, "token_type"=> "bearer",
        "expires_in_minutes"=> 60], 200);
    }

    /**
 * @OA\SecurityScheme(
 *     type="http",
 *     description="Inicie sesión con correo electrónico y contraseña para obtener el token de autenticación. (Autenticación Bearer Token)",
 *     name="Autenticación Bearer Token",
 *     in="header",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 *     securityScheme="apiAuth",
 * )
 */

    /**
    * @OA\Post(
    *     path="/auth/user",
    *     summary="Datos del usuario autenticado",
    *     tags={"Autenticación"},
    *     description="Devuelve los Datos del usuario autenticado.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     security={{ "apiAuth": {} }},
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    //DEVUELVE LOS DATOS DEL USUARIO POR JWT 
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['status' => false,'msg'=>'user_not_found'], 404);
            }   
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['status' => false,'msg'=>'token_expired'], $e->getStatusCode());
            } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['status' => false,'msg'=>'token_invalid'], $e->getStatusCode());
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['status' => false,'msg'=>'token_absent'], $e->getStatusCode());
            }
            return response()->json(['status' => true,'msg'=>'datos del usuario','user'=>$user->only('id', 'name', 'last_name', 'photo')], 200);
    }


    /**
    * @OA\Post(
    *     path="/auth/logout",
    *     summary="Cierra sesión activa del usuario",
    *     tags={"Autenticación"},
    *     description="Cierra sesión activa del usuario.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     security={{ "apiAuth": {} }},
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function logout()
    {
        //auth()->logout();
        JWTAuth::parseToken()->invalidate();
        //JWTAuth::parseToken()->refresh();

        return response()->json(['status' => true,'msg'=> 'Successfully logged out']);
    }
}
