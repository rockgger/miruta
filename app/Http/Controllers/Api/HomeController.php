<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Models\HomeResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

 /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="API Mi Ruta",
     *      description="Descripción del API",
     *      @OA\Contact(
     *          email="r.jimenez@rutanmedellin.org"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="URL API Server"
     * )

    *
    * @OA\Tag(
    *     name="Home",
    *     description="API Endpoints de Home"
    * )
*/

class HomeController extends Controller
{
    /**
    * @OA\Get(
    *     path="/home/resources",
    *     summary="Lista de recursos del Home",
    *     tags={"Home"},
    *     description="Devuelve un listado de recursos para usar en el home landing.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function home(Request $request){

        //$lg = app()->currentLocale();
        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $resources = HomeResource::whereIn('position', ['herohome','desktop', 'mobile'])
            ->latest()
            ->get();

            $desktop = $resources->first(function ($value, $key) {
                return $value->position == 'desktop';
            });

            $mobile = $resources->first(function ($value, $key) {
                return $value->position == 'mobile';
            });

            $hero = $resources->where('position','herohome');
            //return response()->json($hero->pluck(['url']));

            $resp['status'] = true;
            $resp['msg'] = 'Listado de recursos del home (banner and home hero)';
            $resp['data'] = [
                'banner_desktop'=>optional($desktop)->url,
                'banner_mobile'=>optional($mobile)->url,
                'hero_home'=>$hero->pluck(['url']),
            ];
            
            
            return response()->json($resp, 200);  

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 

    }


    public function demo(Request $request){
       /*  return response()->json($request->all()); */
       Helper::mail_register("Rogger demo", "roggerjimenez@gmail.com");
    }
}
