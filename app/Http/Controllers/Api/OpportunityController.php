<?php

namespace App\Http\Controllers\Api;

use App\Models\Label;
use App\Helpers\Helper;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\OpportunityHiring;
use App\Models\OpportunityLanding;
use App\Models\OpportunityNoticia;
use Illuminate\Support\Facades\DB;
use App\Models\OpportunityStandard;
use App\Http\Controllers\Controller;
use App\Models\OpportunityMultioffer;
use App\Models\AssociateMultiofferLanding;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

 /**
    *
    * @OA\Tag(
    *     name="Opportunities",
    *     description="API Endpoints de oportunidades"
    * )
*/

class OpportunityController extends Controller
{   
        
    /**
    * @OA\Get(
    *     path="/opportunities/type",
    *     summary="listado de tipos de oportunidades",
    *     tags={"Opportunities"},
    *     description="Devuelve un listado de tipos de oportunidades.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    //listado de tipos de oportunidades
    public function opportunitiesType(){
        $lg = app()->currentLocale();
        try{
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = [
                'standar'=>[
                    'name'=>($lg == 'es')?'Oportunida estándar':'Standard opportunity',
                    'type_id'=>1,
                    'url'=>'/opportunities/1',
                    'count_published'=>  Opportunity::where('status_id', 13)->where('type_id', 1)->count()
                ],
                'multioffer'=>[
                    'name'=>($lg == 'es')?'Oportunida multioferta':'Multi-offer opportunity',
                    'type_id'=>2,
                    'url'=>'/opportunities/2',
                    'count_published'=>  Opportunity::where('status_id', 13)->where('type_id', 2)->count()
                ],
                'landing'=>[
                    'name'=>($lg == 'es')?'Oportunida landing':'Landing opportunity',
                    'type_id'=>4,
                    'url'=>'/opportunities/4',
                    'count_published'=>  Opportunity::where('status_id', 13)->where('type_id', 4)->count()
                ],
                'hiring'=>[
                    'name'=>($lg == 'es')?'Oportunida contratación':'Hiring opportunity',
                    'type_id'=>3,
                    'url'=>'/opportunities/3',
                    'count_published'=>  Opportunity::where('status_id', 13)->where('type_id', 3)->count()
                ],
            ];

            $resp['status'] = true;
            $resp['msg'] = 'Listado de tipos de oportunidades';
            $resp['data'] = $data;
            
            return response()->json($resp, 200); 

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }

    /**
    * @OA\Get(
    *     path="/opportunities",
    *     summary="listado de oportunidades",
    *     tags={"Opportunities"},
    *     description="Devuelve listado de oportunidades.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *      @OA\Parameter(
    *          name="order",
    *          description="orden de resultados (asc, desc).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="string",
    *              enum= {"asc", "desc"}
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="per_page",
    *          description="número de resultados a mostrar por página.",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="page",
    *          description=" paginación de resultados (pag actual a mostrar).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    //listado de oportunidades
    public function opportunities(Request $request){
        
        $lg = app()->currentLocale();
        $order = isset($request->order)?$request->order:'desc';
        $order = Str::lower($order);
        $pag = 10;
        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }

        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = Opportunity::with('type')
            ->with(['actors'=>function($q) use($lg){
                $q->where('opportunity_labels.type', 'actors')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
            }])
            ->with(['categories' =>function($q3) use($lg){
                $q3->where('opportunity_labels.type', 'categories')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
            }])
            ->where('status_id', 13)
            ->select('id', 'title_'.$lg.' as title', 'presentation_'.$lg.' as text', 'image as thumbnail', 'status_id', 'type_id', 'created_at')
            ->orderBy('created_at',$order)
            ->paginate($pag);

            if(isset($request->order)){
                $data->appends(['order' => $order]);
            }
            
            if(isset($request->per_page) AND is_numeric($request->per_page) ){
                $data->appends(['per_page' => $pag]);
            }

            $resp['status'] = true;
            $resp['msg'] = 'Listado de oportunidades';
            $resp['data'] = $data->total() > 0?$data:null;
            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }


    /**
    * @OA\Get(
    *     path="/opportunities/{type_id}",
    *     summary="lista de oportunidades según el tipo",
    *     tags={"Opportunities"},
    *     description="Devuelve lista de oportunidades según el tipo.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="type_id",
    *          description="Id del tipo de oportunidades a listar",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="order",
    *          description="orden de resultados (asc, desc).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="string",
    *              enum= {"asc", "desc"}
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="per_page",
    *          description="número de resultados a mostrar por página.",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="page",
    *          description=" paginación de resultados (pag actual a mostrar).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    //lista de oportunidades según el tipo
    public function opportunityList(Request $request){

        $lg = app()->currentLocale();
        $order = isset($request->order)?$request->order:'desc';
        $order = Str::lower($order);
        $pag = 10;
        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }

        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = Opportunity::with(['actors'=>function($q) use($lg){
                $q->where('opportunity_labels.type', 'actors')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
            }])
            ->with(['categories' =>function($q3) use($lg){
                $q3->where('opportunity_labels.type', 'categories')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
            }])
            ->where('status_id', 13)
            ->where('type_id', $request->type_id)
            ->select('id', 'title_'.$lg.' as title', 'presentation_'.$lg.' as text', 'image as thumbnail', 'status_id', 'type_id', 'created_at')
            ->orderBy('created_at',$order)
            //->get();
            ->paginate($pag);

            if(isset($request->order)){
                $data->appends(['order' => $order]);
            }
            
            if(isset($request->per_page) AND is_numeric($request->per_page) ){
                $data->appends(['per_page' => $pag]);
            }

            $resp['status'] = true;
            $resp['msg'] = 'Listado de oportunidades por tipo';
            $resp['data'] = $data->total() > 0?$data:null;
            

            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }


    /**
    * @OA\Get(
    *     path="/opportunity/{type_id}/{opportunity_id}",
    *     summary="Detalles de la oportunidad",
    *     tags={"Opportunities"},
    *     description="Devuelve Detalles de la oportunidad.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="type_id",
    *          description="Id del tipo de oportunidad",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="opportunity_id",
    *          description="Id de la oportunidad a buscar.",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    //detalles de la portunidad
    public function opportunity(Request $request){

        $lg = app()->currentLocale();
        
        
        if(!isset($request->type_id)){
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Campo type_id (tipo de oportunidad) requerido",
                "data"=>null
            ];
            return response()->json($resp, $code);  
        }


        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            switch ($request->type_id) {
                case 1:
                    $data = $this->getOpStandar($request->opportunity_id, $lg);
                    break;
                case 2:
                    $data = $this->getOpMultioffer($request->opportunity_id, $lg);
                    break;
                case 3:
                    $data = $this->getOpHiring($request->opportunity_id, $lg);
                    break;
                case 4:
                    $data = $this->getOpLanding($request->opportunity_id, $lg);
                    break;
                    
                default:
                    $data = null;
                    break;
            }
            

            

            

            $resp['status'] = true;
            $resp['msg'] = 'Detalles de la oportunidad';
            $resp['data'] = $data;
            

            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }

    
    /**
    * @OA\Get(
    *     path="/opportunity/{type_id}/{opportunity_id}/offers",
    *     summary="listado de oportunidade u offertas asociadas a la oport. multioffer",
    *     tags={"Opportunities"},
    *     description="Devuelve listado de oportunidade u offertas asociadas a la oport. multioffer.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="type_id",
    *          description="Id del tipo de oportunidad",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *     @OA\Parameter(
    *          name="opportunity_id",
    *          description="Id de la oportunidad a buscar.",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="per_page",
    *          description="número de resultados a mostrar por página.",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="page",
    *          description=" paginación de resultados (pag actual a mostrar).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    //listado de oportunidade u offertas asociadas a la oport. multioffer
    public function opportunityOffers(Request $request){

        $lg = app()->currentLocale();
        $pag = 10;
        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }
        
        if(!isset($request->type_id)){
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Campo type_id (tipo de oportunidad) requerido",
                "data"=>null
            ];
            return response()->json($resp, $code);  
        }

        if(!isset($request->opportunity_id)){
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Campo opportunity_id (id oportunidad) requerido",
                "data"=>null
            ];
            return response()->json($resp, $code);  
        }


        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ]; 

            if($request->type_id != 2){
                $resp['status'] = true;
                $resp['msg'] = 'offertas asociadas a la oportunidad';
                $resp['data'] = null;
                return response()->json($resp, $code);  
            }   

            $data = AssociateMultiofferLanding::where('opportunity_multioffer_id', function($q) use($request){
                $q->select('om.id')
                ->where('opportunity_id', $request->opportunity_id)
                ->from('opportunity_multioffers as om');
            })
            ->join('opportunities as op', 'associate_multioffer_landings.opportunity_id', '=', 'op.id')
            ->where('op.status_id', 13)
            ->select('op.id', 'op.type_id', 'op.image as thumbnail', 'op.title_es as title', 'op.status_id', 'op.presentation_'.$lg.' as text' , 'op.created_at')
            ->paginate($pag);

            if(isset($request->per_page) AND is_numeric($request->per_page) ){
                $data->appends(['per_page' => $pag]);
            }

            $resp['status'] = true;
            $resp['msg'] = 'offertas asociadas a la oportunidad';
            $resp['data'] = $data->total() > 0?$data:null;
            

            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }


    /**
    * @OA\Get(
    *     path="/opportunity/{type_id}/{opportunity_id}/news",
    *     summary="listado de noticias asociadas a una oportunidad",
    *     tags={"Opportunities"},
    *     description="Devuelve listado de noticias asociadas a una oportunidad.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="type_id",
    *          description="Id del tipo de oportunidad",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *     @OA\Parameter(
    *          name="opportunity_id",
    *          description="Id de la oportunidad a buscar.",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="per_page",
    *          description="número de resultados a mostrar por página.",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="page",
    *          description=" paginación de resultados (pag actual a mostrar).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    //listado de noticias asociadas a la oport. 
    public function opportunityNews(Request $request){

        $lg = app()->currentLocale();
        $pag = 10;
        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }
        
        if(!isset($request->type_id)){
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Campo type_id (tipo de oportunidad) requerido",
                "data"=>null
            ];
            return response()->json($resp, $code);  
        }

        if(!isset($request->opportunity_id)){
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Campo opportunity_id (id oportunidad) requerido",
                "data"=>null
            ];
            return response()->json($resp, $code);  
        }


        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ]; 

            if(($request->type_id != 1) AND ($request->type_id != 4)){
                $resp['status'] = true;
                $resp['msg'] = 'Noticias asociadas a la oportunidad';
                $resp['data'] = null;
                return response()->json($resp, $code);  
            }   

            $data = OpportunityNoticia::where('opportunity_id',  $request->opportunity_id)
            ->join('noticias as nt', 'opportunity_noticias.noticia_id', '=', 'nt.id')
            ->where('nt.status_id', 13)
            ->select('nt.id', 'nt.image as thumbnail', 'nt.title_es as title', 'nt.status_id', 'nt.summary_'.$lg.' as text' , 'nt.created_at')
            ->paginate($pag);

            if(isset($request->per_page) AND is_numeric($request->per_page) ){
                $data->appends(['per_page' => $pag]);
            }

            $resp['status'] = true;
            $resp['msg'] = 'Noticias asociadas a la oportunidad';
            $resp['data'] = $data->total() > 0?$data:null;
            

            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }
    
    
    //devuelve datos op estándar
    private function getOpStandar($opportunity_id,  $lg = 'es'){

        $op = Opportunity::with(['actors'=>function($q) use($lg){
            $q->where('opportunity_labels.type', 'actors')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['categories' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'categories')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['targets' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'targets')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['area' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'area')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['partners' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'partners')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with('openstatus')
        ->where('opportunities.status_id', 13)
        ->where('opportunities.type_id', 1)
        ->where('opportunities.id', $opportunity_id)
        ->join('opportunity_standards as os', 'opportunities.id', '=', 'os.opportunity_id')
        ->select('opportunities.id', 'opportunities.title_'.$lg.' as title', 'opportunities.presentation_'.$lg.' as text', 'opportunities.image as thumbnail', 'opportunities.status_id', 'opportunities.open_status_id', 'opportunities.type_id', 'os.start_date', 'os.end_date', 'os.url_forms as url', 'os.benefit->'.$lg.' as benefit', 'os.schedule_activities->'.$lg.' as activities', 'opportunities.created_at')
        ->first();

        if($op != null){
            if($op->benefit != null){
                $op->benefit = json_decode($op->benefit);
            }

            if($op->activities != null){
                $op->activities = json_decode($op->activities);
            }
        }
        return $op;

    }

    //devuelve datos op multi oferta
    private function getOpMultioffer($opportunity_id,  $lg = 'es'){

        $op = Opportunity::with(['actors'=>function($q) use($lg){
            $q->where('opportunity_labels.type', 'actors')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['categories' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'categories')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['targets' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'targets')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['area' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'area')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with('openstatus')
        ->where('opportunities.status_id', 13)
        ->where('opportunities.type_id', 2)
        ->where('opportunities.id', $opportunity_id)
        ->join('opportunity_multioffers as os', 'opportunities.id', '=', 'os.opportunity_id')
        ->select('opportunities.id', 'opportunities.title_'.$lg.' as title', 'opportunities.presentation_'.$lg.' as text', 'opportunities.image as thumbnail', 'opportunities.status_id', 'opportunities.open_status_id', 'opportunities.type_id', 'os.context', 'os.title_group_offer_'.$lg.' as title_group_offer', 'os.description_group_offer_'.$lg.' as description_group_offer', 'opportunities.created_at')
        ->first();

        if($op != null){
            if($op->context != null){
                $op->context = json_decode($op->context);
            }
        }
        

        return $op;

    }

    //devuelve datos op contratación
    private function getOpHiring($opportunity_id,  $lg = 'es'){

        $op = Opportunity::with(['actors'=>function($q) use($lg){
            $q->where('opportunity_labels.type', 'actors')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['categories' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'categories')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['targets' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'targets')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['area' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'area')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with('openstatus')
        ->where('opportunities.status_id', 13)
        ->where('opportunities.type_id', 3)
        ->where('opportunities.id', $opportunity_id)
        ->join('opportunity_hirings as os', 'opportunities.id', '=', 'os.opportunity_id')
        ->join('labels as lb', 'os.type_contract', '=', 'lb.id')
        ->select('opportunities.id', 'opportunities.title_'.$lg.' as title', 'opportunities.presentation_'.$lg.' as text', 'opportunities.image as thumbnail', 'opportunities.status_id', 'opportunities.open_status_id', 'opportunities.type_id', 'os.start_date', 'os.end_date', 'os.url_forms as url', 'os.salary', 'os.attachments',  'lb.name_'.$lg.' as contract', 'opportunities.created_at')
        ->first();

        if($op != null){
            if($op->attachments != null){
                $op->attachments = json_decode($op->attachments);
            }
        }
        

        return $op;

    }

    //devuelve datos op landing
    private function getOpLanding($opportunity_id,  $lg = 'es'){

        $op = Opportunity::with(['actors'=>function($q) use($lg){
            $q->where('opportunity_labels.type', 'actors')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['categories' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'categories')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['targets' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'targets')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with(['area' =>function($q3) use($lg){
            $q3->where('opportunity_labels.type', 'area')
            ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
        }])
        ->with('openstatus')
        ->where('opportunities.status_id', 13)
        ->where('opportunities.type_id', 4)
        ->where('opportunities.id', $opportunity_id)
        ->join('opportunity_landings as os', 'opportunities.id', '=', 'os.opportunity_id')
        ->select('opportunities.id', 'opportunities.title_'.$lg.' as title',  'os.summary_'.$lg.' as summary', 'opportunities.presentation_'.$lg.' as text', 'opportunities.image as thumbnail', 'opportunities.status_id', 'opportunities.open_status_id', 'opportunities.type_id', 'os.context', 'os.title_accordion_'.$lg.' as title_accordion', 'os.accordion->es as accordion', 'os.benefit->es as benefit', 'os.how_functions->es as how_functions', 'os.programs_services->es as programs_services', 'os.url_funtion_image as url_image_how_functions', 'os.url_benefit_image as url_image_benefit', 'opportunities.created_at')
        ->first();

        if($op != null){
            if($op->context != null){
                $op->context = json_decode($op->context);
            }

            if($op->accordion != null){
                $op->accordion = json_decode($op->accordion);
            }

            if($op->how_functions != null){
                $op->how_functions = json_decode($op->how_functions);
            }

            if($op->programs_services != null){
                $op->programs_services = json_decode($op->programs_services);
            }

            if($op->benefit != null){
                $op->benefit = json_decode($op->benefit);
            }
        }
        

        return $op;

    }

    
}
