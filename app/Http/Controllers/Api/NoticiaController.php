<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Noticia;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

 /**
    *
    * @OA\Tag(
    *     name="Noticias",
    *     description="API Endpoints de Noticias"
    * )
*/

class NoticiaController extends Controller
{
    
    /**
    * @OA\Get(
    *     path="/news",
    *     summary="listado de noticias",
    *     tags={"Noticias"},
    *     description="Devuelve listado de noticias activas.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="order",
    *          description="orden de resultados (asc, desc).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="string",
    *              enum= {"asc", "desc"}
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="per_page",
    *          description="número de resultados a mostrar por página.",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      @OA\Parameter(
    *          name="page",
    *          description=" paginación de resultados (pag actual a mostrar).",
    *          required=false,
    *          in="query",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    * @OA\Get(
    *     path="/news/{new_id}",
    *     summary="Detalles de la noticia",
    *     tags={"Noticias"},
    *     description="Devuelve el Detalles de una noticia.",
    *     @OA\Parameter(
    *          name="signature",
    *          description="Firma de autenticación de servicio: hash('256', 'llave privada del servicio'.'identificador único del servicio')",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="uid",
    *          description="identificador único del servicio",
    *          required=true,
    *          in="header",
    *          @OA\Schema(
    *              type="string"
    *          )
    *     ),
    *     @OA\Parameter(
    *          name="new_id",
    *          description="Id de la noticia a buscar",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful operation"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

    //listado de noticias
    public function noticias(Request $request){

        $lg = app()->currentLocale();
        $order = isset($request->order)?$request->order:'desc';
        $order = Str::lower($order);
        $pag = 10;
        if(isset($request->per_page) AND is_numeric($request->per_page)){
            $pag = $request->per_page;
        }

        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = Noticia::with(['categories' =>function($q3) use($lg){
                $q3->where('noticia_labels.type', 'categories')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'noticia_labels.type');
            }])
            ->where('status_id', 13)
            ->select('id', 'title_'.$lg.' as title', 'summary_'.$lg.' as text', 'image as thumbnail', 'status_id', 'created_at')
            ->orderBy('created_at',$order)
            ->paginate($pag);

            if(isset($request->order)){
                $data->appends(['order' => $order]);
            }
            
            if(isset($request->per_page) AND is_numeric($request->per_page) ){
                $data->appends(['per_page' => $pag]);
            }

            $resp['status'] = true;
            $resp['msg'] = 'Listado de noticias';
            $resp['data'] = $data->total() > 0?$data:null;
            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }


    //detalles de la noticia
    public function noticia(Request $request){

        $lg = app()->currentLocale();
        
        
        
        try{

            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>"Se presentó un error, intentalo despues",
                "data"=>null
            ];

            $data = Noticia::with(['categories' =>function($q3) use($lg){
                $q3->where('noticia_labels.type', 'categories')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'noticia_labels.type');
            }])
            ->with(['targets' =>function($q3) use($lg){
                $q3->where('noticia_labels.type', 'targets')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'noticia_labels.type');
            }])
            ->with(['area' =>function($q3) use($lg){
                $q3->where('noticia_labels.type', 'area')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'noticia_labels.type');
            }])
            ->where('status_id', 13)
            ->where('id', $request->new_id)
            ->select('id', 'title_'.$lg.' as title', 'summary_'.$lg.' as summary', 'body_'.$lg.' as text', 'image as thumbnail', 'status_id', 'created_at')
            ->first();   


            if(optional($data->text)){
                $data->text = htmlspecialchars_decode($data->text, ENT_QUOTES);
            }

            $resp['status'] = true;
            $resp['msg'] = 'Detalles de la noticia';
            $resp['data'] = $data;
            

            
            
            return response()->json($resp, 200);  

            //return view('welcome',['data'=>$data]);

        }catch(\Exception $err){
            //capturamos cualquier error 
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resp = [
                "status"=>false,
                "msg"=>$err->getMessage(),
                "data"=>null
            ];
            
            if($err instanceof HttpException){
                $code = $err->getStatusCode();
                $message = Response::$statusTexts[$code];
                $resp["msg"] = $message;                
            }

            return response()->json($resp, $code);            
        } 
    }
}
