<?php

namespace App\Http\Controllers\Aplication;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function home(){


        return view('app.home.home');
    }


    //vista mi ruta n
    public function myrutan(Request $request){

        $lg = app()->getLocale();

        $user = auth()->user();
        $profile = UserDetail::where('user_id', $user->id)
        ->join('profiles as p', 'user_details.profile_id', '=', 'p.id')
        ->select('p.id', 'p.name_'.$lg.' as name')
        ->first();
        
        
        //$services = $user->services();

        return view("app.home.my-rutan", [
            "user"=>$user,
            "profile"=>$profile,
        ]);
    }


    public function myProfile(Request $request){

      

        return view("app.home.my-profile");
    }
    
}
