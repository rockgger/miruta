<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    //vista registro
    public function register(Request $request){
       
        if (Auth::check()) {
            return "autenticado.";
        }
       
        return view('auth.register');
    }
}
