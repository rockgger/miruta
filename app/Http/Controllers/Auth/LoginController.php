<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //Vista login
    public function login(Request $request)
    {

        if (Auth::check()) {

            //redireccionar a la url que tiene la var redirect
            if (isset($request->redirect)) {
                return redirect($request->redirect);
            }

            

            /* if (Auth::user()->is_user()) {
                return redirect()->route('app.dashboard');
            } */

            
            return redirect()->route('app.home');
            /* return "es admin";  */
        }

        return view('auth.login');
    }
}
