<?php

namespace App\Http\Controllers\Admin;

use App\Models\Opportunity;
use Illuminate\Http\Request;
use App\Models\OpportunityHiring;
use App\Models\OpportunityLanding;
use App\Models\OpportunityStandard;
use App\Http\Controllers\Controller;
use App\Models\OpportunityMultioffer;

class DashboardController extends Controller
{
    public function home(){

        $standar =  Opportunity::where('open_status_id', 3)->where('type_id', 1)->count();
        $landing = Opportunity::where('open_status_id', 3)->where('type_id', 4)->count();
        $multi = Opportunity::where('open_status_id', 3)->where('type_id', 2)->count();
        $hiring = Opportunity::where('open_status_id', 3)->where('type_id', 3)->count();

        $activities = Opportunity::where('open_status_id', 3)
        ->select('id', 'title_es as title', 'type_id', 'status_id','created_at')
        ->latest()
        ->limit(3)
        ->get()->toArray();

        //dd($activities);

        return view('admin.dashboard',[
            "activities"=>$activities,
            "count_standar"=>$standar,
            "count_landing"=>$landing,
            "count_multi"=>$multi,
            "count_hiring"=>$hiring,
        ]);
    }
}
