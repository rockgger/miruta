<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OpportunityController extends Controller
{
    public function standard(){

        
        return view('admin.opportunity.standard');
    }


    public function standardCreate(){

        
        return view('admin.opportunity.standard-create');
    }



    public function standardEdit(Request $request){

        if(!isset($request->opportunity_id)){
            abort(404);
        }
        
        return view('admin.opportunity.standard-edit',[
            'opportunity_id'=>$request->opportunity_id
        ]);
    }




    public function landing(){
        return view('admin.opportunity.landing');
    }


    public function landingCreate(){
        return view('admin.opportunity.landing-create');
    }


    public function landingEdit(Request $request){

        if(!isset($request->opportunity_id)){
            abort(404);
        }
        
        return view('admin.opportunity.landing-edit',[
            'opportunity_id'=>$request->opportunity_id
        ]);
    }


    public function hiring(){
        return view('admin.opportunity.hiring');
    }


    public function hiringCreate(){
        return view('admin.opportunity.hiring-create');
    }
    

    public function hiringEdit(Request $request){

        if(!isset($request->opportunity_id)){
            abort(404);
        }
        
        return view('admin.opportunity.hiring-edit',[
            'opportunity_id'=>$request->opportunity_id
        ]);
    }


    public function multioffer(){
        return view('admin.opportunity.multioffer');
    }


    public function multiofferCreate(){
        return view('admin.opportunity.multioffer-create');
    }
    

    public function multiofferEdit(Request $request){

        if(!isset($request->opportunity_id)){
            abort(404);
        }
        
        return view('admin.opportunity.multioffer-edit',[
            'opportunity_id'=>$request->opportunity_id
        ]);
    }

    
    


    

}
