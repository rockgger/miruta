<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LabelController extends Controller
{


    public function index(){

        return view('admin.labels.index');
    }


    public function editGroup(Request $request){

        if(!isset($request->tag_group_id)){
            abort(404);
        }

        return view('admin.labels.edit',[
            'tag_group_id'=>$request->tag_group_id
        ]);
    }

    public function editProfile(Request $request){

        
        return view('admin.profiles.edit');
    }


    public function create(Request $request){

        
        return view('admin.labels.create');
    }
    
}
