<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NoticiasConctroller extends Controller
{
    public function noticias(){

        
        return view('admin.noticias.index');
    }


    public function create(){

        
        return view('admin.noticias.create');
    }


    public function edit(Request $request){

        if(!isset($request->noticia_id)){
            abort(404);
        }
        return view('admin.noticias.edit',[
            'noticia_id'=>$request->noticia_id
        ]);
    }
}
