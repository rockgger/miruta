<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ValidateKeyToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   

        //idioma
        $lenguage = isset($request->lg)?$request->lg:($request->hasHeader('lg')?$request->header('lg'):'es');
        $lenguage = Str::lower($lenguage);
        $lenguage = ($lenguage != 'es' AND $lenguage != 'en')?'es':$lenguage;
        app()->setLocale($lenguage);

        //helper para validar llaves del servicio
        $valid = Helper::authServiceToken($request);   
        
        $valid = (object)$valid;
        if($valid->status){
            return $next($request);
        }        
        return response()->json([
            "status"=>$valid->status,
            "msg"=>$valid->msg,
            "data"=>$valid->data,
        ], $valid->cod);
        //return $next($request);
        
        
        //app()->setLocale($lg);
    }
}
