<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ( auth()->check() || auth()->viaRemember() ){
            if(auth()->user()->is_sAdmin()){

                

                return $next($request);
            }
            //abort(401);
            return redirect()->route('auth.login');
        }else{
            return redirect()->route('auth.login');
        }
    }
}
