<?php

namespace App\Http\Livewire\Auth;

use App\Helpers\Helper;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class Login extends Component
{
    
    protected $queryString = ['redirect'];

    protected $rules = [
        'password'=>'required',
        'email'=>'required|email'
    ];

    protected $messages = [
        'password.required' => 'Ingresa una contraseña',
        'email.required' => 'Ingresa un correo electrónico',
        'email.email' => 'Ingresa un correo electrónico válido'
    ];

    public $email, $password, $email_change;
    public $redirect;

    public function render()
    {
        return view('livewire.auth.login');
    }


    
    public function login(){
        $this->validate();
        $this->resetErrorBag();
        try{

            //$role = 4;
            
            if (Auth::attempt(['email' => $this->email, 'password' => $this->password ], false)){

                
                Auth::user()->session = Session::getId();
                Auth::user()->save();

                if(auth()->user()->role_id == 1){
                    //redireccionar a la url que tiene la var redirect
                    if(isset($this->redirect)){ 


                        $credentials = ['email' => $this->email, 'password' => $this->password ];
                        $token = JWTAuth::attempt($credentials);

                        return redirect()
                        ->away($this->redirect.'?token='.$token);
                    }
                    return redirect()->route('app.home');

                }elseif(in_array(auth()->user()->role_id, [2,3,4])){
                    return redirect()->route('admin.home');
                }else{
                    Auth::logout();
                    session()->flash('message.error', 'Correo electrónico o contraseña incorrecta.');
                    $this->reset(['password']);
                    return;
                }

                
            }else{
                session()->flash('message.error', 'Correo electrónico o contraseña incorrecta.');
                $this->reset(['password']);
            }
        }catch (\Exception $e){
            //generamos cod de seguimiento
            $cod = "AUT".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  

            session()->flash('message.error', "Error intentando ingresar a su cuenta. Cod: ".$cod);
            
            Helper::logError('COD: '.$cod.' | Error login. ERROR: '.$e->getMessage());

            $this->reset(['password']);
        }
        //$this->reset(['email','password']);
        
    }
}
