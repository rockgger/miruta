<?php

namespace App\Http\Livewire\Auth;

use Carbon\Carbon;
use App\Models\User;
use App\Helpers\Helper;
use App\Models\Profile;
use App\Models\UserDetail;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class Register extends Component
{   
    protected $queryString = ['redirect'];
    public $redirect;

    public $profile_id, $profiles = [], $lg, $name, $last_name, $email, $password;

                
    protected $messages = [                 
        'name.required'=>'Ingresa tus nombres',
        'last_name.required'=>'Ingresa tus apellidos',
        'email.required'=>'Ingresa tu correo electrónico',
        'email.email'=>'Ingresa un correo electrónico válido',
        'profile_id.required'=>'Selecciona un perfil',
        'password.required'=>'Ingresa tu contraseña',
        'password.min'=>'La contraseña debe contener mínimo 9 dígitos o caracteres ',
        'email.unique' => 'Correo no disponible.'
    ];

    public function mount(){
        $this->lg = app()->getLocale();
        $this->profiles = Profile::select('id', 'name_'.$this->lg.' as name')->get()->toArray();
    }


    public function render()
    {
        return view('livewire.auth.register');
    }


    public function register(){

        

        $rules = [                
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore(auth()->user())
            ],
            'password'=>'required|min:9',
            'name'=>'required',
            'last_name'=>'required',
            'profile_id'=>'required',
        ];

        $this->validate($rules);
        $this->resetErrorBag();

        try{

            DB::transaction(function (){
                //creamos el nuevo usuario
                $user = new User();
                $user->name = $this->name;
                $user->last_name = $this->last_name;
                $user->email = $this->email;
                $user->password = bcrypt($this->password);
                $user->status_id = 1;
                $user->role_id = 1;
                $user->email_verified_at = Carbon::now();
                $user->treatment_data_at = Carbon::now();
                $user->habeas_data_at = Carbon::now();
                $user->save();

                $detail = new UserDetail();
                $detail->user_id = $user->id;
                $detail->profile_id = $this->profile_id;
                $detail->save();

                Helper::mail_register($user->name, $user->email);

                $this->dispatchBrowserEvent('showSweetAlertRediret', [
                    'title' => 'Bien',
                    'text' => 'Registro exitoso.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR',
                    'url' => route('auth.login')
                ]);
                return;
            });

            //session()->flash('message.error', "No se pudó registrar tu cuenta, intenta en otro momento.");

        }catch (\Exception $e){
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  

            session()->flash('message.error', "Se presentó un error, intenta en otro momento. Cod: ".$cod);
            
            Helper::logError('COD: '.$cod.' | Error registro. ERROR: '.$e->getMessage());
            $this->reset(['password']);
        }
    }
}
