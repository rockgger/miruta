<?php

namespace App\Http\Livewire\Aplication\Profile;

use Livewire\Component;

class Photo extends Component
{   

    protected $listeners = ['reloadPhoto' => 'reloadPhoto'];

    public $photo;

    public function mount(){
        $this->photo = Auth()->user()->photo;
    }

    public function reloadPhoto(){
        $this->photo = Auth()->user()->photo;
    }

    public function render()
    {
        return view('livewire.aplication.profile.photo');
    }
}
