<?php

namespace App\Http\Livewire\Aplication\Profile;

use Livewire\Component;

class Name extends Component
{   

    protected $listeners = ['reloadFullName' => 'reloadFullName'];

    public $fullName;

    public function mount(){
        $this->fullName = Auth()->user()->name;
    }

    public function reloadFullName(){
        $this->fullName = Auth()->user()->name;
    }

    public function render()
    {
        return view('livewire.aplication.profile.name');
    }
}
