<?php

namespace App\Http\Livewire\Aplication\Profile;

use App\Models\User;
use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\UserDetail;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\DocumentType;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Profile as ModelProfile;

class Profile extends Component
{   

    public $user, $lg;
    public $name, $last_name, $email, $photo;
    public $profile_id, $document_type_id, $document_number, $biography, $cell_phone, $country, $city, $interes_ids = [];

    public $profiles = [], $document_types = [], $avatares = [], $interes = []; 

    protected $messages = [ 
        'name.required'=>'Campo requerido.',
        'email.email'=>'Correo no válido.',
        'email.required'=>'Campo requerido.',
        'email.unique'=>'Correo no disponible.',
        'profile_id.required'=>'Campo requerido.',
    ];

    public function mount(){

        $this->lg = app()->getLocale();
        


        $this->user = auth()->user();
        $this->name = $this->user->name;
        $this->last_name = $this->user->last_name;
        $this->email = $this->user->email;
        $this->photo = $this->user->photo;

        $detail = $this->user->user_detail;
        $this->profile_id = $detail->profile_id;
        $this->document_type_id = $detail->document_type_id;
        $this->document_number = $detail->document_number;
        $this->biography = $detail->biography;
        $this->cell_phone = $detail->cell_phone;
        $this->country = $detail->country;
        $this->city = $detail->city;


        $this->profiles = ModelProfile::select('id', 'name_'.$this->lg.' as name')->get()->toArray();
        $this->document_types = DocumentType::select('id', 'name_'.$this->lg.' as name')->get()->toArray();

        //TAG_GROUPS Área de interés id 6
        $this->interes = Label::where('tag_group_id', 6)
                ->select('id', 'name_'.$this->lg.' as name')
                ->get()->toArray();

        $intss = $this->user->labels()->select('labels.id', 'user_labels.type')->where('user_labels.type','area')->get()->toArray();
        $this->interes_ids = Arr::pluck($intss, 'id');
        //dd($this->interes_ids);
        $this->avatares = [
            "/image/icons/profile.svg",
            "/image/icons/profile2.svg",
            "/image/icons/profile3.svg",
            "/image/icons/profile4.svg",
            "/image/icons/profile5.svg",
            "/image/icons/profile6.svg"
        ];

        
    }


    public function render()
    {
        
        return view('livewire.aplication.profile.profile');
    }


    public function updatedEmail($value){

        $values =  $this->validateOnly('email', [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($this->user['id']),
            ]
        ]);

    }

    public function update(){


        
        $rules = [ 
            'name'=>'required',
            'email'=>[
                'required',
                'email',
                Rule::unique('users')->ignore($this->user['id']),
            ],
            'profile_id'=>'required',
        ];
      
        
        $this->resetErrorBag();
        $this->validate($rules);

        try{    

            

            DB::transaction(function () {
                

                $user = User::find($this->user['id']);
                $user->name = $this->name;
                $user->last_name = $this->last_name;
                $user->email = $this->email;
                $user->photo = $this->photo;
                $user->save();


                UserDetail::where('user_id', $this->user['id'])
                ->update([
                    'profile_id'=>$this->profile_id,
                    'document_type_id'=>$this->document_type_id,
                    'document_number'=>$this->document_number,
                    'biography'=>$this->biography,
                    'cell_phone'=>$this->cell_phone,
                    'country'=>$this->country,
                    'city'=>$this->city,
                ]);

                $user->labels()->syncWithPivotValues($this->interes_ids, ['type' => 'area']);

                /* $this->emit('reloadGraph');
                $this->emit('reloadPhoto');
                $this->emit('reloadFullName'); */
                
               
                $this->dispatchBrowserEvent('showSweetAlertRediret', [
                    'title' => 'Bien',
                    'text' => 'Perfil actualizado',
                    'type' => 'success',
                    'btn' => 'ACEPTAR',
                    'url' => route('app.myprofile')
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error actualizando perfil. ERROR: '.$e->getMessage());
        }
    }
}
