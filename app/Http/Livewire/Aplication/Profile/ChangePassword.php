<?php

namespace App\Http\Livewire\Aplication\Profile;

use Livewire\Component;

class ChangePassword extends Component
{   

    public $repeat_new_password, $new_password, $current_password;

    public function render()
    {
        return view('livewire.aplication.profile.change-password');
    }

    public function changePassword(){

        

        $val = $this->validate([
            'repeat_new_password'=>'required|same:new_password',
            'new_password'=>'required|min:9',
            'current_password'=>'required|password'
        ],
        [
            'new_password.required'=>'Ingresa una contraseña.',
            'new_password.min'=>'La contraseña debe contener mínimo 9 dígitos o caracteres.',
            'current_password.required'=>'Ingresa tu actual contraseña.',
            'current_password.password'=>'Contraseña incorrecta.',
            'repeat_new_password.required'=>'Ingresa una contraseña.',
            'repeat_new_password.same'=>'Las contraseña no coinciden.',
        ]);

        $user = auth()->user();
        $user->password = bcrypt($val['new_password']);
        $user->save();
        
        $this->dispatchBrowserEvent('showSweetAlert', [
            'title' => 'Bien',
            'text' => 'Contraseña actualizada',
            'type' => 'success',
            'btn' => 'ACEPTAR',
        ]);

        $this->reset();
    }

}
