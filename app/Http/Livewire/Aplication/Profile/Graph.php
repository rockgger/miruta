<?php

namespace App\Http\Livewire\Aplication\Profile;

use App\Helpers\Helper;
use Livewire\Component;

class Graph extends Component
{   

    protected $listeners = ['reloadGraph' => 'reloadGraph'];

    public $ptg = 0;
    public $user;


    public function mount(){
        $prof = Helper::validateFullProfile($this->user);
        $this->ptg = $prof->ptg;
    }

    public function reloadGraph(){
        $prof = Helper::validateFullProfile($this->user);
        $this->ptg = $prof->ptg;
    }

    public function render()
    {   

        

        return view('livewire.aplication.profile.graph');
    }
}
