<?php

namespace App\Http\Livewire\Aplication\Home;

use Livewire\Component;

class Events extends Component
{
    public function render()
    {
        return view('livewire.aplication.home.events');
    }
}
