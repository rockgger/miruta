<?php

namespace App\Http\Livewire\Aplication\Home;

use App\Models\User;
use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\Opportunity;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class Oportunity extends Component
{   
    protected $listeners = [
        "loadOpportunities" => 'loadOpportunities'
    ];

    public $tapOption;
    public $readyToLoad = false;
    public $opportunities = [];
    public $limit = 10;
    public $offset = 0;
    public $opportunities_total = 0;

    public $categories_op =[];
    public $category_id;

    public function render()
    {   
        
        
        return view('livewire.aplication.home.oportunity'); 
    }

    public function loadOpportunities(){
        
        //$lg = app()->currentLocale();
        $lg = app()->getLocale();

        try{    
            if(count($this->categories_op) <= 0){
                $this->categories_op = Label::where('tag_group_id', 4)
                ->select('id', 'name_'.$lg.' as name')
                ->get()->toArray();
            }
            

            $this->opportunities_total = Opportunity::with(['actors'=>function($q) use($lg){
                $q->where('opportunity_labels.type', 'actors')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
            }])
            ->with(['categories' =>function($q3) use($lg){
                $q3->where('opportunity_labels.type', 'categories')
                ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
            }])
            ->whereHas('categories', function($q){
                if($this->category_id > 0){
                    $q->where('labels.id', $this->category_id);
                }
            })
            ->where('status_id', 13)
            ->count();


            if($this->opportunities_total > 0){
                $this->opportunities = Opportunity::with('openstatus', 'type')
                ->with(['actors'=>function($q) use($lg){
                    $q->where('opportunity_labels.type', 'actors')
                    ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
                }])
                ->with(['categories' =>function($q3) use($lg){
                    $q3->where('opportunity_labels.type', 'categories')
                    ->select('labels.id', 'labels.name_'.$lg.' as name', 'opportunity_labels.type');
                }])
                ->whereHas('categories', function($q){
                    if($this->category_id > 0){
                        $q->where('labels.id', $this->category_id);
                    }
                })
                ->where('status_id', 13)
                ->select('id', 'title_'.$lg.' as title', 'image', 'presentation_'.$lg.' as presentation', 'status_id', 'open_status_id', 'type_id', 'created_at')
                ->latest()
                ->offset(0)
                ->limit($this->limit)
                ->get()
                ->toArray();
            }

            $this->readyToLoad = true;

            //dd($this->opportunities);
        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error cargando lista de oportunidades app user. ERROR: '.$e->getMessage());
        }
    }

    public function updatedCategoryId(){
        $this->loadOpportunities();
    }
}
