<?php

namespace App\Http\Livewire\Aplication\Home;

use Livewire\Component;

class News extends Component
{
    public function render()
    {
        return view('livewire.aplication.home.news');
    }
}
