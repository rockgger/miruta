<?php

namespace App\Http\Livewire\Admin\Label;

use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CreateGroup extends Component
{   

    public $name_group;

    public function render()
    {
        return view('livewire.admin.label.create-group');
    }

    public function save(){
        $rules = [ 
            'name_group'=>'required',
        ];

        $this->validate($rules, [
            'name_group.required'=>'Campo requerido'
        ]);
        $this->resetErrorBag();

        try{
            $tg = new TagGroup();
            $tg->name_es = Str::ucfirst($this->name_group);
            $tg->uid = Str::slug($this->name_group,'_');
            $tg->save();

            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Bien',
                'text' => 'Grupo creado.',
                'type' => 'success',
                'btn' => 'ACEPTAR'
            ]);
            return redirect()->route('admin.label.edit', $tg->id);

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  

            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando grupo de etiqueta. ERROR: '.$e->getMessage());
        }
        
    }
}
