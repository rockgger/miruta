<?php

namespace App\Http\Livewire\Admin\Label;

use App\Models\Profile;
use Livewire\Component;
use App\Models\TagGroup;
use Illuminate\Support\Facades\Cache;

class LabelList extends Component
{   
    public $labels = [];
    public $loading = true;
    public $profile = 0;
    public $lg;

    

    protected $listeners = ["loadLabels" => 'loadLabels'];

    public function mount(){
        $this->lg = app()->getLocale();
    }

    public function render()
    {
        return view('livewire.admin.label.label-list');
    }

    public function loadLabels(){

        
        $this->profile = Cache::rememberForever('profiles-count', function () {
            return Profile::count();
        });

        /* $this->labels = Cache::rememberForever('profiles-count', function () {
            return Profile::count();
        }); */
        
        $this->labels = TagGroup::select('id', 'name_'.$this->lg.' as name')
        ->withCount('labels')
        ->get()
        ->toArray();
        //dd($this->labels);
        $this->loading = false;

    }
}
