<?php

namespace App\Http\Livewire\Admin\Label;

use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use Illuminate\Support\Str;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;

class EditGroup extends Component
{   
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public $tag_group_id, $name_group, $name_es, $name_en, $label_id; 

    public function mount(){
        $lbg = TagGroup::find($this->tag_group_id);
        $this->name_group = $lbg->name_es;
    }

    public function render()
    {   

        $labels = Label::where('tag_group_id', $this->tag_group_id)
        ->paginate(12);

        return view('livewire.admin.label.edit-group',[
            'labels'=>$labels
        ]);
    }

    public function save(){

        $rules = [ 
            'name_group'=>'required',
        ];

        $this->validate($rules, [
            'name_group.required'=>'Campo requerido'
        ]);
        $this->resetErrorBag();

        try{
            TagGroup::where('id', $this->tag_group_id)
            ->update([
                'name_es'=>Str::ucfirst($this->name_group),
                'uid'=>Str::slug($this->name_group,'_')
            ]);

            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Bien',
                'text' => 'Datos actualizados.',
                'type' => 'success',
                'btn' => 'ACEPTAR'
            ]);
            return;

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  

            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error guardando nombre de grupo de etiqueta. ERROR: '.$e->getMessage());
        }
    }

    public function restData(){
        $this->reset(['name_es','name_en']);
    }

    public function store($new = false){
        $rules = [ 
            'name_es'=>'required',
        ];

        $this->validate($rules, [
            'name_es.required'=>'Campo requerido'
        ]);
        $this->resetErrorBag();

        try{

            DB::transaction(function () use($new){
                $l = new Label();
                $l->name_es = Str::ucfirst($this->name_es);
                $l->name_en = Str::ucfirst($this->name_en);
                $l->tag_group_id = $this->tag_group_id;
                $l->save();
                
                $this->reset(['name_es','name_en']);
                if($new){
                    session()->flash('message.success', 'Nueva etiqueta creada.');
                    
                }else{
                    $this->dispatchBrowserEvent('closeModal', ['id' => 'idcreatelabel']);
                    $this->dispatchBrowserEvent('showSweetAlert', [
                        'title' => 'Bien',
                        'text' => 'Nueva etiqueta creada.',
                        'type' => 'success',
                        'btn' => 'ACEPTAR'
                    ]);
                }
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('closeModal', ['id' => 'idcreatelabel']);
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error guardando nombre etiqueta. ERROR: '.$e->getMessage());
        }
    }

    public function deleted(Label $label){
        try{
            DB::transaction(function () use($label){
                $label->delete();
               
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Etiqueta eliminada.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error eliminando etiqueta. ERROR: '.$e->getMessage());
        }
    }

    public function editProfile(Label $label){
        $this->name_es = $label->name_es;
        $this->name_en = $label->name_en;
        $this->label_id = $label->id;
    }
    

    public function update(){
        $rules = [ 
            'name_es'=>'required',
        ];

        $this->validate($rules, [
            'name_es.required'=>'Campo requerido'
        ]);
        $this->resetErrorBag();

        try{

            DB::transaction(function () {
                $p = Label::where('id',$this->label_id)
                ->update([
                    'name_es' => Str::ucfirst($this->name_es),
                    'name_en' => Str::ucfirst($this->name_en)
                ]);
                
                $this->dispatchBrowserEvent('closeModal', ['id' => 'ideditlabel']);
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Información actualizada.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('closeModal', ['id' => 'ideditlabel']);
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error actualizando nombre de etiqueta. ERROR: '.$e->getMessage());
        }
    }
}
