<?php

namespace App\Http\Livewire\Admin\Profiles;

use App\Helpers\Helper;
use App\Models\Profile;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class Edit extends Component
{   

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public $tag_group_id, $name_group, $name_es, $name_en, $profile_id;

    public function render()
    {   

        $profiles = Profile::paginate(12);

        return view('livewire.admin.profiles.edit',[
            'profiles'=>$profiles
        ]);
    }

    public function store($new = false){

        $rules = [ 
            'name_es'=>'required',
        ];

        $this->validate($rules, [
            'name_es.required'=>'Campo requerido'
        ]);
        $this->resetErrorBag();

        try{

            DB::transaction(function () use($new){
                $p = new Profile();
                $p->name_es = Str::ucfirst($this->name_es);
                $p->name_en = Str::ucfirst($this->name_en);
                $p->save();
                Cache::forget('profiles-count');
                $this->reset(['name_es','name_en']);
                if($new){
                    session()->flash('message.success', 'Nueva etiqueta creada.');
                    
                }else{
                    $this->dispatchBrowserEvent('closeModal', ['id' => 'idcreatelabel']);
                    $this->dispatchBrowserEvent('showSweetAlert', [
                        'title' => 'Bien',
                        'text' => 'Nueva etiqueta creada.',
                        'type' => 'success',
                        'btn' => 'ACEPTAR'
                    ]);
                }
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('closeModal', ['id' => 'idcreatelabel']);
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error guardando nombre de perfil (etiqueta). ERROR: '.$e->getMessage());
        }
    }


    public function restData(){
        $this->reset(['name_es','name_en']);
    }

    public function deleted(Profile $profile){
        try{
            DB::transaction(function () use($profile){
                $profile->delete();
                Cache::forget('profiles-count');
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Etiqueta eliminada.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error eliminando perfil (etiqueta). ERROR: '.$e->getMessage());
        }
    }


    public function editProfile(Profile $profile){
        $this->name_es = $profile->name_es;
        $this->name_en = $profile->name_en;
        $this->profile_id = $profile->id;
    }
    

    public function update(){
        $rules = [ 
            'name_es'=>'required',
        ];

        $this->validate($rules, [
            'name_es.required'=>'Campo requerido'
        ]);
        $this->resetErrorBag();

        try{

            DB::transaction(function () {
                $p = Profile::where('id',$this->profile_id)
                ->update([
                    'name_es' => Str::ucfirst($this->name_es),
                    'name_en' => Str::ucfirst($this->name_en)
                ]);
                
                $this->dispatchBrowserEvent('closeModal', ['id' => 'ideditlabel']);
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Información actualizada.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('closeModal', ['id' => 'ideditlabel']);
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error actualizando nombre de perfil (etiqueta). ERROR: '.$e->getMessage());
        }
    }

}
