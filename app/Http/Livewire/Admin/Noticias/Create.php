<?php

namespace App\Http\Livewire\Admin\Noticias;

use App\Models\Label;
use App\Helpers\Helper;
use App\Models\Noticia;
use Livewire\Component;
use App\Models\TagGroup;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use view;

class Create extends Component
{

    use WithFileUploads;
    
    public $image;
    
    public $list_categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [];
    public $url_forms, $title_es, $title_en, $body_es, $body_en, $summary_es, $summary_en;
    public $tag_group = [], $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id;


    public function mount(){
       
        $this->tag_group = TagGroup::select('id', 'name_es as name')->orderBy('name')->get()->toArray();
    }

    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }


    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }

    public function loadCategories($categories){

        $ids = Arr::pluck($categories, 'id');

        $ls = Label::where('tag_group_id', $this->categories_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_categories = $ls->toArray();
    }
    

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }
    
    public function render()
    {
        return view('livewire.admin.noticias.create');
    }


    public function store($public_status_id){
        //dd($this->body_es);

        $rules = [ 
            'title_es'=>'required',
            'summary_es'=>'max:240',
            'summary_en'=>'max:240',
        ];

        if ($this->image != null) {
            $rules['image'] = 'image|max:1024';
        }
        
        

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'summary_es.max'=>'Máximo 240 caracteres.',
            'summary_en.max'=>'Máximo 240 caracteres.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);

        try{    

            
            $categorias	 = Arr::pluck($this->categories_ids, 'id');
            $targets = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');


            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use( $categorias, $targets, $areas_de_interes, $url_image, $public_status_id){
                

                $noticia = new Noticia();
                
                $noticia->title_es = $this->title_es;
                $noticia->title_en = $this->title_en;

                $noticia->summary_es = $this->summary_es;
                $noticia->summary_en = $this->summary_en;

                if($url_image != null){
                    $noticia->image = $url_image;    
                }
                
                $noticia->status_id = $public_status_id;//publicado o borrador
                $noticia->user_id = auth()->user()->id; //usuario admin autor

                $noticia->body_es = htmlspecialchars($this->body_es, ENT_QUOTES); //deco "htmlspecialchars_decode();"
                $noticia->body_en = htmlspecialchars($this->body_en, ENT_QUOTES);

                $noticia->save();

                $noticia->categories()->attach($categorias, ['type'=>'categories']);
                $noticia->targets()->attach($targets, ['type'=>'targets']);
                $noticia->area()->attach($areas_de_interes, ['type'=>'area']);
                

                $msn = ($public_status_id == 13)?'Noticia guardada y publicada.':'Noticia guardada en borrador';
                $this->dispatchBrowserEvent('showSweetAlertRediret', [
                    'title' => 'Bien',
                    'text' => $msn,
                    'type' => 'success',
                    'btn' => 'ACEPTAR',
                    'url' => route('admin.news')
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando noticia. ERROR: '.$e->getMessage());
        }

    }
}
 