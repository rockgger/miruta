<?php

namespace App\Http\Livewire\Admin\Noticias;

use App\Models\Noticia;
use Livewire\Component;
use App\Models\NoticiaLabel;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;

class Noticias extends Component
{

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $open = true;
    public $search = '';
    public $order = 'DESC';

    protected $listeners = [
        "deleteNews" => 'deleteNews',
    ];
    
    public function render()
    {   

        $status_id = ($this->open)?13:16;
        $noticias = Noticia::with(['author'=>function($q){
            $q->select('id', 'name', 'last_name');
        }])
        ->where(function($query) {
            $query->where('title_es','like', '%'.$this->search.'%');
        })
        ->where('status_id', $status_id)
        ->select('id', 'title_es as title', 'status_id', 'user_id', 'created_at')
        ->orderBy('created_at',$this->order)
        ->paginate(12);

        return view('livewire.admin.noticias.noticias',[
            'noticias'=>$noticias
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingOrder()
    {
        $this->resetPage();
    }

    public function updatingOpen()
    {
        $this->resetPage();
    }


    public function deleteNews($id){
       
        //eliminar 
        DB::transaction(function () use($id){
            $o = Noticia::where('id', $id)->delete();           
            NoticiaLabel::where('new_id', $id)->delete();
            

            $this->dispatchBrowserEvent('showToast', [
                'heading' => '',
                'text' => 'Noticia eliminada',
                'icon' => 'success',
                'position' => 'top-right',
            ]);
            $this->resetPage();
        });
        
    }
}
