<?php

namespace App\Http\Livewire\Admin\Team;

use App\Models\Team;
use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use Illuminate\Support\Str;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ListTeam extends Component
{   
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
 
    public $photo;
    public $search = '';
    public $photo_temp;
    
    public $team_id, $testimony, $phrase, $area_id, $role, $email, $name, $tag_group_id, $labels = [], $tag_group = [];
    public function render()
    {

        $teams = Team::select('id', 'name', 'role', 'email','photo')
        ->where('name', 'like', '%'.$this->search.'%')
        ->orWhere('role', 'like', '%'.$this->search.'%')
        ->orWhere('email', 'like', '%'.$this->search.'%')
        ->latest()
        ->paginate(12);

        return view('livewire.admin.team.list-team',[
            'teams'=>$teams
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount(){
        $lg = app()->getLocale();
        $this->tag_group = TagGroup::select('id', 'name_'.$lg.' as name')->orderBy('name')->get()->toArray();
    }

    public function updatedPhoto()
    {
        $this->validate([
            'photo' => 'image|max:1024',
        ],[
            'photo.image'=>'Imagen no váida.',
            'photo.max'=>'Sube una foto de máximo 1MB.',
        ]);
    }

    public function store(){

        $this->resetErrorBag();

        $rule = [
            'name'=>'required',
            'role'=>'required',
            'email'=>'email'
        ];

        if ($this->photo != null){
            $rule = [
                'name'=>'required',
                'photo'=>'image|max:1024',
                'role'=>'required',
                'email'=>'email'
            ];
        }
        

        $message = [
            'name.required'=>'Campo requerido.',
            'photo.image'=>'Imagen no váida.',
            'photo.max'=>'Sube una foto de máximo 1MB.',
            'role.required'=>'Campo requerido.',
            'email.email'=>'Ingresa un email válido.',
        ];

        $this->validate($rule, $message);

        

        try{

            $url_image = null;
            if ($this->photo != null) {
                $r = Str::random(10);
                $info = pathinfo($this->photo->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->photo->storeAs('images/teams', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use($url_image){
                $t = new Team();
                $t->name = $this->name;
                $t->email = $this->email;
                $t->role = $this->role;
                $t->testimony = $this->testimony;
                $t->phrase = $this->phrase;
                $t->label_id = $this->area_id;
                if($url_image != null){
                    $t->photo = $url_image;
                }
                $t->save();
                
                $this->dispatchBrowserEvent('closeModal', ['id' => 'idcreatelabel']);
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Compañero creado.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);

                $this->restData();
                $this->reset(['photo']);
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('closeModal', ['id' => 'idcreatelabel']);
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando equipo de trabajo. ERROR: '.$e->getMessage());
        }
        
    }


    public function restData(){
        $this->reset(['name','testimony','phrase','area_id','role','email','tag_group_id']);
        $this->resetErrorBag();
    }


    public function updatedTagGroupId($id){
        
        $lg = app()->getLocale();
        $this->labels = Label::select('id', 'name_'.$lg.' as name')->where('tag_group_id', $id)->orderBy('name')->get()->toArray();
    }


    public function delete(Team $team){

        $team->delete();
        $this->resetPage();
        $this->dispatchBrowserEvent('showSweetAlert', [
            'title' => 'Bien',
            'text' => 'Compañero Eliminado.',
            'type' => 'success',
            'btn' => 'ACEPTAR'
        ]);
    }


    public function edit(Team $team){

        $this->team_id = $team->id;
        $this->testimony = $team->testimony;
        $this->phrase = $team->phrase;
        $this->area_id = $team->label_id;
        $this->role = $team->role;
        $this->email = $team->email;
        $this->name = $team->name;
        $this->photo_temp = $team->photo;
 
        
        $this->labels = Label::select('id', 'name_es as name')->where('tag_group_id', function($q){
            $q->select('tag_group_id')
            ->where('id', $this->area_id)
            ->from('labels');

        })->orderBy('name')->get()->toArray();
    }

    public function update(){
        $this->resetErrorBag();

        $rule = [
            'name'=>'required',
            'role'=>'required',
            'email'=>'email'
        ];

        if ($this->photo != null){
            $rule = [
                'name'=>'required',
                'photo'=>'image|max:1024',
                'role'=>'required',
                'email'=>'email'
            ];
        }
        

        $message = [
            'name.required'=>'Campo requerido.',
            'photo.image'=>'Imagen no váida.',
            'photo.max'=>'Sube una foto de máximo 1MB.',
            'role.required'=>'Campo requerido.',
            'email.email'=>'Ingresa un email válido.',
        ];

        $this->validate($rule, $message);

        try{

            $url_image = null;
            if ($this->photo != null) {
                $r = Str::random(10);
                $info = pathinfo($this->photo->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->photo->storeAs('images/teams', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use($url_image){
                $t = Team::find($this->team_id);
                $t->name = $this->name;
                $t->email = $this->email;
                $t->role = $this->role;
                $t->testimony = $this->testimony;
                $t->phrase = $this->phrase;
                $t->label_id = $this->area_id;
                if($url_image != null){
                    $t->photo = $url_image;
                }
                $t->save();
                
                $this->dispatchBrowserEvent('closeModal', ['id' => 'idupdatelabel']);
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Información actualizada.',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);

                $this->restData();
                $this->reset(['photo']);
            });

        }catch (\Exception $e){
            
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('closeModal', ['id' => 'idupdatelabel']);
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error actualizando equipo de trabajo. ERROR: '.$e->getMessage());
        }
    }
}
