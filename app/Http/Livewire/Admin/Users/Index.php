<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\Profile;
use App\Models\User;
use Livewire\Component;
use App\Models\UserDetail;
use Livewire\WithPagination;

class Index extends Component
{   

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $order = 'DESC';

    public $profile_id = 0, $profiles = [];
    public $profiles_total = [];
    public $users_total = 0;

    

    public function mount(){
        //datos de usuarios registrados totales y por perfil
        $this->profiles = Profile::select('id', 'name_es as name')->get()->toArray();
        foreach($this->profiles as $i => $pf){
            $this->profiles_total[$i]['name'] = $pf['name'];
            $this->profiles_total[$i]['count'] = UserDetail::where('profile_id', $pf['id'])->count();
        }
        $this->users_total = User::where('role_id', 1)->count();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingOrder()
    {
        $this->resetPage();
    }
    
    public function render()
    {   
        $users = User::where(function($query) {
            $query->where('users.name','like', '%'.$this->search.'%')
            ->orwhere('users.last_name','like', '%'.$this->search.'%')
            ->orwhere('users.email','like', '%'.$this->search.'%');
        })
        ->where('users.role_id', 1)
        ->join('user_details as ud', 'users.id', '=', 'ud.user_id')
        ->join('profiles as pr', 'ud.profile_id', '=', 'pr.id')
        ->select('users.id', 'users.name', 'users.last_name', 'pr.name_es as profile', 'users.created_at')
        ->where(function($q2) {
            if( $this->profile_id != 0){
                $q2->where('pr.id', $this->profile_id);
            }
        })
        ->orderBy('users.created_at',$this->order)
        ->paginate(12);

        return view('livewire.admin.users.index',[
            'users'=>$users
        ]);
    }

    

    public function modalInvite(){
        session()->reflash();
    }
}
