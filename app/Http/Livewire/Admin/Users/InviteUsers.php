<?php

namespace App\Http\Livewire\Admin\Users;

use App\Helpers\Helper;
use Livewire\Component;

class InviteUsers extends Component
{   

    public $add_users = [];

    public function render()
    {
        return view('livewire.admin.users.invite-users');
    }

    public function inviteUsers(){
        //dd($this->add_users);
        $rules = [ 
            'add_users'=>'required',
        ];

        $messages = [ 
            'add_users.required'=>'Campo requerido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);

        try{
            foreach ($this->add_users as  $us) {
                Helper::mail_invite_users($us);
            }
            session()->flash('message.success', "Invitación enviada.");
        }catch (\Exception $e){
            
        }
        
    }

    public function modalInvite(){
        session()->reflash();
    }
}
