<?php

namespace App\Http\Livewire\Admin\Opportunities;

use Carbon\Carbon;
use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Models\OpportunityHiring;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HiringEdit extends Component
{   
    use WithFileUploads;

    public $opportunity_id, $open, $status, $hiring;
    public $opportunity;
    public $image;
    public $start_date;
    public $end_date;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [], $type_contract_ids, $list_type_contract = [], $type_contract = [];
    public $url_forms, $title_es, $title_en, $object_contract_es, $object_contract_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id, $type_contract_tag_group_id;

    public $emails_notofy = [];

    public $salary;

    public $attachments = [], $attach;


    public function mount(){
        $this->opportunity = Opportunity::with('hiring')
        ->with(['labels'=>function($q){
            $q->select('labels.id', 'labels.name_es as name','labels.tag_group_id', 'opportunity_labels.type');
        }])
        ->find($this->opportunity_id)->toArray();

        $this->hiring = $this->opportunity['hiring'];

        $this->title_es = $this->opportunity['title_es'];
        $this->title_en = $this->opportunity['title_en'];
        $this->open = ($this->opportunity['open_status_id'] == 3)?true:false;
        $this->status = ($this->opportunity['status_id'] == 13)?true:false;
        $this->object_contract_es = $this->opportunity['presentation_es'];
        $this->object_contract_en = $this->opportunity['presentation_en'];

        $this->start_date = date('Y-m-d',strtotime($this->hiring['start_date']));
        $this->end_date = date('Y-m-d',strtotime($this->hiring['end_date']));
        
        $this->url_forms = $this->hiring['url_forms'];
        $this->salary = $this->hiring['salary'];

        $labels = $this->opportunity['labels'];
        

        //ENTIDADES
        $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        }));

        //CATEGORÍAS
        $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        }));

        //DIRIGIDO A
        $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        }));

        //ÁREA DE INTERES
        $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        }));

        $grup_labels_id = [];
        if(count($actors) > 0){
            $grup_labels_id[] = $actors[0]['tag_group_id'];
        }

        if(count($categories) > 0){
            $grup_labels_id[] = $categories[0]['tag_group_id'];
        }

        if(count($targets) > 0){
            $grup_labels_id[] = $targets[0]['tag_group_id'];
        }

        if(count($area) > 0){
            $grup_labels_id[] = $area[0]['tag_group_id'];
        }
        

        $this->tag_group = TagGroup::with(['labels' => function($q) use($grup_labels_id){
            $q->select('labels.id', 'labels.name_es as name', 'labels.tag_group_id')
            ->whereIn('tag_group_id', $grup_labels_id)
            ->orderBy('labels.name_es');
        }])
        ->select('id', 'name_es as name')
        ->orderBy('name')
        ->get()->toArray();

        

        //ENTIDADES
        /* $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        })); */
        if(count($actors) > 0){
            $this->entities_ids = $actors;
            $this->tag_group_id = $actors[0]['tag_group_id'];
        }
           
        //CATEGORÍAS
        /* $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        })); */
        $this->categories_ids = $categories;
        if(count($this->categories_ids) > 0){
            $ct_temp = Arr::first($this->tag_group, function ($g, $key) use($categories){
                return $g['id'] == $categories[0]['tag_group_id'];
            });
            $this->categories = $ct_temp['labels'];
            $this->categories_tag_group_id = $this->categories[0]['tag_group_id'];
            $this->categories_ids = Arr::pluck($categories, 'id');
        }
        

        //DIRIGIDO A
        /* $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        })); */
        $this->dirigida_ids = $targets;
        if(count($this->dirigida_ids) > 0){
            $dt_temp = Arr::first($this->tag_group, function ($g, $key) use($targets){
                return $g['id'] == $targets[0]['tag_group_id'];
            });
            $this->dirigida = $dt_temp['labels'];
            $this->dirigida_tag_group_id = $this->dirigida[0]['tag_group_id'];
            $this->dirigida_ids = Arr::pluck($targets, 'id');
        }

        //ÁREA DE INTERES
        /* $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        })); */
        if(count($area) > 0){
            $this->interes_ids = $area;
            $this->interes_tag_group_id = $area[0]['tag_group_id'];
        }

        //NOTIFICACIÓN EMAIL
        $this->emails_notofy = json_decode($this->hiring["emails_notofy"]);

        //TIPO DE CONRATO
        $this->type_contract_ids = $this->hiring['type_contract'];
        if($this->type_contract_ids != null){
            $this->type_contract = Label::select('id', 'name_es as name', 'tag_group_id')
            ->where('tag_group_id', function($q){
                $q->select('tag_group_id')
                ->where('id', $this->type_contract_ids)
                ->from('labels');
            })
            ->orderBy('name')
            ->get()
            ->toArray();
            
            $this->type_contract_tag_group_id = $this->type_contract[0]['tag_group_id'];
        }

        //ADJUNTOS
        $this->attachments = json_decode($this->hiring["attachments"], true);
        //dd($this->attachments);
    }


    //cambiar estdo de abierto y cerrado
    public function  updatingOpen($val){
        $id_change = ($val)?3:4;
        opportunity::where('id',$this->opportunity_id)
        ->update([
            'open_status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function  updatingStatus($val){
        $id_change = ($val)?13:16;
        opportunity::where('id',$this->opportunity_id)
        ->update([
            'status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }

    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }

    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }

    

    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedTypeContractTagGroupId($id){
        
        $lg = 'es';
        $this->type_contract = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->get()
        ->toArray();
    }

    public function updatedAttach(){
        $this->resetErrorBag();
        if ($this->attach != null) {
            foreach($this->attach as $at){
                $r = Str::random(10);
                $info = pathinfo($at->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $at->storeAs('images', $name, 'public2');
                $att = Storage::disk('public2')->url($res);
                $this->attachments[] = [
                    'name'=>$at->getClientOriginalName(),
                    'url'=>$att
                ];
            }
        }
    }

    public function removeAttach($i){
        unset($this->attachments[$i]);
        $this->attachments = array_values($this->attachments);
    }



    public function render()
    {
        return view('livewire.admin.opportunities.hiring-edit');
    }

    public function update(){
        $rules = [ 
            'title_es'=>'required',
            'start_date'=>'required|date',
            'end_date'=>'required|date|after_or_equal:start_date',
            'url_forms'=>'url',
        ];

        if ($this->image != null) {
            $rules = [ 
                'title_es'=>'required',
                'image'=>'image|max:1024',
                'start_date'=>'required|date',
                'end_date'=>'required|date|after_or_equal:start_date',
                'url_forms'=>'url',
            ];
        }

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'start_date.required'=>'Campo requerido.',
            'start_date.date'=>'Formato no válido.',
            'end_date.required'=>'Campo requerido.',
            'end_date.date'=>'Formato no válido.',
            'end_date.after_or_equal'=>'La fecha debe ser menor a la "fecha de inicio".',
            'url_forms.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);
        
        try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');

            
            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $url_image){
                
                $op = Opportunity::find($this->opportunity_id);
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->object_contract_es;
                $op->presentation_en = $this->object_contract_en;
                if($url_image != null){
                    $op->image = $url_image;
                }
                $op->save();

                OpportunityHiring::where('id', $this->hiring['id'])
                ->update([
                    'start_date'=> new Carbon($this->start_date),
                    'end_date'=>  new Carbon($this->end_date),
                    'url_forms'=> $this->url_forms,
                    'type_contract'=> $this->type_contract_ids,
                    'emails_notofy'=> json_encode($this->emails_notofy),
                    'salary'=> $this->salary,
                    'attachments'=> json_encode($this->attachments),
                ]);

                $op->labels()->detach();

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                $op->area()->attach($areas_de_interes, ['type'=>'area']);

                $msn = 'Oportunidad actualizada';
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => $msn,
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error editando oportunidad contratación. ERROR: '.$e->getMessage());
        }
    }
}
