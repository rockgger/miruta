<?php

namespace App\Http\Livewire\Admin\Opportunities;

use App\Models\Label;
use App\Helpers\Helper;
use App\Models\Noticia;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Models\OpportunityLanding;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LandingEdit extends Component
{   

    use WithFileUploads;
    
    public $opportunity_id, $open, $status, $landing;
    public $cover_image;
    public $start_date;
    public $end_date;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [], $aliados_ids = [], $list_aliados = [];
    public $url_web, $url_video, $context_image, $option_context = 'video', $summary_es, $summary_en, $title_es, $title_en, $description_es, $description_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id, $aliados_tag_group_id;

    //beneficios
    public $benefit_es = [], $benefit_en = [], $benefit_image;

    //como funciona
    public $funtion_es = [], $funtion_en = [], $funtion_image;
    //Programas y servicios
    public $services_es = [], $services_en = [], $img_service_es, $id_file_input_es, $img_url_service_es, $img_service_en, $id_file_input_en, $img_url_service_en;
    //Acordeón
    public $accordion_es = [], $accordion_en = [], $title_accordion_es, $title_accordion_en;

    public $context;

    public $list_noticias = [], $noticias_ids = [], $total_noticias = 0; 

    public $limit = 8;
    public $offset = 0;

    public function mount(){
       
        

        $this->opportunity = Opportunity::with('landing')
        ->with(['labels'=>function($q){
            $q->select('labels.id', 'labels.name_es as name','labels.tag_group_id', 'opportunity_labels.type');
        }])
        ->with(['associated_noticias' => function($q){
            $q->select('noticias.id','noticias.title_es as title', 'noticias.status_id', 'noticias.summary_es as summary', 'noticias.created_at');
        }])
        ->find($this->opportunity_id)->toArray();
        $this->landing = $this->opportunity['landing'];

        $this->title_es = $this->opportunity['title_es'];
        $this->title_en = $this->opportunity['title_en'];
        $this->status = ($this->opportunity['status_id'] == 13)?true:false;
        $this->open = ($this->opportunity['open_status_id'] == 3)?true:false;
        $this->description_es = $this->opportunity['presentation_es'];
        $this->description_en = $this->opportunity['presentation_en']; 

        $this->url_web = $this->landing['url_web'];
        $this->summary_es = $this->landing['summary_es'];
        $this->summary_en = $this->landing['summary_en'];

        
        
        $this->landing['context'] = json_decode($this->landing['context']);

        $this->option_context = $this->landing['context']->option;
        if($this->option_context == 'video'){
            $this->url_video = $this->landing['context']->url;
        }else{
            $this->context = $this->landing['context']->url;
        }

        
        $labels = $this->opportunity['labels'];

        //ENTIDADES
        $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        }));

        //CATEGORÍAS
        $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        }));

        //DIRIGIDO A
        $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        }));

        //ÁREA DE INTERES
        $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        }));

        $grup_labels_id = [];
        if(count($actors) > 0){
            $grup_labels_id[] = $actors[0]['tag_group_id'];
        }

        if(count($categories) > 0){
            $grup_labels_id[] = $categories[0]['tag_group_id'];
        }

        if(count($targets) > 0){
            $grup_labels_id[] = $targets[0]['tag_group_id'];
        }

        if(count($area) > 0){
            $grup_labels_id[] = $area[0]['tag_group_id'];
        }
        

        $this->tag_group = TagGroup::with(['labels' => function($q) use($grup_labels_id){
            $q->select('labels.id', 'labels.name_es as name', 'labels.tag_group_id')
            ->whereIn('tag_group_id', $grup_labels_id)
            ->orderBy('labels.name_es');
        }])
        ->select('id', 'name_es as name')
        ->orderBy('name')
        ->get()->toArray();

        

        //ENTIDADES
        /* $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        })); */
        if(count($actors) > 0){
            $this->entities_ids = $actors;
            $this->tag_group_id = $actors[0]['tag_group_id'];
        }
           
        //CATEGORÍAS
        /* $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        })); */
        $this->categories_ids = $categories;
        if(count($this->categories_ids) > 0){
            $ct_temp = Arr::first($this->tag_group, function ($g, $key) use($categories){
                return $g['id'] == $categories[0]['tag_group_id'];
            });
            $this->categories = $ct_temp['labels'];
            $this->categories_tag_group_id = $this->categories[0]['tag_group_id'];
            $this->categories_ids = Arr::pluck($categories, 'id');
        }
        

        //DIRIGIDO A
        /* $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        })); */
        $this->dirigida_ids = $targets;
        if(count($this->dirigida_ids) > 0){
            $dt_temp = Arr::first($this->tag_group, function ($g, $key) use($targets){
                return $g['id'] == $targets[0]['tag_group_id'];
            });
            $this->dirigida = $dt_temp['labels'];
            $this->dirigida_tag_group_id = $this->dirigida[0]['tag_group_id'];
            $this->dirigida_ids = Arr::pluck($targets, 'id');
        }

        //ÁREA DE INTERES
        /* $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        })); */
        if(count($area) > 0){
            $this->interes_ids = $area;
            $this->interes_tag_group_id = $area[0]['tag_group_id'];
        }


        //COMO FUNCIONA
        $how_functions = json_decode($this->landing['how_functions'], true);
        $this->funtion_es = $how_functions['es'];
        $this->funtion_en = $how_functions['en'];

        //PROGRAMAS Y SERVICIOS
        $programs_services = json_decode($this->landing['programs_services'], true);
        $this->services_es = $programs_services['es'];
        $this->services_en = $programs_services['en'];

        //TITULO ACORDEON
        $this->title_accordion_es = $this->landing['title_accordion_es'];
        $this->title_accordion_en = $this->landing['title_accordion_en'];

        //ACORDEONES
        $accordion = json_decode($this->landing['accordion'], true);
        $this->accordion_es = $accordion['es'];
        $this->accordion_en = $accordion['en'];

        //BENEFICIOS
        $benefit = json_decode($this->landing['benefit'], true);
        $this->benefit_es = $benefit['es'];
        $this->benefit_en = $benefit['en'];

        //NOticias. ASOCIADAS
        $lsnoticias = $this->opportunity['associated_noticias'];
        if(count($lsnoticias) > 0){
            $this->noticias_ids = $lsnoticias;
        }
        
    }


    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }

    //cambiar estdo de abierto y cerrado
    public function  updatingOpen($val){
        $id_change = ($val)?3:4;
        Opportunity::where('id',$this->opportunity_id)
        ->update([
            'open_status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }

    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }


    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedCoverImage(){
        $this->resetErrorBag();
    }

    public function updatedImgServiceEs()
    {
        $this->validate([
            'img_service_es' => 'image|max:1024',
        ]);

        //subir img y devolver url        
        $r = Str::random(10);
        $info = pathinfo($this->img_service_es->getClientOriginalName());
        $ext = $info['extension'];
        $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
        $res = $this->img_service_es->storeAs('images', $name, 'public2');

        
        $this->img_url_service_es = Storage::disk('public2')->url($res);
        $this->id_file_input_es = Str::random(5);
    }

    public function updatedImgServiceEn()
    {
        $this->validate([
            'img_service_en' => 'image|max:1024',
        ]);

        //subir img y devolver url        
        $r = Str::random(10);
        $info = pathinfo($this->img_service_en->getClientOriginalName());
        $ext = $info['extension'];
        $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
        $res = $this->img_service_en->storeAs('images', $name, 'public2');

        
        $this->img_url_service_en = Storage::disk('public2')->url($res);
        $this->id_file_input_en = Str::random(5);
    }

    public function clearImg(){
        $this->reset(['img_service_es','img_url_service_es', 'img_service_en','img_url_service_en']);
        $this->id_file_input_es = Str::random(5);
        $this->id_file_input_en = Str::random(5);
    }
    
    public function  updatingStatus($val){
        $id_change = ($val)?13:16;
        Opportunity::where('id',$this->opportunity_id)
        ->update([
            'status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function update(){
        $rules = [ 
            'title_es'=>'required',
            'url_web'=>'url',
            'summary_es'=>'max:250',
            'summary_en'=>'max:250',
        ];

        if ($this->cover_image != null) {
            $rules['cover_image'] = 'image|max:1024';
        }

        if ($this->benefit_image != null) {
            $rules['benefit_image'] = 'image|max:1024';
        }

        if ($this->funtion_image != null) {
            $rules['funtion_image'] = 'image|max:1024';
        }
        
        if ($this->context_image != null) {
            $rules['context_image'] = 'image|max:1024';
        }

        if ($this->url_video != null) {
            $rules['url_video'] = 'url';
        }
        
        

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'cover_image.image'=>'Imagen no válida.',
            'cover_image.max'=>'Máximo 1MB.',
            'benefit_image.image'=>'Imagen no válida.',
            'benefit_image.max'=>'Máximo 1MB.',
            'funtion_image.image'=>'Imagen no válida.',
            'funtion_image.max'=>'Máximo 1MB.',
            'url_web.url'=>'Formato de url no válido.',
            'summary_es.max'=>'Máximo 250 caracteres.',
            'summary_en.max'=>'Máximo 250 caracteres.',
            'context_image.image'=>'Imagen no válida.',
            'context_image.max'=>'Máximo 1MB.',
            'url_video.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);

        try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');
            
            $noticias_ids = Arr::pluck($this->noticias_ids, 'id');

            $benefit = json_encode([
                'es'=>$this->benefit_es,
                'en'=>$this->benefit_en,
            ]);

            $functions = json_encode([
                'es'=>$this->funtion_es,
                'en'=>$this->funtion_en,
            ]);


            $services = json_encode([
                'es'=>$this->services_es,
                'en'=>$this->services_en,
            ]);

            $accordions = json_encode([
                'es'=>$this->accordion_es,
                'en'=>$this->accordion_en,
            ]);

            
            
            $url_cover_image = null;
            if ($this->cover_image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->cover_image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->cover_image->storeAs('images', $name, 'public2');
                $url_cover_image = Storage::disk('public2')->url($res);
            }

            $url_benefit_image = $this->landing['url_benefit_image'];
            if ($this->benefit_image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->benefit_image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->benefit_image->storeAs('images', $name, 'public2');
                $url_benefit_image = Storage::disk('public2')->url($res);
            }

            $url_funtion_image = $this->landing['url_funtion_image'];
            if ($this->funtion_image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->funtion_image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->funtion_image->storeAs('images', $name, 'public2');
                $url_funtion_image = Storage::disk('public2')->url($res);
            }

            $url_context_image = ($this->landing['context']['option'] == 'image')?$this->context:null;
            if (($this->context_image != null) AND ($this->option_context == 'image')) {
                $r = Str::random(10);
                $info = pathinfo($this->context_image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->context_image->storeAs('images', $name, 'public2');
                $url_context_image = Storage::disk('public2')->url($res);
            }


            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $functions, $benefit, $accordions, $services, $url_cover_image,  $url_benefit_image, $url_funtion_image, $url_context_image, $noticias_ids){
                
                $op = Opportunity::find($this->opportunity_id);
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->description_es;
                $op->presentation_en = $this->description_en;
                if($url_cover_image != null){
                    $op->image = $url_cover_image;
                }
                $op->save();

                OpportunityLanding::where('id', $this->landing['id'])
                ->update([
                    'summary_es'=> $this->summary_es,
                    'summary_en'=> $this->summary_en,
                    'url_web'=> $this->url_web,
                    'benefit'=> $benefit,
                    'how_functions'=> $functions,
                    'programs_services'=> $services,
                    'context'=> json_encode([
                        'option'=> $this->option_context,
                        'url' => ($this->option_context == 'video')?$this->url_video:$url_context_image
                    ]),
                    'title_accordion_es'=> $this->title_accordion_es,
                    'title_accordion_en'=> $this->title_accordion_en,
                    'accordion'=> $accordions,
                    'url_funtion_image'=> $url_funtion_image,
                    'url_benefit_image'=> $url_benefit_image,
                ]);

                $op->associated_noticias()->sync($noticias_ids);
                
                $op->labels()->detach();

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                $op->area()->attach($areas_de_interes, ['type'=>'area']);
                

                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Oportunidad actualizada',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando oportunidad landing. ERROR: '.$e->getMessage());
        }
    }


    public function render()
    {
        return view('livewire.admin.opportunities.landing-edit');
    }

    public function loadNoticias($noticias){


        $this->total_noticias = Noticia::where('status_id', 13)
        ->count();

        $ids = Arr::pluck($noticias, 'id');
        
        $ls = Noticia::whereNotIn('id', $ids)
        ->orderBy('created_at','desc')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'summary_es as summary', 'created_at')
        ->offset(0)
        ->limit($this->limit)
        ->get();

        $this->list_noticias = $ls->toArray();
    }


    public function loadModeNoticias($noticias){

        $ids = Arr::pluck($noticias, 'id');

        $this->offset = count($this->list_noticias);

        $ls = Noticia::whereNotIn('id', $ids)
        ->orderBy('created_at','desc')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'summary_es as summary', 'created_at')
        ->offset($this->offset)
        ->limit($this->limit)
        ->get();

        $this->list_noticias = array_merge($this->list_noticias, $ls->toArray());

         
    }
}
