<?php

namespace App\Http\Livewire\Admin\Opportunities;

use Carbon\Carbon;
use App\Models\Label;
use App\Helpers\Helper;
use App\Models\Noticia;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use App\Models\OpportunityStandard;
use Illuminate\Support\Facades\Storage;

class StandardCreate extends Component
{   
    use WithFileUploads;
    
    //public $lang = "es";
    public $image;
    public $start_date;
    public $end_date;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [], $aliados_ids = [], $list_aliados = [];
    public $url_forms, $title_es, $title_en, $presentation_es, $presentation_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id, $aliados_tag_group_id;

    //beneficios
    public $benefit_es = [], $benefit_en = [];
    //cronograma actividades
    public $activity_es = [], $activity_en = [];

    public $list_noticias = [], $noticias_ids = [], $total_noticias = 0; 

    public $limit = 8;
    public $offset = 0;

    public function mount(){
       
        $this->tag_group = TagGroup::select('id', 'name_es as name')->orderBy('name')->get()->toArray();

        
    }


    public function render()
    {
        return view('livewire.admin.opportunities.standard-create');
    }


    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }

    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }

    public function loadAliados($aliados){

        $ids = Arr::pluck($aliados, 'id');

        $ls = Label::where('tag_group_id', $this->aliados_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_aliados = $ls->toArray();
        
    }

    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function store($public_status_id){
        

        $rules = [ 
            'title_es'=>'required',
            'start_date'=>'required|date',
            'end_date'=>'required|date|after_or_equal:start_date',
            'url_forms'=>'url',
        ];

        if ($this->image != null) {
            $rules = [ 
                'title_es'=>'required',
                'image'=>'image|max:1024',
                'start_date'=>'required|date',
                'end_date'=>'required|date|after_or_equal:start_date',
                'url_forms'=>'url',
            ];
        }

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'start_date.required'=>'Campo requerido.',
            'start_date.date'=>'Formato no válido.',
            'end_date.required'=>'Campo requerido.',
            'end_date.date'=>'Formato no válido.',
            'end_date.after_or_equal'=>'La fecha debe ser menor a la "fecha de inicio".',
            'url_forms.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);
        
        try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');
            $aliados = Arr::pluck($this->aliados_ids, 'id');

            $noticias_ids = Arr::pluck($this->noticias_ids, 'id');
            
            $benefit = json_encode([
                'es'=>$this->benefit_es,
                'en'=>$this->benefit_en,
            ]);

            $activities = json_encode([
                'es'=>$this->activity_es,
                'en'=>$this->activity_en,
            ]);
            
            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $aliados, $benefit, $activities, $url_image, $public_status_id, $noticias_ids){
                
                $op = new Opportunity();
                
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->presentation_es;
                $op->presentation_en = $this->presentation_en;
                if($url_image != null){
                    $op->image = $url_image;
                }
                $op->type_id = 1; //tipo estándar
                $op->status_id = $public_status_id;//publicado o borrador
                $op->open_status_id = 3; //oportunidad abierta
                $op->user_id = auth()->user()->id; //usuario admin autor
                $op->save();

                $standar = new OpportunityStandard();
                $standar->opportunity_id = $op->id;

                $standar->start_date = new Carbon($this->start_date);
                $standar->end_date = new Carbon($this->end_date);
                $standar->url_forms = $this->url_forms;
                $standar->benefit = $benefit;
                $standar->schedule_activities = $activities;
                $standar->save();

                $op->associated_noticias()->attach($noticias_ids);

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                
                $op->area()->attach($areas_de_interes, ['type'=>'area']);
                $op->partners()->attach($aliados, ['type'=>'partners']);
                

                

                

                $msn = ($public_status_id == 13)?'Oportunidad guardada y publicada.':'Oportunidad guardada en borrador';
                $this->dispatchBrowserEvent('showSweetAlertRediret', [
                    'title' => 'Bien',
                    'text' => $msn,
                    'type' => 'success',
                    'btn' => 'ACEPTAR',
                    'url' => route('admin.opportunity.standard')
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando oportunidad estandar. ERROR: '.$e->getMessage());
        }

    }

    public function loadNoticias($noticias){


        $this->total_noticias = Noticia::where('status_id', 13)
        ->count();

        $ids = Arr::pluck($noticias, 'id');
        
        $ls = Noticia::whereNotIn('id', $ids)
        ->orderBy('created_at','desc')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'summary_es as summary', 'created_at')
        ->offset(0)
        ->limit($this->limit)
        ->get();

        $this->list_noticias = $ls->toArray();
    }


    public function loadModeNoticias($noticias){

        $ids = Arr::pluck($noticias, 'id');

        $this->offset = count($this->list_noticias);

        $ls = Noticia::whereNotIn('id', $ids)
        ->orderBy('created_at','desc')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'summary_es as summary', 'created_at')
        ->offset($this->offset)
        ->limit($this->limit)
        ->get();

        $this->list_noticias = array_merge($this->list_noticias, $ls->toArray());

         
    }
}
