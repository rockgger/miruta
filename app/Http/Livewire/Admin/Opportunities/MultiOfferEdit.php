<?php

namespace App\Http\Livewire\Admin\Opportunities;

use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Models\OpportunityLanding;
use Illuminate\Support\Facades\DB;
use App\Models\OpportunityMultioffer;
use Illuminate\Support\Facades\Storage;

class MultiOfferEdit extends Component
{   

    use WithFileUploads;
    
    public $opportunity_id, $open, $status, $multioffer;
    public $image;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [];

    public $url_video, $context_image, $option_context = 'video', $title_es, $title_en, $presentation_es, $presentation_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id;

    //Grupos de ofertas
    public $title_group_offer_es, $title_group_offer_en, $description_group_offer_es, $description_group_offer_en;

    public $list_opportunities = [], $opportunities_ids = [], $total_opportunities = 0; 

    public $limit = 10;
    public $offset = 0;

    public function mount(){
        $this->opportunity = Opportunity::with(['multioffer.associated_opportunities' => function($q){
            $q->select('opportunities.id', 'opportunities.type_id', 'opportunities.title_es as title', 'opportunities.status_id', 'opportunities.open_status_id', 'opportunities.created_at');
        }])
        ->with(['labels'=>function($q){
            $q->select('labels.id', 'labels.name_es as name','labels.tag_group_id', 'opportunity_labels.type');
        }])
        ->find($this->opportunity_id)->toArray();
       
        $this->multioffer = $this->opportunity['multioffer'];

        $this->title_es = $this->opportunity['title_es'];
        $this->title_en = $this->opportunity['title_en'];
        $this->status = ($this->opportunity['status_id'] == 13)?true:false;
        $this->open = ($this->opportunity['open_status_id'] == 3)?true:false;
        $this->presentation_es = $this->opportunity['presentation_es'];
        $this->presentation_en = $this->opportunity['presentation_en'];
        
        $this->title_group_offer_es = $this->multioffer['title_group_offer_es'];
        $this->title_group_offer_en = $this->multioffer['title_group_offer_en'];

        $this->description_group_offer_es = $this->multioffer['description_group_offer_es'];
        $this->description_group_offer_en = $this->multioffer['description_group_offer_en'];

        
        
        
        $this->multioffer['context'] = json_decode($this->multioffer['context']);

        $this->option_context = $this->multioffer['context']->option;
        if($this->option_context == 'video'){
            $this->url_video = $this->multioffer['context']->url;
        }else{
            $this->context = $this->multioffer['context']->url;
        }



        $labels = $this->opportunity['labels'];

        //ENTIDADES
        $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        }));

        //CATEGORÍAS
        $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        }));

        //DIRIGIDO A
        $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        }));

        //ÁREA DE INTERES
        $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        }));

        $grup_labels_id = [];
        if(count($actors) > 0){
            $grup_labels_id[] = $actors[0]['tag_group_id'];
        }

        if(count($categories) > 0){
            $grup_labels_id[] = $categories[0]['tag_group_id'];
        }

        if(count($targets) > 0){
            $grup_labels_id[] = $targets[0]['tag_group_id'];
        }

        if(count($area) > 0){
            $grup_labels_id[] = $area[0]['tag_group_id'];
        }
        

        $this->tag_group = TagGroup::with(['labels' => function($q) use($grup_labels_id){
            $q->select('labels.id', 'labels.name_es as name', 'labels.tag_group_id')
            ->whereIn('tag_group_id', $grup_labels_id)
            ->orderBy('labels.name_es');
        }])
        ->select('id', 'name_es as name')
        ->orderBy('name')
        ->get()->toArray();

        

        //ENTIDADES
        /* $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        })); */
        if(count($actors) > 0){
            $this->entities_ids = $actors;
            $this->tag_group_id = $actors[0]['tag_group_id'];
        }
           
        //CATEGORÍAS
        /* $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        })); */
        $this->categories_ids = $categories;
        if(count($this->categories_ids) > 0){
            $ct_temp = Arr::first($this->tag_group, function ($g, $key) use($categories){
                return $g['id'] == $categories[0]['tag_group_id'];
            });
            $this->categories = $ct_temp['labels'];
            $this->categories_tag_group_id = $this->categories[0]['tag_group_id'];
            $this->categories_ids = Arr::pluck($categories, 'id');
        }
        

        //DIRIGIDO A
        /* $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        })); */
        $this->dirigida_ids = $targets;
        if(count($this->dirigida_ids) > 0){
            $dt_temp = Arr::first($this->tag_group, function ($g, $key) use($targets){
                return $g['id'] == $targets[0]['tag_group_id'];
            });
            $this->dirigida = $dt_temp['labels'];
            $this->dirigida_tag_group_id = $this->dirigida[0]['tag_group_id'];
            $this->dirigida_ids = Arr::pluck($targets, 'id');
        }

        //ÁREA DE INTERES
        /* $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        })); */
        if(count($area) > 0){
            $this->interes_ids = $area;
            $this->interes_tag_group_id = $area[0]['tag_group_id'];
        }


        //OPORTU. ASOCIADAS
        $lc_opp_ids = $this->multioffer['associated_opportunities'];
        if(count($lc_opp_ids) > 0){
            $this->opportunities_ids = $lc_opp_ids;
        }

    }


    public function  updatingStatus($val){
        $id_change = ($val)?13:16;
        Opportunity::where('id',$this->opportunity_id)
        ->update([
            'status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }

    //cambiar estdo de abierto y cerrado
    public function  updatingOpen($val){
        $id_change = ($val)?3:4;
        Opportunity::where('id',$this->opportunity_id)
        ->update([
            'open_status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }

    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }


    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedImage(){
        $this->resetErrorBag();
    }

    
    public function loadOpportunities($opportunities){


        $this->total_opportunities = OpportunityLanding::where('status_id', 13)
        ->count();

        $ids = Arr::pluck($opportunities, 'id');
        
        $ls = OpportunityLanding::whereNotIn('id', $ids)
        ->orderBy('title_es')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'open_status_id', 'created_at')
        ->offset(0)
        ->limit($this->limit)
        ->get();

        $this->list_opportunities = $ls->toArray();
    }

    public function loadModeOpportunities($opportunities){

        $ids = Arr::pluck($opportunities, 'id');

        $this->offset = count($this->list_opportunities);

        $ls = OpportunityLanding::whereNotIn('id', $ids)
        ->orderBy('title_es')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'open_status_id', 'created_at')
        ->offset($this->offset)
        ->limit($this->limit)
        ->get();

        $this->list_opportunities = array_merge($this->list_opportunities, $ls->toArray());

         
    }

    public function update(){
        
        $rules = [ 
            'title_es'=>'required',
        ];

        if ($this->image != null) {
            $rules['image'] = 'image|max:1024';
        }
        
        if ($this->context_image != null) {
            $rules['context_image'] = 'image|max:1024';
        }

        if ($this->url_video != null) {
            $rules['url_video'] = 'url';
        }
        
        

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'context_image.image'=>'Imagen no válida.',
            'context_image.max'=>'Máximo 1MB.',
            'url_video.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);
        
        //try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');
            
            $opportunities_landing_ids = Arr::pluck($this->opportunities_ids, 'id');

                       
            
            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }


            $url_context_image = ($this->multioffer['context']['option'] == 'image')?$this->context:null;
            if (($this->context_image != null) AND ($this->option_context == 'image')) {
                $r = Str::random(10);
                $info = pathinfo($this->context_image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->context_image->storeAs('images', $name, 'public2');
                $url_context_image = Storage::disk('public2')->url($res);
            }

            

            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $opportunities_landing_ids,  $url_image, $url_context_image){
                

                $op = Opportunity::find($this->opportunity_id);
                
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->presentation_es;
                $op->presentation_en = $this->presentation_en;
                if($url_image != null){
                    $op->image = $url_image;
                }
                $op->save();


                $multi = OpportunityMultioffer::find($this->multioffer['id']);
                $multi->title_group_offer_es = $this->title_group_offer_es;
                $multi->title_group_offer_en = $this->title_group_offer_en;
                $multi->description_group_offer_es = $this->description_group_offer_es;
                $multi->description_group_offer_en = $this->description_group_offer_en;

                $multi->context = json_encode([
                    'option'=> $this->option_context,
                    'url' => ($this->option_context == 'video')?$this->url_video:$url_context_image
                ]);
                $multi->save();

                $multi->associated_opportunities()->sync($opportunities_landing_ids);
                
                $op->labels()->detach();

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                $op->area()->attach($areas_de_interes, ['type'=>'area']);
                

               
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => 'Oportunidad actualizada',
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);

            });

        /* }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error editando oportunidad multi oferta. ERROR: '.$e->getMessage());
        } */

    }

    public function render()
    {
        return view('livewire.admin.opportunities.multi-offer-edit');
    }
}
