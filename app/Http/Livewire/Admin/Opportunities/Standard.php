<?php

namespace App\Http\Livewire\Admin\Opportunities;

use Livewire\Component;
use App\Models\Opportunity;
use Livewire\WithPagination;
use App\Models\OpportunityLabel;
use Illuminate\Support\Facades\DB;
use App\Models\OpportunityStandard;

class Standard extends Component
{   

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $open = true;
    public $search = '';
    public $order = 'DESC';

    protected $listeners = [
        "deleteOpportunity" => 'deleteOpportunity',
    ];
    
    
    public function render()
    {   
        $open_status_id = ($this->open)?3:4;
        $op = Opportunity::with(['author'=>function($q){
            $q->select('id', 'name', 'last_name');
        }])
        ->where(function($query) {
            $query->where('title_es','like', '%'.$this->search.'%')
            ->orWhere('presentation_es','like', '%'.$this->search.'%');
        })
        ->where('open_status_id', $open_status_id)
        ->where('type_id', 1) //tipo standar
        ->select('id', 'title_es as title', 'presentation_es as presentation', 'status_id', 'open_status_id', 'user_id', 'created_at')
        ->orderBy('created_at',$this->order)
        ->paginate(12);
        
        
        return view('livewire.admin.opportunities.standard',[
            'opportunities'=>$op
        ]);
    }


    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingOrder()
    {
        $this->resetPage();
    }

    public function updatingOpen()
    {
        $this->resetPage();
    }

    public function deleteOpportunity($opp_id){
       
        //eliminar 
        DB::transaction(function () use($opp_id){
            $o = Opportunity::where('id', $opp_id)->delete();           
            OpportunityStandard::where('opportunity_id', $opp_id)->delete();
            OpportunityLabel::where('opportunity_id', $opp_id)->delete();
            

            $this->dispatchBrowserEvent('showToast', [
                'heading' => '',
                'text' => 'Oportunidad eliminada',
                'icon' => 'success',
                'position' => 'top-right',
            ]);
            $this->resetPage();
        });
        
    }
}
