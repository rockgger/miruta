<?php

namespace App\Http\Livewire\Admin\Opportunities;

use Carbon\Carbon;
use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Models\OpportunityHiring;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HiringCreate extends Component
{   
    use WithFileUploads;
    
    //public $lang = "es";
    public $image;
    public $start_date;
    public $end_date;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [], $type_contract_ids , $list_type_contract = [], $type_contract = [];
    public $url_forms, $title_es, $title_en, $object_contract_es, $object_contract_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id, $type_contract_tag_group_id;

    public $emails_notofy = [];

    public $salary;

    public $attachments = [], $attach;

    public function mount(){
       
        $this->tag_group = TagGroup::select('id', 'name_es as name')->orderBy('name')->get()->toArray();

        
    }
    
    public function render()
    {
        return view('livewire.admin.opportunities.hiring-create');
    }

    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }

    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }

    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedTypeContractTagGroupId($id){
        
        $lg = 'es';
        $this->type_contract = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->get()
        ->toArray();
    }

    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }

    public function updatedAttach(){
        /* $rules = [ 
            'max:1024'
        ];

        $messages = [ 
            'benefit_image.image'=>'Imagen no válida.',
            'benefit_image.max'=>'Máximo 1MB.',
        ]; */
        $this->resetErrorBag();
        if ($this->attach != null) {
            foreach($this->attach as $at){
                $r = Str::random(10);
                $info = pathinfo($at->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $at->storeAs('images', $name, 'public2');
                $att = Storage::disk('public2')->url($res);
                $this->attachments[] = [
                    'name'=>$at->getClientOriginalName(),
                    'url'=>$att
                ];
            }
        }
    }

    public function removeAttach($i){
        unset($this->attachments[$i]);
        $this->attachments = array_values($this->attachments);
    }

    public function store($public_status_id){
        
        $rules = [ 
            'title_es'=>'required',
            'start_date'=>'required|date',
            'end_date'=>'required|date|after_or_equal:start_date',
            'url_forms'=>'url',
        ];

        if ($this->image != null) {
            $rules = [ 
                'title_es'=>'required',
                'image'=>'image|max:1024',
                'start_date'=>'required|date',
                'end_date'=>'required|date|after_or_equal:start_date',
                'url_forms'=>'url',
            ];
        }

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'start_date.required'=>'Campo requerido.',
            'start_date.date'=>'Formato no válido.',
            'end_date.required'=>'Campo requerido.',
            'end_date.date'=>'Formato no válido.',
            'end_date.after_or_equal'=>'La fecha debe ser menor a la "fecha de inicio".',
            'url_forms.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);
        
        try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');

            
            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $url_image, $public_status_id){
                

                $op = new Opportunity();
                
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->object_contract_es;
                $op->presentation_en = $this->object_contract_en;
                if($url_image != null){
                    $op->image = $url_image;
                }
                $op->type_id = 3; //tipo estándar
                $op->status_id = $public_status_id;//publicado o borrador
                $op->open_status_id = 3; //oportunidad abierta
                $op->user_id = auth()->user()->id; //usuario admin autor
                $op->save();
                
                $hiring = new OpportunityHiring();
                $hiring->opportunity_id = $op->id;
                $hiring->start_date = new Carbon($this->start_date);
                $hiring->end_date = new Carbon($this->end_date);
                $hiring->url_forms = $this->url_forms;

                $hiring->type_contract = $this->type_contract_ids;
                $hiring->emails_notofy = json_encode($this->emails_notofy);
                $hiring->salary = $this->salary;
                $hiring->attachments = json_encode($this->attachments);
                $hiring->save();

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                $op->area()->attach($areas_de_interes, ['type'=>'area']);
                

                $msn = ($public_status_id == 13)?'Oportunidad guardada y publicada.':'Oportunidad guardada en borrador';
                $this->dispatchBrowserEvent('showSweetAlertRediret', [
                    'title' => 'Bien',
                    'text' => $msn,
                    'type' => 'success',
                    'btn' => 'ACEPTAR',
                    'url' => route('admin.opportunity.hiring')
                ]);

            });
 
        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando oportunidad contratación. ERROR: '.$e->getMessage());
        }

    }
}
