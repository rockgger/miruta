<?php

namespace App\Http\Livewire\Admin\Opportunities;

use Carbon\Carbon;
use App\Models\Label;
use App\Helpers\Helper;
use App\Models\Noticia;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use App\Models\OpportunityStandard;
use Illuminate\Support\Facades\Storage;

class StandardEdit extends Component
{   

    use WithFileUploads;
    public $opportunity_id, $open, $status, $standar;
    public $opportunity;
    public $image;
    public $start_date;
    public $end_date;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [], $aliados_ids = [], $list_aliados = [];
    public $url_forms, $title_es, $title_en, $presentation_es, $presentation_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id, $aliados_tag_group_id;

    //beneficios
    public $benefit_es = [], $benefit_en = [];
    //cronograma actividades
    public $activity_es = [], $activity_en = [];

    public $list_noticias = [], $noticias_ids = [], $total_noticias = 0; 

    public $limit = 8;
    public $offset = 0;

    public function mount(){
       
        $this->tag_group = TagGroup::with(['labels' => function($q){
            $q->select('labels.id', 'labels.name_es as name', 'labels.tag_group_id')
            ->orderBy('labels.name_es');
        }])
        ->select('id', 'name_es as name')
        ->orderBy('name')
        ->get()->toArray();

        $this->opportunity = Opportunity::with('standar')
        ->with(['labels'=>function($q){
            $q->select('labels.id', 'labels.name_es as name','labels.tag_group_id', 'opportunity_labels.type');
        }])
        ->with(['associated_noticias' => function($q){
            $q->select('noticias.id','noticias.title_es as title', 'noticias.status_id', 'noticias.summary_es as summary', 'noticias.created_at');
        }])
        ->find($this->opportunity_id)->toArray();
        $this->standar = $this->opportunity['standar'];
        
        $this->title_es = $this->opportunity['title_es'];
        $this->title_en = $this->opportunity['title_en'];
        $this->open = ($this->opportunity['open_status_id'] == 3)?true:false;
        $this->status = ($this->opportunity['status_id'] == 13)?true:false;
        $this->start_date = date('Y-m-d',strtotime($this->standar['start_date']));
        $this->end_date = date('Y-m-d',strtotime($this->standar['end_date']));
        $this->presentation_es = $this->opportunity['presentation_es'];
        $this->presentation_en = $this->opportunity['presentation_en'];
        

        $this->url_forms = $this->standar['url_forms'];
    
        
        $labels = $this->opportunity['labels'];

        
        //ENTIDADES
        $actors = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'actors';
        }));
        if(count($actors) > 0){
            $this->entities_ids = $actors;
            $this->tag_group_id = $actors[0]['tag_group_id'];
        }
           
        //CATEGORÍAS
        $categories = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'categories';
        }));
        $this->categories_ids = $categories;
        if(count($this->categories_ids) > 0){
            $ct_temp = Arr::first($this->tag_group, function ($g, $key) use($categories){
                return $g['id'] == $categories[0]['tag_group_id'];
            });
            $this->categories = $ct_temp['labels'];
            $this->categories_tag_group_id = $this->categories[0]['tag_group_id'];
            $this->categories_ids = Arr::pluck($categories, 'id');
        }
        

        //DIRIGIDO A
        $targets = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'targets';
        }));
        $this->dirigida_ids = $targets;
        if(count($this->dirigida_ids) > 0){
            $dt_temp = Arr::first($this->tag_group, function ($g, $key) use($targets){
                return $g['id'] == $targets[0]['tag_group_id'];
            });
            $this->dirigida = $dt_temp['labels'];
            $this->dirigida_tag_group_id = $this->dirigida[0]['tag_group_id'];
            $this->dirigida_ids = Arr::pluck($targets, 'id');
        }

        //ÁREA DE INTERES
        $area = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'area';
        }));
        if(count($area) > 0){
            $this->interes_ids = $area;
            $this->interes_tag_group_id = $area[0]['tag_group_id'];
        }

            
        //BENEFICIOS
        $benefit = json_decode($this->standar['benefit'], true);
        $this->benefit_es = $benefit['es'];
        $this->benefit_en = $benefit['en'];
            
        //CRONOGRAMA ACT
        $activities = json_decode($this->standar['schedule_activities'], true);
        $this->activity_es = $activities['es'];
        $this->activity_en = $activities['en'];

        //ALIADOS
        $partners = array_values(Arr::where($labels, function ($label, $key){
            return $label['type'] == 'partners';
        }));
        if(count($partners) > 0){
            $this->aliados_ids = $partners;
            $this->aliados_tag_group_id = $partners[0]['tag_group_id'];
        }


        //NOticias. ASOCIADAS
        $lsnoticias = $this->opportunity['associated_noticias'];
        if(count($lsnoticias) > 0){
            $this->noticias_ids = $lsnoticias;
        }
        

    }
    
    public function render()
    {
        return view('livewire.admin.opportunities.standard-edit');
    }


    //cambiar estdo de abierto y cerrado
    public function  updatingOpen($val){
        $id_change = ($val)?3:4;
        Opportunity::where('id',$this->opportunity_id)
        ->update([
            'open_status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function  updatingStatus($val){
        $id_change = ($val)?13:16;
        Opportunity::where('id',$this->opportunity_id)
        ->update([
            'status_id'=>$id_change
        ]);
        $this->dispatchBrowserEvent('showToast', [
            'heading' => '',
            'text' => 'Oportunidad actualizada',
            'icon' => 'success',
            'position' => 'top-right',
        ]);
    }

    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }

    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }

    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }

    public function loadAliados($aliados){

        $ids = Arr::pluck($aliados, 'id');

        $ls = Label::where('tag_group_id', $this->aliados_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_aliados = $ls->toArray();
        
    }

    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }




    public function update(){
        
        $rules = [ 
            'title_es'=>'required',
            'start_date'=>'required|date',
            'end_date'=>'required|date|after_or_equal:start_date',
            'url_forms'=>'url',
        ];

        if ($this->image != null) {
            $rules = [ 
                'title_es'=>'required',
                'image'=>'image|max:1024',
                'start_date'=>'required|date',
                'end_date'=>'required|date|after_or_equal:start_date',
                'url_forms'=>'url',
            ];
        }

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'start_date.required'=>'Campo requerido.',
            'start_date.date'=>'Formato no válido.',
            'end_date.required'=>'Campo requerido.',
            'end_date.date'=>'Formato no válido.',
            'end_date.after_or_equal'=>'La fecha debe ser menor a la "fecha de inicio".',
            'url_forms.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);
        
        try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');
            $aliados = Arr::pluck($this->aliados_ids, 'id');

            $noticias_ids = Arr::pluck($this->noticias_ids, 'id');

            $benefit = json_encode([
                'es'=>$this->benefit_es,
                'en'=>$this->benefit_en,
            ]);

            $activities = json_encode([
                'es'=>$this->activity_es,
                'en'=>$this->activity_en,
            ]);
            
            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }

            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $aliados, $benefit, $activities, $url_image, $noticias_ids){
                
                $op = Opportunity::find($this->opportunity_id);
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->presentation_es;
                $op->presentation_en = $this->presentation_en;
                if($url_image != null){
                    $op->image = $url_image;
                }
                $op->save();

                $standar = OpportunityStandard::where('id', $this->standar['id'])
                ->update([
                    'start_date'=> new Carbon($this->start_date),
                    'end_date'=> new Carbon($this->end_date),
                    'url_forms'=> $this->url_forms,
                    'benefit'=> $benefit,
                    'schedule_activities'=> $activities
                ]);

                
                $op->associated_noticias()->sync($noticias_ids);
                
                $op->labels()->detach();

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                $op->area()->attach($areas_de_interes, ['type'=>'area']);
                $op->partners()->attach($aliados, ['type'=>'partners']);

                

                $msn = 'Oportunidad actualizada';
                $this->dispatchBrowserEvent('showSweetAlert', [
                    'title' => 'Bien',
                    'text' => $msn,
                    'type' => 'success',
                    'btn' => 'ACEPTAR'
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error editando oportunidad estandar. ERROR: '.$e->getMessage());
        }

    }


    public function loadNoticias($noticias){


        $this->total_noticias = Noticia::where('status_id', 13)
        ->count();

        $ids = Arr::pluck($noticias, 'id');
        
        $ls = Noticia::whereNotIn('id', $ids)
        ->orderBy('created_at','desc')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'summary_es as summary', 'created_at')
        ->offset(0)
        ->limit($this->limit)
        ->get();

        $this->list_noticias = $ls->toArray();
    }


    public function loadModeNoticias($noticias){

        $ids = Arr::pluck($noticias, 'id');

        $this->offset = count($this->list_noticias);

        $ls = Noticia::whereNotIn('id', $ids)
        ->orderBy('created_at','desc')
        ->where('status_id', 13)
        ->select('id', 'title_es as title','status_id', 'summary_es as summary', 'created_at')
        ->offset($this->offset)
        ->limit($this->limit)
        ->get();

        $this->list_noticias = array_merge($this->list_noticias, $ls->toArray());

         
    }
}
