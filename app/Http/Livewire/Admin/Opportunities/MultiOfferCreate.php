<?php

namespace App\Http\Livewire\Admin\Opportunities;

use App\Models\Label;
use App\Helpers\Helper;
use Livewire\Component;
use App\Models\TagGroup;
use App\Models\Opportunity;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Models\OpportunityLanding;
use Illuminate\Support\Facades\DB;
use App\Models\OpportunityMultioffer;
use Illuminate\Support\Facades\Storage;

class MultiOfferCreate extends Component
{   

    use WithFileUploads;
     

    public $image;
    public $entities_ids = [], $list_entities = [], $categories = [], $categories_ids =[], $dirigida = [], $dirigida_ids = [], $interes_ids = [], $list_interes = [];

    public $url_video, $context_image, $option_context = 'video', $title_es, $title_en, $presentation_es, $presentation_en;
    public $tag_group = [], $tag_group_id, $categories_tag_group_id, $dirigida_tag_group_id, $interes_tag_group_id;

    //Grupos de ofertas
    public $title_group_offer_es, $title_group_offer_en, $description_group_offer_es, $description_group_offer_en;

    public $list_opportunities = [], $opportunities_ids = [], $total_opportunities = 0; 

    public $limit = 8;
    public $offset = 0;

    public function mount(){
       
        $this->tag_group = TagGroup::select('id', 'name_es as name')->orderBy('name')->get()->toArray();

        
    }
    
    
    public function loadEntities($entities){

        $ids = Arr::pluck($entities, 'id');

        $ls = Label::where('tag_group_id', $this->tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_entities = $ls->toArray();
    }


    public function loadInteres($interes){

        $ids = Arr::pluck($interes, 'id');

        $ls = Label::where('tag_group_id', $this->interes_tag_group_id)
        ->whereNotIn('id', $ids)
        ->orderBy('name_es')
        ->select('id', 'name_es as name')
        ->get();

        $this->list_interes = $ls->toArray();
    }

    public function updatedCategoriesTagGroupId($id){
        
        $lg = 'es';
        $this->categories = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }

    public function updatedDirigidaTagGroupId($id){
        
        $lg = 'es';
        $this->dirigida = Label::select('id', 'name_'.$lg.' as name')
        ->where('tag_group_id', $id)
        ->orderBy('name')
        ->get()
        ->toArray();
    }


    public function loadOpportunities($opportunities){


        $this->total_opportunities = Opportunity::where('status_id', 13)
        ->whereIn('type_id', [1,4])
        ->count();

        $ids = Arr::pluck($opportunities, 'id');
        
        $ls = Opportunity::whereNotIn('id', $ids)
        ->orderBy('title_es')
        ->where('status_id', 13)
        ->whereIn('type_id', [1,4])
        ->select('id', 'title_es as title','status_id', 'open_status_id', 'type_id', 'created_at')
        ->offset(0)
        ->limit($this->limit)
        ->get();

        $this->list_opportunities = $ls->toArray();
    }

    public function loadModeOpportunities($opportunities){

        $ids = Arr::pluck($opportunities, 'id');

        $this->offset = count($this->list_opportunities);

        $ls = Opportunity::whereNotIn('id', $ids)
        ->orderBy('title_es')
        ->where('status_id', 13)
        ->whereIn('type_id', [1,4])
        ->select('id', 'title_es as title','status_id', 'open_status_id', 'type_id', 'created_at')
        ->offset($this->offset)
        ->limit($this->limit)
        ->get();

        $this->list_opportunities = array_merge($this->list_opportunities, $ls->toArray());

         
    }

    public function resetGroup($taps, $ids){
        $this->reset([$taps, $ids]);
    }

    public function render()
    {
        return view('livewire.admin.opportunities.multi-offer-create');
    }

    

    public function store($public_status_id){
        
        $rules = [ 
            'title_es'=>'required',
        ];

        if ($this->image != null) {
            $rules['image'] = 'image|max:1024';
        }
        
        if ($this->context_image != null) {
            $rules['context_image'] = 'image|max:1024';
        }

        if ($this->url_video != null) {
            $rules['url_video'] = 'url';
        }
        
        

        $messages = [ 
            'title_es.required'=>'Campo requerido.',
            'image.image'=>'Imagen no válida.',
            'image.max'=>'Máximo 1MB.',
            'context_image.image'=>'Imagen no válida.',
            'context_image.max'=>'Máximo 1MB.',
            'url_video.url'=>'Formato de url no válido.',
        ];
        $this->resetErrorBag();
        $this->validate($rules, $messages);
        
        try{
            
            $entidades_o_actores = Arr::pluck($this->entities_ids, 'id');
            $categorias_oportunidad	 = $this->categories_ids;
            $oportunidad_dirigida_a = $this->dirigida_ids;
            $areas_de_interes = Arr::pluck($this->interes_ids, 'id');
            
            $opportunities_landing_ids = Arr::pluck($this->opportunities_ids, 'id');

                       
            
            $url_image = null;
            if ($this->image != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image->storeAs('images', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);
            }


            $url_context_image = null;
            if (($this->context_image != null)) {
                $r = Str::random(10);
                $info = pathinfo($this->context_image->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->context_image->storeAs('images', $name, 'public2');
                $url_context_image = Storage::disk('public2')->url($res);
            }

            

            DB::transaction(function () use($entidades_o_actores, $categorias_oportunidad, $oportunidad_dirigida_a, $areas_de_interes, $opportunities_landing_ids,  $url_image, $public_status_id, $url_context_image){
                

                $op = new Opportunity();
                
                $op->title_es = $this->title_es;
                $op->title_en = $this->title_en;
                $op->presentation_es = $this->presentation_es;
                $op->presentation_en = $this->presentation_en;
                if($url_image != null){
                    $op->image = $url_image;
                }
                $op->type_id = 2; //tipo multiofertas
                $op->status_id = $public_status_id;//publicado o borrador
                $op->open_status_id = 3; //oportunidad abierta
                $op->user_id = auth()->user()->id; //usuario admin autor
                $op->save();

                
                $multioffer = new OpportunityMultioffer();
                $multioffer->opportunity_id = $op->id;
                $multioffer->title_group_offer_es = $this->title_group_offer_es;
                $multioffer->title_group_offer_en = $this->title_group_offer_en;
                $multioffer->description_group_offer_es = $this->description_group_offer_es;
                $multioffer->description_group_offer_en = $this->description_group_offer_en;

                //['option'=>option_context, 'url'=>'url video o url img']
                $multioffer->context = json_encode([
                    'option'=> $this->option_context,
                    'url' => ($this->option_context == 'video')?$this->url_video:$url_context_image
                ]);

                $multioffer->save();

                
                $multioffer->associated_opportunities()->attach($opportunities_landing_ids);

                $op->actors()->attach($entidades_o_actores, ['type'=>'actors']);
                $op->categories()->attach($categorias_oportunidad, ['type'=>'categories']);
                $op->targets()->attach($oportunidad_dirigida_a, ['type'=>'targets']);
                $op->area()->attach($areas_de_interes, ['type'=>'area']);
                

                $msn = ($public_status_id == 13)?'Oportunidad guardada y publicada.':'Oportunidad guardada en borrador';
                $this->dispatchBrowserEvent('showSweetAlertRediret', [
                    'title' => 'Bien',
                    'text' => $msn,
                    'type' => 'success',
                    'btn' => 'ACEPTAR',
                    'url' => route('admin.opportunity.multioffer')
                ]);

            });

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error creando oportunidad multi oferta. ERROR: '.$e->getMessage());
        }

    }
}
