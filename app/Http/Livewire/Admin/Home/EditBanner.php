<?php

namespace App\Http\Livewire\Admin\Home;

use App\Helpers\Helper;
use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\HomeResource;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class EditBanner extends Component
{   

    use WithFileUploads;

    /* protected $listeners = [
        "deletedResourceBanner" => 'deletedResourceBanner',
    ]; */
    
    public $url_desktop, $url_mobile;
    public $desktop, $mobile;
    
    public function render()
    {   

        $resources = HomeResource::whereIn('position', ['desktop', 'mobile'])
        ->get();

        $this->url_desktop = $resources->first(function ($value, $key) {
            return $value->position == 'desktop';
        });
        $this->url_mobile = $resources->first(function ($value, $key) {
            return $value->position == 'mobile';
        });

        return view('livewire.admin.home.edit-banner');
    }


    public function updatedDesktop(){

        $rules = [ 
            'desktop'=>'image|max:1024',
        ];
        
        $messages = [ 
            'desktop.image'=>'Imagen no válida.',
            'desktop.max'=>'El archivo debe ser de máximo 1MB.',
        ];

        $this->resetErrorBag();
        $this->validate($rules, $messages);
        

    }

    public function updatedMobile(){

        $rules = [ 
            'mobile'=>'image|max:1024',
        ];
        
        $messages = [ 
            'mobile.image'=>'Imagen no válida.',
            'mobile.max'=>'El archivo debe ser de máximo 1MB.',
        ];

        $this->resetErrorBag();
        $this->validate($rules, $messages);
        

    }

    public function update(){

        if (($this->desktop == null) AND ($this->mobile == null)){
            return;
        }
        
        $rules = [];

        if ($this->desktop != null) {
            $rules['desktop'] = 'image|max:1024';
        }

        if ($this->mobile != null) {
            $rules['mobile'] = 'image|max:1024';
        }
        
        $messages = [ 
            'desktop.image'=>'Imagen no válida.',
            'desktop.required'=>'Ingresa una imagen para desktop.',
            'desktop.max'=>'El archivo debe ser de máximo 1MB.',
            'mobile.image'=>'Imagen no válida.',
            'mobile.required'=>'Ingresa una imagen para mobile.',
            'mobile.max'=>'El archivo debe ser de máximo 1MB.',
        ];

        $this->resetErrorBag();
        $this->validate($rules, $messages);

        try{
            if ($this->desktop != null) {
                $r = Str::random(10);
                $info = pathinfo($this->desktop->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->desktop->storeAs('images/home', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);

                if($this->url_desktop == null){
                    $this->url_desktop = new HomeResource();
                    $this->url_desktop->position = 'desktop';
                    $this->url_desktop->type = 'image';
                }
                $this->url_desktop->url = $url_image;
                $this->url_desktop->save();

                $this->dispatchBrowserEvent('showToast', [
                    'heading' => '',
                    'text' => 'Banner desktop actualizado.',
                    'icon' => 'success',
                    'position' => 'top-right',
                ]);
            }

            if ($this->mobile != null) {
                $r = Str::random(10);
                $info = pathinfo($this->mobile->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->mobile->storeAs('images/home', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);

                if($this->url_mobile == null){
                    $this->url_mobile = new HomeResource();
                    $this->url_mobile->position = 'mobile';
                    $this->url_mobile->type = 'image';
                }
                $this->url_mobile->url = $url_image;
                $this->url_mobile->save();

                $this->dispatchBrowserEvent('showToast', [
                    'heading' => '',
                    'text' => 'Banner mobile actualizado.',
                    'icon' => 'success',
                    'position' => 'top-right',
                ]);

            }
        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error actualizando Banner desktop en home hero. ERROR: '.$e->getMessage());
        }

    }
}
