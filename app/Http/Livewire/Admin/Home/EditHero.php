<?php

namespace App\Http\Livewire\Admin\Home;

use App\Helpers\Helper;
use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\HomeResource;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class EditHero extends Component
{   


    use WithFileUploads;

    protected $listeners = [
        "deletedResource" => 'deletedResource',
    ];
    
    public $resources = [];
    public $image_hero;

    public function render()
    {   
        $this->resources = HomeResource::where('position', 'herohome')
        ->latest()
        ->get()
        ->toArray();
        
        return view('livewire.admin.home.edit-hero');
    }

    public function updatedImageHero(){

        $rules = [ 
            'image_hero'=>'image|max:1024',
        ];
        
        $messages = [ 
            'image_hero.image'=>'Imagen no válida.',
            'image_hero.max'=>'El archivo debe ser de máximo 1MB.',
        ];

        $this->resetErrorBag();
        $this->validate($rules, $messages);
        try{
            if ($this->image_hero != null) {
                $r = Str::random(10);
                $info = pathinfo($this->image_hero->getClientOriginalName());
                $ext = $info['extension'];
                $name = Str::slug($info['filename'].'_'.$r, '_').'.'.$ext;
                $res = $this->image_hero->storeAs('images/home', $name, 'public2');
                $url_image = Storage::disk('public2')->url($res);

                $r = new HomeResource();
                $r->type = 'image';
                $r->position = 'herohome';
                $r->url = $url_image;
                $r->save();

                $this->dispatchBrowserEvent('showToast', [
                    'heading' => '',
                    'text' => 'Nuevo recurso agregado.',
                    'icon' => 'success',
                    'position' => 'top-right',
                ]);

            }
        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error agregando recurso a home hero. ERROR: '.$e->getMessage());
        }

    }


    public function deletedResource($id){
        try{

            HomeResource::where('id', $id)->delete();
            $this->dispatchBrowserEvent('showToast', [
                'heading' => '',
                'text' => 'Rrecurso hero home eliminado.',
                'icon' => 'success',
                'position' => 'top-right',
            ]);

        }catch (\Exception $e){
                
            //generamos cod de seguimiento
            $cod = "REG".date('ymdHms').Str::random(3);
            $cod = Str::upper($cod);  
            $this->dispatchBrowserEvent('showSweetAlert', [
                'title' => 'Error',
                'text' => 'Se presentó un error, intenta en otro momento. Cod: '.$cod,
                'type' => 'error',
                'btn' => 'ACEPTAR'
            ]);
            
            Helper::logError('COD: '.$cod.' | Error borrando recurso de home hero. ERROR: '.$e->getMessage());
        }
    }
}
