<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunityMultioffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_multioffers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('opportunity_id');  
            $table->foreign('opportunity_id')->references('id')->on('opportunities');

            $table->json('context')->nullable(); //arrays de ids de la tabla labels ['option'=>option_context, 'url'=>'url video o url img']

            $table->string('title_group_offer_es')->nullable();
            $table->string('title_group_offer_en')->nullable();

            $table->text('description_group_offer_es')->nullable();
            $table->text('description_group_offer_en')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_multioffers');
    }
}
