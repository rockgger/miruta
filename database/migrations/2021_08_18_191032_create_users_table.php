<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('photo')->default('/image/icons/profile.svg')->nullable();

            $table->string('token')->nullable();
            $table->string('ip')->nullable();
            $table->string('session')->nullable();
            $table->string('recover_password')->nullable();

            $table->unsignedBigInteger('status_id'); 
            $table->foreign('status_id')->references('id')->on('statuses');

            $table->unsignedBigInteger('role_id'); 
            $table->foreign('role_id')->references('id')->on('roles');

            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('treatment_data_at')->nullable();
            $table->timestamp('habeas_data_at')->nullable();
            
            
            $table->index(['name', 'last_name']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
