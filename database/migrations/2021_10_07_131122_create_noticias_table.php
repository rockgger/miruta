<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->string('title_es');
            $table->string('title_en')->nullable();
            $table->string('image')->default('/image/img.jpg')->nullable();

            $table->string('summary_es', 250)->nullable();
            $table->string('summary_en', 250)->nullable();

            $table->longText('body_es')->nullable();
            $table->longText('body_en')->nullable();

            $table->unsignedBigInteger('status_id'); //publicado o en borrador 
            $table->foreign('status_id')->references('id')->on('statuses');


            $table->unsignedBigInteger('user_id');   //autor
            $table->foreign('user_id')->references('id')->on('users');

            $table->index('title_es');
            $table->index('title_en');
            $table->index('summary_es');
            $table->index('summary_en');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
