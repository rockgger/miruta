<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociateMultiofferLandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associate_multioffer_landings', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('opportunity_multioffer_id');  
            $table->foreign('opportunity_multioffer_id')->references('id')->on('opportunity_multioffers');

            $table->unsignedBigInteger('opportunity_id');   
            $table->foreign('opportunity_id')->references('id')->on('opportunities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associate_multioffer_landings');
    }
}
