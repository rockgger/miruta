<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunityStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_standards', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('opportunity_id');  
            $table->foreign('opportunity_id')->references('id')->on('opportunities');

            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();

            $table->string('url_forms')->nullable();
            
            $table->json('benefit')->nullable(); //arrays con español e ingles
            $table->json('schedule_activities')->nullable(); //arrays con español e ingles

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_standards');
    }
}
