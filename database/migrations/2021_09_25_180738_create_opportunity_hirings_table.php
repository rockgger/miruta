<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunityHiringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_hirings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('opportunity_id');  
            $table->foreign('opportunity_id')->references('id')->on('opportunities');

            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();

            $table->string('url_forms')->nullable();
            
            $table->unsignedBigInteger('type_contract')->nullable();  //id de lista de etiquetas
            $table->foreign('type_contract')->references('id')->on('labels');

            $table->string('salary')->nullable();

            $table->json('emails_notofy')->nullable(); //arrays de email a los cuales le llega la HV

            $table->json('attachments')->nullable(); //arrays de url de documentos adjuntos
            


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_hirings');
    }
}
