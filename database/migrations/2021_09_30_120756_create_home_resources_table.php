<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_resources', function (Blueprint $table) {
            $table->id();
            
            $table->string('url');
            $table->string('type')->default('image'); //tipo de recurso (vido, image)
            $table->string('position')->nullable(); //tipo (desktop, mobile, herohome)

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_resources');
    }
}
