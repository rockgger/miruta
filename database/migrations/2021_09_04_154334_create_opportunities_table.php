<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunities', function (Blueprint $table) {
            $table->id();

            $table->string('title_es');
            $table->string('title_en')->nullable();
            $table->string('image')->default('/image/img.jpg')->nullable();

            $table->text('presentation_es')->nullable();
            $table->text('presentation_en')->nullable();

            $table->unsignedBigInteger('type_id');   //tipo de oportunidad
            $table->foreign('type_id')->references('id')->on('type_opportunities');

            $table->unsignedBigInteger('status_id'); //publicado o en borrador 
            $table->foreign('status_id')->references('id')->on('statuses');

            $table->unsignedBigInteger('open_status_id'); //oportunidad abierta o cerrada
            $table->foreign('open_status_id')->references('id')->on('statuses');

            $table->unsignedBigInteger('user_id');   //autor
            $table->foreign('user_id')->references('id')->on('users');

            $table->index('title_es');
            $table->index('title_en');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunities');
    }
}
