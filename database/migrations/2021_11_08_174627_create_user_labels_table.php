<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_labels', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');  
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('label_id');   
            $table->foreign('label_id')->references('id')->on('labels');

            $table->string('type'); //category, target, areas

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_labels');
    }
}
