<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunityLandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_landings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('opportunity_id');  
            $table->foreign('opportunity_id')->references('id')->on('opportunities');

            $table->string('url_web')->nullable();

            $table->string('summary_es', 255)->nullable();
            $table->string('summary_en', 255)->nullable();
            
            $table->json('benefit')->nullable(); //arrays con español e ingles
            $table->json('how_functions')->nullable(); //arrays con español e ingles

            $table->json('programs_services')->nullable(); //arrays con español e ingles

            $table->json('context')->nullable(); //arrays de ids de la tabla labels ['option'=>option_context, 'url'=>'url video o url img']

            $table->string('title_accordion_es')->nullable();
            $table->string('title_accordion_en')->nullable();

            $table->json('accordion')->nullable(); //arrays con español e ingles

            $table->string('url_funtion_image')->nullable();
            $table->string('url_benefit_image')->nullable();            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_landings');
    }
}
