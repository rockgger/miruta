<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); 
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('profile_id')->nullable(); 
            $table->foreign('profile_id')->references('id')->on('profiles');

            $table->unsignedBigInteger('document_type_id')->nullable(); 
            $table->foreign('document_type_id')->references('id')->on('document_types');

            $table->string('document_number')->nullable();
            $table->text('biography')->nullable();
            $table->string('cell_phone')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();

            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
