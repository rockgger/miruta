<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiaLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticia_labels', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('new_id');  
            $table->foreign('new_id')->references('id')->on('noticias');

            $table->unsignedBigInteger('label_id');   
            $table->foreign('label_id')->references('id')->on('labels');

            $table->string('type'); //category, target, areas

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticia_labels');
    }
}
