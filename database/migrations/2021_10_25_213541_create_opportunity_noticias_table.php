<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunityNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_noticias', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('noticia_id');  
            $table->foreign('noticia_id')->references('id')->on('noticias');

            $table->unsignedBigInteger('opportunity_id');   
            $table->foreign('opportunity_id')->references('id')->on('opportunities');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_noticias');
    }
}
