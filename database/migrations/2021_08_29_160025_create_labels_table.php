<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels', function (Blueprint $table) {
            $table->id();
            $table->string('name_es');
            $table->string('name_en')->nullable();
            $table->string('icon')->nullable();
            $table->string('image')->default('/image/img.jpg')->nullable();
            $table->string('description_es')->nullable(); 
            $table->string('description_en')->nullable();

            $table->unsignedBigInteger('tag_group_id'); 
            $table->foreign('tag_group_id')->references('id')->on('tag_groups');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels');
    }
}
