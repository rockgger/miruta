<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunityLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_labels', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('opportunity_id');  
            $table->foreign('opportunity_id')->references('id')->on('opportunities');

            $table->unsignedBigInteger('label_id');   
            $table->foreign('label_id')->references('id')->on('labels');

            $table->string('type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_labels');
    }
}
