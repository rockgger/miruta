<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOpportunities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_opportunities')->insert([
            [                
                'name_en' => "Standard opportunity",
                'name_es' => "Oportunidad estándar",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "Multi-offer opportunity",
                'name_es' => "Oportunidad multioferta",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "Hiring opportunity",
                'name_es' => "Oportunidad contratación",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "Landing opportunity",
                'name_es' => "Oportunidad landing",
                'created_at' => Carbon::now()
            ]
            
        ]);
    }
}
