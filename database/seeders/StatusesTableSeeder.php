<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [                
                'name_es' => "Activo",
                'name_en' => "Active",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Inactivo",
                'name_en' => "Inactive",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Abierta",
                'name_en' => "Open",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Cerrada",
                'name_en' => "Closed",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Autorizado",
                'name_en' => "Authorized",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "No autorizado",
                'name_en' => "Not authorized",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Aprobado",
                'name_en' => "Approved",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "No aprobado",
                'name_en' => "Not approved",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Pendiente",
                'name_en' => "Pending",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "En proceso",
                'name_en' => "In process",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Rechazado",
                'name_en' => "Rejected",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Finalizado",
                'name_en' => "Finalized",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Publicado",
                'name_en' => "Published",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "No publicado",
                'name_en' => "Not published",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Cancelado",
                'name_en' => "Cancelled",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Borrador",
                'name_en' => "Draft",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Error",
                'name_en' => "Error",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "No validado",
                'name_en' => "Not validated",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Pagado",
                'name_en' => "Paid",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "No pagado",
                'name_en' => "Unpaid",
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
