<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LabelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tag_groups')->insert([
            [                
                'uid' => Str::slug('Áreas de equipo de trabajo','_'),
                'name_es'=>'Áreas de equipo de trabajo',
                'created_at' => Carbon::now()
            ],
            [                
                'uid' => 'entidades_o_actores',
                'name_es'=>'Entidades o actores',
                'created_at' => Carbon::now()
            ],
            [                
                'uid' => 'aliados',
                'name_es'=>'Aliados',
                'created_at' => Carbon::now()
            ],
            [                
                'uid' => Str::slug('Categorías' ,'_'),
                'name_es'=>'Categorías',
                'created_at' => Carbon::now()
            ],
            [                
                'uid' => Str::slug('dirigida a','_'),
                'name_es'=>'dirigida a',
                'created_at' => Carbon::now()
            ],
            [                
                'uid' => Str::slug('Áreas de interés','_'),
                'name_es'=>'Áreas de interés',
                'created_at' => Carbon::now()
            ],
            [                
                'uid' => Str::slug('Tipo de contrato','_'),
                'name_es'=>'Tipo de contrato',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
