<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DemoLabelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('labels')->insert([

            [                
                'tag_group_id' => 1,
                'name_es'=>'Administrativo',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 1,
                'name_es'=>'Mercadeo',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 1,
                'name_es'=>'Transformación digital',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 1,
                'name_es'=>'Laboratorio de Innovación',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 1,
                'name_es'=>'Economías creativas',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 2,
                'name_es'=>'Entidades o actores 1',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 2,
                'name_es'=>'Entidades o actores 2',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 2,
                'name_es'=>'Entidades o actores 3',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 2,
                'name_es'=>'Entidades o actores 4',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 2,
                'name_es'=>'Entidades o actores 5',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 1',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 2',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 3',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 4',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 5',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 6',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 3,
                'name_es'=>'Aliados 7',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 4,
                'name_es'=>'Categorías 1',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 4,
                'name_es'=>'Categorías 2',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 4,
                'name_es'=>'Categorías 3',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 4,
                'name_es'=>'Categorías 4',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 4,
                'name_es'=>'Categorías 5',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 4,
                'name_es'=>'Categorías 6',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 5,
                'name_es'=>'Dirigida a 1',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 5,
                'name_es'=>'Dirigida a 2',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 5,
                'name_es'=>'Dirigida a 3',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 5,
                'name_es'=>'Dirigida a 4',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 5,
                'name_es'=>'Dirigida a 5',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 5,
                'name_es'=>'Dirigida a 6',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 6,
                'name_es'=>'Áreas 1',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 6,
                'name_es'=>'Áreas 2',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 6,
                'name_es'=>'Áreas 3',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 6,
                'name_es'=>'Áreas 4',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 6,
                'name_es'=>'Áreas 5',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 6,
                'name_es'=>'Áreas 6',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 7,
                'name_es'=>'Tipo de contrato 1',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 7,
                'name_es'=>'Tipo de contrato 2',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 7,
                'name_es'=>'Tipo de contrato 3',
                'created_at' => Carbon::now()
            ],
            [                
                'tag_group_id' => 7,
                'name_es'=>'Tipo de contrato 4',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
