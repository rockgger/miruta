<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [                
                'name' => 'Admin',
                'last_name' =>'Rutan',
                'email' =>'admin@miruta.local',
                'password' =>bcrypt('123456789'),
                'status_id' =>1,
                'role_id' =>2,
                'email_verified_at' => Carbon::now(),
                'treatment_data_at' => Carbon::now(),
                'habeas_data_at' => Carbon::now(),
                'created_at' => Carbon::now()
            ],
            [                
                'name' => 'Rogger',
                'last_name' =>'Jiménez',
                'email' =>'roggerjimenez@gmail.com',
                'password' =>bcrypt('123456789'),
                'status_id' =>1,
                'role_id' =>1,
                'email_verified_at' => Carbon::now(),
                'treatment_data_at' => Carbon::now(),
                'habeas_data_at' => Carbon::now(),
                'created_at' => Carbon::now()
            ],
            [                
                'name' => 'Demo',
                'last_name' =>'',
                'email' =>'demo@miruta.local',
                'password' =>bcrypt('123456789'),
                'status_id' =>1,
                'role_id' =>1,
                'email_verified_at' => Carbon::now(),
                'treatment_data_at' => Carbon::now(),
                'habeas_data_at' => Carbon::now(),
                'created_at' => Carbon::now()
            ]
        ]);

        DB::table('user_details')->insert([
            [
                'user_id' => 2,
                'profile_id'=>1,
            ],
            [
                'user_id' => 3,
                'profile_id'=>1,
            ],
        ]);

        
    }
}
