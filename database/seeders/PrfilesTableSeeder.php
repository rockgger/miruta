<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            [                
                'name_es' => "Emprendedor",
                'name_en' => "Entrepreneur",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Empresario",
                'name_en' => "Businessman",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Empleado",
                'name_en' => "Employee",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Estudiante",
                'name_en' => "Student",
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Inversionista",
                'name_en' => "Investor",
                'created_at' => Carbon::now()
            ],
        ]);
    }
}
