<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AvatarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('avatars')->insert([
            [                
                'name' => "Avatar",
                'url'=>'/image/icons/profile.svg',
                'created_at' => Carbon::now()
            ],
            [                
                'name' => "Avatar",
                'url'=>'/image/icons/profile2.svg',
                'created_at' => Carbon::now()
            ],
            [                
                'name' => "Avatar",
                'url'=>'/image/icons/profile3.svg',
                'created_at' => Carbon::now()
            ],
            [                
                'name' => "Avatar",
                'url'=>'/image/icons/profile4.svg',
                'created_at' => Carbon::now()
            ],
            [                
                'name' => "Avatar",
                'url'=>'/image/icons/profile5.svg',
                'created_at' => Carbon::now()
            ],
            [                
                'name' => "Avatar",
                'url'=>'/image/icons/profile6.svg',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
