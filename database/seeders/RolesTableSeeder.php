<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [                
                'name_en' => "User",
                'name_es' => "Usuario",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "SuperAdmin",
                'name_es' => "Super administrado",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "Admin",
                'name_es' => "Administrador",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "Support",
                'name_es' => "Soporte",
                'created_at' => Carbon::now()
            ],
            [                
                'name_en' => "Content",
                'name_es' => "Contenido",
                'created_at' => Carbon::now()
            ],
            
        ]);
    }
}
