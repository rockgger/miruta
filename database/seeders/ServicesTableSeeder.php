<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [                
                'uid' => "landingrutan",
                'key'=> "hZEZFOUdEBikSUx8LfTgo3C8RXxx13nAAYIRrBBcnts5K5Rgishgg",
                'name'=> "Landing Rutan",
                'description_es'=> "Servicio de landing Rutan digital",
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
