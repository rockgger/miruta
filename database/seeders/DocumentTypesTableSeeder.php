<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_types')->insert([
            [                
                'name_es' => "Cédula de ciudadanía",
                'name_en' => "Citizenship card",
                'code'=>'cc',
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Cédula de extranjería",
                'name_en' => "Foreigner ID",
                'code'=>'ce',
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Pasaporte",
                'name_en' => "Passport",
                'code'=>'ppn',
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Número de seguridad social",
                'name_en' => "Social Security number",
                'code'=>'ssn',
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Documento nacional de identificación",
                'name_en' => "National identification document",
                'code'=>'dni',
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Tarjeta de identidad",
                'name_en' => "Identity card",
                'code'=>'ti',
                'created_at' => Carbon::now()
            ],
            [                
                'name_es' => "Número de indentificación tributaria",
                'name_en' => "Tax identification number",
                'code'=>'nit',
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
