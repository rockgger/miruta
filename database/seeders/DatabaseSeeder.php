<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        
        $this->call(RolesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(AvatarsTableSeeder::class);
        $this->call(DocumentTypesTableSeeder::class);
        $this->call(PrfilesTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(LabelsTableSeeder::class);
        $this->call(TypeOpportunities::class);
        $this->call(DemoLabelsSeeder::class);
              
    }
}
