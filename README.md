# Mi RUTA

## _Módulo mi ruta_
funcionalidad para los usuarios de rutan

## _Módulo Admin_
Funcionalidad para usuarios administradores de rutan

## _Módulo API_
Expone un webservice para ser consumido por landing u otros.

## _Tech_
-   PHP
-   Laravel 8 - Framework PHP
-   Bootstrap - UI web apps
-   jQuery
-   Blade - HTML Markdown
-   AlpineJS - JavaScript framework
-   laravel-livewire

## _Instalación_
- Se requiere PHP 7.4 +
- composer
- Crear una BD
- Parametrizar archivo .env
```sh
cp .evn.example .env
composer install
php artisan migrate
php artisan db:seed
php artisan serve
```

## _Contacto_
-   r.jimenez@rutanmedelling.org