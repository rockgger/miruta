window.addEventListener("showToast", (event) => {
    $.toast({
        heading: event.detail.heading,
        text: event.detail.text,
        icon: event.detail.icon,
        showHideTransition: "slide",
        position: event.detail.position,
        hideAfter: 6000,
        loader: false,
    });
});

window.addEventListener("closeModal", (event) => {
    $("#" + event.detail.id).modal("hide");
});

window.addEventListener("openModal", (event) => {
    $("#" + event.detail.id).modal("show");
});

//notificación final
window.addEventListener("showSweetAlert", (event) => {
    Swal.fire({
        icon: `${event.detail.type}`,
        title: `${event.detail.title}`,
        html: `${event.detail.text}`,
        showClass: {
            popup: "animate__animated animate__fadeInDown",
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        confirmButtonText: `${event.detail.btn}`,
        confirmButtonColor: "#d5f1ed",
        confirmButtonClass: "btn btn-custom mt-2",
    });
});


//notifiaciones con pregunta y redirecciona a una url
window.addEventListener('showSweetAlertRediret', event => {
    Swal.fire({
        icon: `${event.detail.type}`,
        title: `${event.detail.title}`,
        html: `${event.detail.text}`,
        showClass: {
            popup: "animate__animated animate__fadeInDown",
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        confirmButtonText: `${event.detail.btn}`,
        confirmButtonColor: "#d5f1ed",
        confirmButtonClass: "btn btn-custom mt-2",
    }).then(function(r) {
        if (r.value) {
            window.location.href = event.detail.url;
        }
    })
});

//notifiaciones con pregunta
window.addEventListener('showSweetAlertQuestion', event => {
    Swal.fire({
        icon: `${event.detail.type}`,
        title: `${event.detail.title}`,
        html: `${event.detail.text}`,
        showClass: {
            popup: "animate__animated animate__fadeInDown",
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn-text mt-2"
    }).then(function(r) {
        if (r.value) {
            Livewire.emit(`${event.detail.actionEmit}`, event.detail.val, event.detail.val2)
        }
    })
});