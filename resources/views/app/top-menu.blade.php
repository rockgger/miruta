<div class="navbar-rutan">
    <div class="full-logo">
        <img src="{{asset('image/full-logo.svg')}}" alt="">
    </div>

    <div class="menu-options">
        <ul class="list-unstyled topnav-menu float-right mb-0">
            <li class="dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle  waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="mdi mdi-bell noti-icon active"></i>
                    <!-- <span class="badge badge-primary rounded-circle noti-icon-badge">3</span> -->
                </a>
                <div class="dropdown-menu dropdown-menu-end dropdown-lg">

                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="m-0 text-uppercase">
                            <span class="">
                                <a href="javascript:void(0);" class="text-dark">
                                    <small>Notificaciones</small>
                                </a>
                            </span>
                        </h5>
                    </div>

                    <!-- All-->
                    <a href="javascript:void(0);" class="d-block text-center notify-item notify-all">
                        Ver todos
                    </a>

                </div>
            </li>

            <li class="dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{Auth()->user()->photo}}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ms-1">
                    {{Auth()->user()->fullName()}} <i class="ti-angle-down i-rn ms-1"></i> 
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-end profile-dropdown list-options-user-rn">

                    <!-- item-->
                    <a href="{{ route('app.myprofile') }}" class="dropdown-item notify-item">
                        <span>Mi perfil</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <!-- item-->
                    <a href="{{route('auth.logout')}}" class="dropdown-item notify-item">
                        <span>Salir</span>
                    </a>

                </div>
            </li>
        </ul>

    </div>
</div>

