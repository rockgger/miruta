<!-- Left Sidebar -->
<div class="right-bar" style="display:none;" x-data="">
    <div class="rightbar-title">
        <!-- LOGO -->
        <div class="" style="display: inline-block;">
            <a href="https://www.rutanmedellin.org/" class="">                
                <span class="">
                    <img src="{{asset('image/full-logo.svg')}}" alt="" height="36">
                </span>
            </a>
        </div>

        <a href="javascript:void(0);" class="show_menu_mobile float-right" x-on:click="toggleMenuMobile();">
            <i class="ti-close"></i>
        </a>
        
    </div>

    <div class="slimscroll-menu">
       
        

        <div class="left-menu-account ms-3 pe-3">
            <div class="name-user">
                <span>hola</span>
                <h1 class="mt-0"><b>{{Auth()->user()->name}}</b></h1>
            </div>

            <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="{{route('app.home')}}" id="index">
                        <i class="rn-home"></i>
                        <span>inicio</span>
                    </a>     
                </div>   
            </div>

            <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="{{ route('app.myrutan') }}" id="myrutan">
                        <i class="rn-miruta"></i>
                        <span>Mi ruta</span>
                    </a> 
                </div>       
            </div>

            <!-- <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="#">
                        <i class="rn-info"></i>
                        <span>En curso</span>
                    </a>        
                </div>
            </div> -->

            <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="#" id="myevents">
                        <i class="rn-calendar"></i>
                        <span>Mis eventos</span>
                    </a>        
                </div>
            </div>

            <!-- <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="#">
                        <i class="rn-service"></i>
                        <span>Servicios Ruta N</span>
                    </a>        
                </div>
            </div> -->

            <!-- <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="#">
                        <i class="rn-portfolio"></i>
                        <span>Mis empresas</span>
                    </a>        
                </div>
            </div> -->

            <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="#" id="li-library">
                        <i class="rn-cloud"></i>
                        <span>Biblioteca</span>
                    </a>        
                </div>
            </div>

            <div class="list-menu-account ">
                <div class="l2-mn">
                    <a href="#" id="li-inbox">
                        <i class="rn-inbox"></i>
                        <span>Inbox</span>
                    </a>        
                </div>
            </div>

            <hr style="border-color: #979797;">

            <!-- <div class="list-menu-account ">
                <div class="">
                    <a href="javascript:void(0);" class="">                       
                        <span>Configuración</span>
                    </a>        
                </div>
            </div> -->
            <div class="list-menu-account ">
                <div class="">
                    <a href="{{ route('app.myprofile') }}" class="">                       
                        <span>MI PERFIL</span>
                    </a>        
                </div>
            </div>
            <div class="list-menu-account ">
                <div class="">
                    <a href="{{route('auth.logout')}}" class="" style="color: #fdc300;">                       
                        <span>Salir</span>
                    </a>        
                </div>
            </div>

        </div>
                
                  
    </div> <!-- end slimscroll-menu-->
    
</div>
<!-- /left-bar -->

