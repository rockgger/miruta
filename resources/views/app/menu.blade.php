<div class="left-menu-account">
    <div class="name-user d-mobile-none">
        <span>hola</span>
        <h1 class="mt-0"><b>{{Auth()->user()->name}}</b></h1>
    </div>

    <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="{{route('app.home')}}" id="index">
                <i class="rn-home"></i>
                <span>inicio</span>
            </a>     
        </div>   
    </div>

    <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="{{ route('app.myrutan') }}" id="myrutan">
                <i class="rn-miruta"></i>
                <span>Mi ruta</span>
            </a> 
        </div>       
    </div>

    <!-- <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="#">
                <i class="rn-info"></i>
                <span>En curso</span>
            </a>        
        </div>
    </div> -->

    <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="#" id="myevents">
                <i class="rn-calendar"></i>
                <span>Mis eventos</span>
            </a>        
        </div>
    </div>

    <!-- <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="#">
                <i class="rn-service"></i>
                <span>Servicios Ruta N</span>
            </a>        
        </div>
    </div> -->

    <!-- <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="#">
                <i class="rn-portfolio"></i>
                <span>Mis empresas</span>
            </a>        
        </div>
    </div> -->

    <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="#" id="li-library">
                <i class="rn-cloud"></i>
                <span>Biblioteca</span>
            </a>        
        </div>
    </div>

    <div class="list-menu-account d-mobile-none">
        <div class="l2-mn">
            <a href="#" id="li-inbox">
                <i class="rn-inbox"></i>
                <span>Inbox</span>
            </a>        
        </div>
    </div>

</div>