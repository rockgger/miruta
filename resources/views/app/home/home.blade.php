@extends('app.index')

@section("styles")
<link href="{{asset('libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/switchery-custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('libs/owl/owl.carousel.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('libs/owl/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/owl-custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid animated fadeIn ">      
                     

    <div class="row">
        <div class="col-md-12">           
            
            <div class="card-account" x-data="{show:'oportunity'}">
                <h3 class="text-uppercase mb-4">Hoy tenemos para ti:</h3>
                <div class="row items-menu-center" >
                    <div class="col-4 text-center  ">
                        <a href="javascript:void(0);" class="text-uppercase title-items " :class="{'active':show==='oportunity'}" x-on:click="show='oportunity'">
                            <span class="">OPORTUNIDADES</span>
                        <img src="{{asset('image/oportunidades.png')}}" alt="" class="rounded-circle animated bounceIn">
                        </a>
                    </div>

                    <div class="col-4 text-center ">
                        <a href="javascript:void(0);" class="text-uppercase title-items" :class="{'active':show==='events'}" x-on:click="show='events'">
                            <span class="">Eventos</span>
                        <img src="{{asset('image/eventos.png')}}" alt="" class="rounded-circle animated bounceIn">
                        </a>
                    </div>

                    <div class="col-4 text-center ">
                        <a href="javascript:void(0);" class="text-uppercase title-items"  :class="{'active':show==='news'}" x-on:click="show='news'">
                            <span class="">Noticias</span>
                        <img src="{{asset('image/noticias.png')}}" alt="" class="rounded-circle animated bounceIn">
                        </a>
                    </div>
                </div>
                <div class="row" x-show="show === 'oportunity' " x-transition >
                    <livewire:aplication.home.oportunity>
                </div>

                <div class="row" x-show="show === 'events' " x-transition>
                    <livewire:aplication.home.events>
                </div>

                <div class="row" x-show="show === 'news' " x-transition>
                    <livewire:aplication.home.news>
                </div>
                
                
            </div> 
            
        </div> <!-- end col-->
    </div>                       
    
</div>
@endsection

@section('scripts')
<script src="{{asset('libs/switchery/switchery.min.js')}}"></script>
<script src="{{asset('libs/owl/owl.carousel.min.js')}}"></script>
<script>
$(document).ready(function(){

    $(".list-menu-account #index").addClass("active");
    $(".navbar-rutan-mobile .notification-list .id-index").addClass("active");
    
    $('[data-plugin="switchery"]').each(function(e,n){
        new Switchery($(this)[0],  {
            color: '#fbf1ca', 
            jackColor: '#f1cd3d',
            size: 'small',
            secondaryColor: '#ebebeb',
            jackSecondaryColor: '#b3b3b3'
        })
    });

    //Livewire.emit('loadOpportunities');

}); 



</script>
@endsection