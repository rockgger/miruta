@extends('app.index')

@section("styles")
<link href="{{asset('libs/owl/owl.carousel.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('libs/owl/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('css/owl-custom.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid animated fadeIn">      
                     

    <div class="row">
        <div class="col-md-12">           
            
            <div class="card-account my-rutan">
                <div class="row">
                    <div class="col-md-6">  
                        <img  src="{{$user->photo}}" class="img-fluid rounded-circle img-my-ruta"  alt="">
                        <h3 class="text-uppercase mb-0 mt-0">{{$user->fullName()}}</h3>
                        <p class="profile-rn text-capitalize">{{$profile->name}}</p>
                       <!--  <p class="score-rn">Score: 560</p> -->
                    </div>

                    <div class="col-md-6 ">  
                        <livewire:aplication.profile.graph :user="$user">
                        
                    </div>

                    <div class="col-md-12 mt-3 mb-4">
                        <h3 class="text-uppercase mb-2 text-mobile-center">Tu resumen</h3>
                        <div class="owl-carousel owl-theme">
                            
                            <div class="item-summary">
                                <div class="card-sumary">
                                    <h4 class="item-number">4 </h4>
                                    <h3 class="text-uppercase m-0">Ofertas</h3>
                                    <p class="m-0">Aplicadas</p>

                                    <div class="Line-2 mt-3 mb-3"></div>
                                    <a href="#" class="btn btn-custom w-100  text-uppercase pe-4 ps-4">Ir</a>
                                    <!-- <p class=" ms-2"><i class="me-1 text-dirty mdi mdi-brightness-1"></i>1 en curso</p>
                                    <p class="text-very-light ms-2"><i class="me-1  mdi mdi-brightness-1"></i>3 cerradas</p> -->
                                </div>
                            </div>

                            <div class="item-summary">
                                <div class="card-sumary">
                                    <h4 class="item-number">2 </h4>
                                    <h3 class="text-uppercase m-0">Eventos</h3>
                                    <p class="m-0">Registrados</p>

                                    <div class="Line-2 mt-3 mb-3"></div>
                                    <a href="#" class="btn btn-custom w-100  text-uppercase pe-4 ps-4">Ir</a>
                                    <!-- <p class=" ms-2"><i class="me-1 text-dirty mdi mdi-brightness-1"></i>1  abierto</p>
                                    <p class="text-very-light ms-2"><i class="me-1  mdi mdi-brightness-1"></i>1  asistido</p> -->
                                </div>
                            </div>

                            <div class="item-summary">
                                <div class="card-sumary">
                                    <h4 class="item-number">10 </h4>
                                    <h3 class="text-uppercase m-0">Oportunidades</h3>
                                    <p class="m-0">Aplicadas</p>

                                    <div class="Line-2 mt-3 mb-3"></div>
                                    <a href="#" class="btn btn-custom w-100  text-uppercase pe-4 ps-4">Ir</a>
                                    <!-- <p class=" ms-2"><i class="me-1 text-dirty mdi mdi-brightness-1"></i>1  en curso</p>
                                    <p class="text-very-light ms-2"><i class="me-1  mdi mdi-brightness-1"></i>9  cerradas</p> -->
                                </div>
                            </div>
                           
                        </div>
                    </div>

                    <div class="col-md-12 mt-4 mb-4">
                        <h3 class="text-uppercase mb-2 text-mobile-center">Mis servicios</h3>
                            <div class="row">                            
                                
                                <div class="col-md-4 item-summary " >
                                    <div class="card-sumary p-0">     
                                        <div class="img-service" style="background-image: url('/image/img.jpg')"> 
                                        </div>                                   
                                        <h3 class="text-uppercase mt-2 mb-2 ms-2 me-0 hel">Observatorio </h3>  
                                        <a href="#" class="btn btn-icon-round waves-effect waves-light float-right"><i class="fe-arrow-right"></i></a>                                      
                                    </div>
                                </div>

                                <div class="col-md-4 item-summary " >
                                    <div class="card-sumary p-0">     
                                        <div class="img-service" style="background-image: url('/image/img.jpg')"> 
                                        </div>                                   
                                        <h3 class="text-uppercase mt-2 mb-2 ms-2 me-0 hel">Sunn </h3>  
                                        <a href="#" class="btn btn-icon-round waves-effect waves-light float-right"><i class="fe-arrow-right"></i></a>                                      
                                    </div>
                                </div>

                                <div class="col-md-4 item-summary " >
                                    <div class="card-sumary p-0">     
                                        <div class="img-service" style="background-image: url('/image/img.jpg')"> 
                                        </div>                                   
                                        <h3 class="text-uppercase mt-2 mb-2 ms-2 me-0 hel">Economías creativas y culturales </h3>  
                                        <a href="#" class="btn btn-icon-round waves-effect waves-light float-right"><i class="fe-arrow-right"></i></a>                                      
                                    </div>
                                </div>

                                <div class="col-md-4 item-summary " >
                                    <div class="card-sumary p-0">     
                                        <div class="img-service" style="background-image: url('/image/img.jpg')"> 
                                        </div>                                   
                                        <h3 class="text-uppercase mt-2 mb-2 ms-2 me-0 hel">Distrito de innovación</h3>  
                                        <a href="#" class="btn btn-icon-round waves-effect waves-light float-right"><i class="fe-arrow-right"></i></a>                                      
                                    </div>
                                </div>

                                <div class="col-md-4 item-summary " >
                                    <div class="card-sumary p-0">     
                                        <div class="img-service" style="background-image: url('/image/img.jpg')"> 
                                        </div>                                   
                                        <h3 class="text-uppercase mt-2 mb-2 ms-2 me-0 hel">Gran pacto por la innovación </h3>  
                                        <a href="#" class="btn btn-icon-round waves-effect waves-light float-right"><i class="fe-arrow-right"></i></a>                                      
                                    </div>
                                </div>

                                
                                
                            </div>
                        
                    </div>

                    <!-- <div class="col-md-12 mt-4 mb-5">                        
                        <div class="row">
                            <div class="col-md-6 text-center mb-2" >
                                <h3 class="text-uppercase mb-2">Empresas registradas</h3>
                                <h1 class="text-marigold">2</h1>
                            </div>

                            <div class="col-md-6 text-center mb-2" >
                                <h3 class="text-uppercase mb-2">proyectos registrados</h3>
                                <h1 class="text-muted">0</h1>
                            </div>
                        </div>
                    </div> -->

                </div>

                
            </div> 
        </div> <!-- end col-->
    </div>                       
    
</div>
@endsection

@section('scripts')

<script src="{{asset('libs/owl/owl.carousel.min.js')}}"></script>
<script>
$(document).ready(function(){

    $(".list-menu-account #myrutan").addClass("active");
    $(".navbar-rutan-mobile .notification-list .id-myrutan").addClass("active");


    $('.owl-carousel').owlCarousel({
        loop:true,
        dots:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:7000,
        autoplayHoverPause:true,        
        smartSpeed:450,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:3
            }
        }
    });

}); 


</script>
@endsection