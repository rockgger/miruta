@extends('app.index')

@section("styles")
<link href="{{asset('css/register.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid animated fadeIn">      
                     

    <div class="row">
        <div class="col-md-12">           
            
            <div class="card-account my-rutan my-configuration">
                <livewire:aplication.profile.profile >                
            </div> 
        </div> <!-- end col-->
    </div>                       
    
</div>
@endsection
