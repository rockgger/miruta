<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    @include('app.head')
    <link href="{{asset('css/menu-bar.css')}}" rel="stylesheet" type="text/css" />
    <body class="" >
        
        <!-- Begin page -->
        <div id="wrapper ">    
            
            <!-- menú superior -->
            <div class="container-fluid  content-account">      
        
                @include('app.top-menu')


                <div class="content-wrapp">
                    
                    @include('app.menu')

                    <!-- contenido -->
                    <div class="content content-account-rn">

                    <!-- Start Content-->
                        @yield('content')
                    <!-- container -->

                    </div> <!-- content -->

                    <footer class="footer-rn mt-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-6">
                                    <img class="" src="{{asset('image/logo-rutan.png')}}" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <div class="text-md-right text-footer-rn">
                                        Todos los derechos reservados © {{ date('Y') }} <a href="https://www.rutanmedellin.org/" target="_blank">Ruta N </a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    
                </div>
                
                

            </div> 

            


            
        </div>
        <!-- END wrapper -->
        @include('app.menu-mobile-bottom')
        @include('app.menu-mobil')
        <!-- script JS -->
        @include('app.footer-script')

        <script>
        $(document).ready(function () {
            {{-- $('.slimscroll-menu-l').slimscroll({
                height: 'auto',
                position: 'right',
                size: "8px",
                color: '#ccc',
                wheelStep: 5,
                touchScrollStep: 20
            }); --}}

            
        });        
        </script>
        
    </body>
</html>