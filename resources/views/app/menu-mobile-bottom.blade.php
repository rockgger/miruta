{{--   MENU Vs MOVIL    --}}
<div class="navbar-rutan-mobile" x-data="">
    <div class="menu-options">
        <ul class="list-unstyled topnav-menu ">

            <li class="notification-list">
                <a class="nav-link  nav-user me-0 waves-effect waves-light id-index"   href="{{route('app.home')}}" role="button" >                    
                    <i class="rn-home"></i>                 
                </a>                
            </li>

            <li class="notification-list">
                <a class="nav-link  nav-user me-0 waves-effect waves-light id-myrutan"  href="{{ route('app.myrutan') }}" role="button" >                    
                    <i class="rn-miruta"></i>                 
                </a>                
            </li>

            

            <li class="dropup notification-list">
                <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="mdi mdi-bell noti-icon active"></i>
                    <!-- <span class="badge badge-primary rounded-circle noti-icon-badge">3</span> -->
                </a>
                <div class="dropdown-menu dropdown-menu-right-ed  dropdown-lg">

                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="m-0 text-uppercase">
                            <span class="">
                                <a href="javascript:void(0);" class="text-dark">
                                    <small>Notificaciones</small>
                                </a>
                            </span>
                        </h5>
                    </div>

                    <div class="slimscroll noti-scroll">

                        
                        
                    </div>

                    <!-- All-->
                    <a href="javascript:void(0);" class="d-block text-center notify-item notify-all">
                        Ver todos
                    </a>

                </div>
            </li>

            <li class="notification-list">
                <a class="nav-link  nav-user me-0 waves-effect waves-light show_menu_mobile " x-on:click="toggleMenuMobile();" href="javascript:void(0);" role="button" >
                    <img src="{{Auth()->user()->photo}}" alt="user-image" class="rounded-circle">                    
                </a>                
            </li>
        </ul>

    </div>
</div>