<div class="dropstart"  x-data="{show:false}">
    <a href="javascript:void(0);"  x-on:click="show = ! show" class="btn btn-primary text-uppercase waves-effect waves-light btn-offert btn-w-md me-3">
        <i class="fe-plus" x-show="!show"></i>
        <i class="fe-minus" x-show="show"></i>
    nuevo
    </a>

    <div class="dropdown-menu dropdown-alt-dashboard" x-show="show" x-transition  @click.outside="show = false">
        <div>
            <a class="dropdown-item"  href="#oportunidades-option2" data-bs-toggle="collapse">
                oportunidad 
                <span class="menu-arrow"></span>
            </a>
            <div class="collapse secound-options-dashboard" id="oportunidades-option2">
                <ul class="nav-second-level ps-0">
                    <li>
                        <a href="{{ route('admin.opportunity.standard.create') }}">Estándar</a>
                        <a href="{{ route('admin.opportunity.multioffer.create') }}">Multi oferta</a>
                        <a href="{{ route('admin.opportunity.landing.create') }}">Landing</a>
                        <a href="{{ route('admin.opportunity.hiring.create') }}">Contratación</a>
                    </li>
                </ul>
            </div>
        </div>
        
        <a class="dropdown-item " href="{{ route('admin.news.create') }}">noticia</a>
        <a class="dropdown-item " href="javascript:void(0);">evento</a>
        <a class="dropdown-item " href="{{ route('admin.team.index') }}">compañero de equipo</a>
        <a class="dropdown-item " href="{{ route('admin.users') }}">Usuario</a>
    </div>
</div>