<div>
    <div id="{{ $idmodal }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable">
            <div class="modal-content" x-data="{listopportunities:@entangle('list_opportunities').defer, totalop:@entangle('total_opportunities')}" >
                <div class="modal-header text-center">
                    <h3 class="text-uppercase text-brandon mt-2 mb-0 w-100 text-primary">{{ $title }} <br><small class="text-lowercase text-secondary" x-text="totalop+' oportunidades encontradas'"></small></h3>
                    
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true"><i class="fe-x"></i></button>
                </div>
                <div class="modal-body px-md-3 mb-4" wire:loading.remove wire:target='list_opportunities'> 
                    <div class="row">
                        @if (session()->has('message.success'))
                            <div class="alert alert-success alert-dismissible msg fadeIn animated" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                {{ session('message.success') }}
                            </div>
                        @endif
                        <div class="col-md-12 "> 
                            <div class="row">
                                <template x-for="list in listopportunities">
                                    <div class="col-lg-6">
                                        <div class="card-label  mb-3 d-flex text-blak-gray flex-column align-items-start h-auto position-relative listopportunities" :class="{'op-standar':list.type_id == 1, 'op-landing':list.type_id == 4}">
                                            <span class="title-label text-lowercase" x-text="list.title"></span>
                                            <span class="activity-data-date"><b>Estado: </b> <span x-text="(list.open_status_id == 3)?'Abierta':'Cerrada'"></span></span>
                                            <span>Oportunidad <span x-text="(list.type_id == 1)?'Estándar':'landing'"></span></span>

                                            <button title="Agregar" x-on:click="opportunities.push(list); listopportunities.splice( listopportunities.indexOf(list), 1 ); " ><i class="fe-plus text-primary"></i></button></span>
                                        </div>
                                         
                                    </div>
                                   
                                </template>
                                <template x-if="listopportunities.length <= 0 ">
                                    <p class="text-muted text-center w-100" x-transition>Sin resultados.</p>
                                </template>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class=" mt-3 d-flex align-items-center justify-content-center p-0 ">
                            <button x-on:click="$wire.loadModeOpportunities(opportunities)" wire:loading.remove type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">Cargar más</button>

                            <button  wire:loading disabled type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect width-lg"><i class="mdi mdi-spin mdi-loading"></i></button>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-body px-md-5 mb-4"  wire:loading wire:target='list_opportunities'>
                    <div class="col-12">
                        <div class="card-loading" style="min-height: auto;">
                            <div class="spinner-border avatar-md text-contrast m-0 p-0" ></div>
                        </div>
                    </div>
                </div>

                {{-- <div class="modal-footer mb-4 flex-column align-items-center justify-content-end p-0">

                    <button x-on:click="$wire.loadModeOpportunities(opportunities)" wire:loading.remove type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">Cargar más</button>

                    <button  wire:loading disabled type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect width-lg"><i class="mdi mdi-spin mdi-loading"></i></button>
                </div> --}}

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>