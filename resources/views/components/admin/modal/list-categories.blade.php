<div>
    <div id="{{ $idmodal }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" x-data="{listcategories:@entangle('list_categories').defer}" >
                <div class="modal-header text-center">
                    <h3 class="text-uppercase text-brandon mt-2 mb-0 w-100 text-primary">{{ $title }}</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true"><i class="fe-x"></i></button>
                </div>
                <div class="modal-body px-md-5 mb-4" wire:loading.remove> 
                    <div class="row">
                        @if (session()->has('message.success'))
                            <div class="alert alert-success alert-dismissible msg fadeIn animated" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                {{ session('message.success') }}
                            </div>
                        @endif
                        <div class="col-md-12 "> 
                            <div class="d-flex flex-wrap">
                                <template x-for="list in listcategories">
                                    <span class="badge-opportinity justify-content-between badge-soft-secondary rounded-pill w-100"><span x-text="list.name"></span> <button title="Agregar" x-on:click="categories.push(list); listcategories.splice( listcategories.indexOf(list), 1 ); " ><i class="fe-plus text-primary"></i></button></span>
                                </template>
                                <template x-if="listcategories.length <= 0 ">
                                    <p class="text-muted text-center w-100" x-transition>Sin resultados.</p>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body px-md-5 mb-4"  wire:loading>
                    <div class="col-12">
                        <div class="card-loading" style="min-height: auto;">
                            <div class="spinner-border avatar-md text-contrast m-0 p-0" ></div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>