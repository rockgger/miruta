<div>
    <div id="{{ $idmodal }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 class="text-uppercase text-brandon mt-2 mb-0 w-100 text-primary">{{ $title }}</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true"><i class="fe-x"></i></button>
                </div>
                <div class="modal-body px-md-5 "> 
                    <div class="row">
                        <div class="col-12" x-data="{}">
                            <div class="mb-2 component-upload" x-on:click="$refs.photo.click();">
                                
                                @if($photo)
                                    <img class="w-100 object-fit-cover avatar-md rounded-circle" src="{{ $photo->temporaryUrl() }}" alt="" wire:loading.remove wire:target="updatedPhoto">
                                @elseif($phototemp != null)
                                    <img class="w-100 object-fit-cover avatar-md rounded-circle" src="{{ $phototemp }}" alt="" wire:loading.remove wire:target="updatedPhoto">
                                @else
                                    <div class="d-flex flex-column align-items-center justify-content-center" wire:loading.remove wire:target="updatedPhoto">
                                        <i class="rn-photo-upload"></i>
                                        <p class="m-0 mt-1 text-muted">Subir foto</p>
                                    </div>
                                @endif
                                <i class="mdi mdi-spin mdi-loading" wire:loading wire:target="updatedPhoto"></i>
                            </div>
                            @error('photo')
                                <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                            @enderror
                            <input type="file" x-ref="photo" class="invisible" wire:model.defer='photo' accept="image/*">
                            
                        </div>

                        <div class="col-md-6 " x-data="{labels:@entangle('labels'), tag_group:@entangle('tag_group')}"> 
                            <div class="form-group mb-2">
                                <input type="text" wire:model.defer='name' class="form-control input-profile" placeholder="Nombre" >
                                @error('name')
                                    <span class="text-danger w-100 d-block ">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group mb-2">
                                <input type="text" wire:model.defer='role' class="form-control input-profile" placeholder="Rol" >
                                @error('role')
                                    <span class="text-danger w-100 d-block ">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <input type="email" wire:model.defer='email' class="form-control input-profile" placeholder="Email" >
                                @error('email')
                                    <span class="text-danger w-100 d-block ">{{ $message }}</span>
                                @enderror
                            </div>


                            <div class="row" x-show="!labels.length > 0" >
                                <div class="col-sm-4">
                                    <label class="label-rn">Área: </label>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <select class="select-rutan" wire:model='tag_group_id'>
                                            <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                                            <template x-for="tag in tag_group">
                                                <option :value="tag.id" x-text="tag.name"></option>
                                            </template>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row" x-show="labels.length > 0" x-transition>
                                <div class="col-sm-3">
                                    <label class="label-rn">Área: </label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select class="select-rutan" wire:model.defer='area_id'>
                                            <option class="text-muted"  selected >Seleccionar opción</option>
                                            <template x-for="label in labels">
                                                <option :value="label.id" x-text="label.name"></option>
                                            </template>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>  
                        </div>

                        <div class="col-md-6 "> 
                            <div class="form-group mb-2">
                                <textarea type="text" class="form-control input-profile"  wire:model.defer='phrase'  style="height: 80px;" placeholder="Frase" ></textarea>
                            </div>
                            <div class="form-group mb-2">
                                <textarea type="text" class="form-control input-profile" wire:model.defer='testimony'  placeholder="Testimonio" ></textarea>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer mb-4 mt-3 flex-column align-items-center justify-content-end p-0">
                    {{ $btnEvents }}
                    <button type="button" disabled class="btn btn-primary width-lg" wire:loading wire:target="store, update"><i
                    class="mdi mdi-spin mdi-loading"></i></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
