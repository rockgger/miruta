<div>
    <div id="{{ $idlabel }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 class="text-uppercase text-brandon mt-2 mb-0 w-100 text-primary">{{ $title }}</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true"><i class="fe-x"></i></button>
                </div>
                <div class="modal-body px-md-5 "> 
                    <div class="row">
                        @if (session()->has('message.success'))
                            <div class="alert alert-success alert-dismissible msg fadeIn animated" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                {{ session('message.success') }}
                            </div>
                        @endif
                        <div class="col-md-12 "> 
                            <div class="form-group">
                                <p class="label-rn">Nombre en español</p>
                                <input type="text" wire:model.defer='name_es' class="form-control input-profile " placeholder="Nombre en español" >
                                @error('name_es')
                                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 "> 
                            <div class="form-group">
                                <p class="label-rn">Nombre en ingles</p>
                                <input type="text"  wire:model.defer='name_en' class="form-control input-profile " placeholder="Nombre en ingles" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mb-4 mt-3 flex-column align-items-center justify-content-end p-0">
                    {{ $btnEvents }}
                    
                    <div class="col-12" wire:loading>
                        <div class="card-loading" style="min-height: auto;">
                            <div class="spinner-border avatar-md text-contrast m-0 p-0" ></div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
