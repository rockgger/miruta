<div x-data="{avatars:$wire.avatares, pt:@entangle('photo')}">
    <div id="{{ $idmodal }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" >
                <div class="modal-header text-center">
                    <h3 class="text-uppercase text-brandon mt-2 mb-0 w-100 text-primary">{{ $title }}</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true"><i class="fe-x"></i></button>
                </div>
                <div class="modal-body px-md-2 mb-2" wire:loading.remove> 
                    <div class="row">
                        @if (session()->has('message.success'))
                        <div class="col-md-12 "> 
                            <div class="alert alert-success alert-dismissible msg fadeIn animated" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                {{ session('message.success') }}
                            </div>
                        </div>
                        @endif
                        <template x-for="avatar in avatars">
                            <div class="col-sm-4 col-6">
                                <div class="box-select-avatar" :class="{'active': pt == avatar}" x-on:click="pt = avatar;">
                                    <img :src="avatar" class="img-fluid rounded-circle img-my-ruta" alt="avatar">
                                </div>
                            </div>
                        </template>
                    </div>
                </div>
                <div class="modal-body px-md-5 mb-2"  wire:loading>
                    <div class="col-12">
                        <div class="card-loading" style="min-height: auto;">
                            <div class="spinner-border avatar-md text-contrast m-0 p-0" ></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mb-4 mt-3 flex-column align-items-center justify-content-end p-0">
                    <button  type="button"  data-bs-dismiss="modal" class="btn btn-primary  text-uppercase text-brandon waves-effect">Seleccionar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>