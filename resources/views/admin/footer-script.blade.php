<!-- Vendor js -->
<script src="{{ asset('js/vendor.min.js') }}"></script>

<script src="{{ asset('libs/sweetalert2/sweetalert2.min.js') }}"></script>
@stack('before-scripts')
@yield('before-scripts')
<!-- App js -->
<script src="{{ asset('js/app.js') }}"></script>

<!-- Tost-->
<script defer src="{{ asset('libs/jquery-toast-plugin/jquery.toast.min.js') }}"></script>
<script defer src="{{ asset('libs/jquery-toast-plugin/toastr.init.js') }}"></script>

<script src="{{ asset('js/notification.js') }}"></script>

@yield('scripts')
@stack('scripts')

@livewireScripts
