@extends('admin.index')
@section('content')
<div class="container">                         

    <h2 class="text-uppercase text-primary ">Equipo de trabajo</h2>

    <section class="mt-3 mt-md-4">
        <livewire:admin.team.list-team>
    </section>
</div> 
@endsection 