<div class="left-side-menu-rutn" x-data="{}">
        
    <button type="button"
        x-on:click="document.querySelector('.left-side-menu-rutn').classList.remove('left-menu-movile-active')"
        class="btn-cloce-left-bar left-bar-toggle waves-effect waves-light"><i class="fe-x"></i></button>
    <div class="logo-box">
        <a href="{{ route('admin.home') }}" class="logo logo-light text-center">
            <span class="logo-lg">
                <img src="{{ asset('image/logo-alt.png') }}" alt="" height="69">
            </span>
        </a>
    </div>
    <hr class="mt-0 mb-2 mb-md-4 bg-transparent">

    <div class="scrollbar-simplebar pb-4" data-simplebar>
        <!--- Sidemenu -->
        <div id="sidebar-menu" >
           
            <ul id="side-menu">
                <li>
                    <a href="{{ route('admin.home') }}" class="option-firts-menu">
                        <i class="rn-dashboard"></i>
                        <h5> Dashboard </h5>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.home.edit') }}" class="option-firts-menu">
                        <i class="rn-home"></i>
                        <h5> editar home </h5>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.team.index') }}" class="option-firts-menu">
                        <i class="rn-team"></i>
                        <h5> Equipo de trabajo </h5>
                    </a>
                </li>

                <li>
                    <a href="#oportunidades-option" class="option-firts-menu" data-bs-toggle="collapse">
                        <i class="rn-portfolio"></i>
                        <h5> oportunidades </h5>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="oportunidades-option">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ route('admin.opportunity.standard') }}">Estándar</a>
                                <a href="{{ route('admin.opportunity.multioffer') }}">Multi oferta</a>
                                <a href="{{ route('admin.opportunity.landing') }}">Landing</a>
                                <a href="{{ route('admin.opportunity.hiring') }}">Contratación</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="{{ route('admin.news') }}" class="option-firts-menu">
                        <i class="rn-noticias"></i>
                        <h5> noticias </h5>
                    </a>
                </li>

                {{-- <li>
                    <a href="#" class="option-firts-menu">
                        <i class="rn-eventos"></i>
                        <h5> Eventos </h5>
                    </a>
                </li>
                <li>
                    <a href="#" class="option-firts-menu">
                        <i class="rn-toolkit"></i>
                        <h5> Toolkit </h5>
                    </a>
                </li> --}}
                <li>
                    <a href="{{ route('admin.users') }}" class="option-firts-menu">
                        <i class="rn-users-setting"></i>
                        <h5> Usuarios </h5>
                    </a>
                </li>
                {{-- <li>
                    <a href="#" class="option-firts-menu">
                        <i class="rn-pqrs"></i>
                        <h5> PQRS </h5>
                    </a>
                </li> --}}
                <li>
                    <a href="{{ route('admin.label.index') }}" class="option-firts-menu">
                        <i class="rn-category"></i>
                        <h5> Etiquetas </h5>
                    </a>
                </li>
                <li>
                    <a href="#" class="option-firts-menu">
                        <i class="rn-setting"></i>
                        <h5> Configuración </h5>
                    </a>
                </li>
            </ul>
        </div>
        
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    

    <!-- Sidebar -left -->

</div>
