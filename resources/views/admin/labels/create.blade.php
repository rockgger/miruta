@extends('admin.index')
@section('content')
<div class="container">                         

     
    <section class="d-flex justify-content-between align-items-center">
        <h2 class="text-uppercase text-primary ">Nuevo grupo de etiquetas</h2> 
        <a href="{{ route('admin.label.index') }}" class="btn-text"><i class="fe-x"></i>Cancelar</a>
    </section>

    <section class="mt-3 mt-md-4">
        <livewire:admin.label.create-group>
    </section>
</div> 
@endsection