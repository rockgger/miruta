@extends('admin.index')
@section('content')
<div class="container">                         

    <h2 class="text-uppercase text-primary ">Etiquetas</h2>  
    <section class="d-flex justify-content-between align-items-center mt-2">
        <h4 class="text-uppercase">Grupos de etiquetas</h4>
        <a href="{{ route('admin.label.create') }}" class="btn btn-custom  text-uppercase" >NUEVO GRUPO DE ETIQUETAS</a>
    </section>

    <section class="mt-3 mt-md-4">
        <livewire:admin.label.label-list>
    </section>
</div> 
@endsection