
<div class="navbar-rutan-admin">
    <div class="container">
        <div class="d-flex justify-content-end align-items-center">
            <x-admin.dashboard.btn-nuevo/>

            <div class="menu-options ps-2">
                <div class="nav-link nav-user me-0 waves-effect waves-light d-flex flex-column align-items-start justify-content-start " >
                    <a href="#" class="position-relative d-flex flex-column align-items-start justify-content-start">
                        <span class="pro-user-name ms-0 text-uppercase font-18 mb-0 line-height-1 text-brandon"><b>{{ auth()->user()->fullName() }}</b></span>
                        <span class="text-muted line-height-1 ">Editar mi perfil</span>
                    </a>
                    <a href="{{ route('auth.logout') }}" class="text-danger font-18 pt-1"><b>Salir</b></a>
                </div>
            </div>
        </div>
    </div>    
</div>