@extends('admin.index')

@section('before-styles')
<link href="{{asset('libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
<link href="{{asset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/switchery-custom.css')}}" rel="stylesheet" type="text/css" />
@endsection 


@section('content')
<div class="container"> 
    <div class="d-flex justify-content-start">
        <h2 class="text-uppercase text-primary ">Editar noticia</h2>
        
    </div>
    <section class="mt-3 mt-md-4">
        <livewire:admin.noticias.edit :noticia_id="$noticia_id">
    </section>
</div> 
@endsection 

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script src="{{asset('libs/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('libs/summernote/summernote-es-ES.min.js')}}"></script>
<script src="{{asset('libs/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('libs/mohithg-switchery/switchery.min.js')}}"></script>
@endsection 
