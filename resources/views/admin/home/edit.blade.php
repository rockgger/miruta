@extends('admin.index')
@section('content')
<div class="container">                         

    <h2 class="text-uppercase text-primary ">Editar home</h2>

    <section class="mt-3 mt-md-4">
        <livewire:admin.home.edit-hero>
    </section>


    <section class="mt-3">
        <livewire:admin.home.edit-banner>
    </section>
</div> 
@endsection 