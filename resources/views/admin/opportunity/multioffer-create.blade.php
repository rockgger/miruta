@extends('admin.index')

@section('before-styles')
<link href="{{asset('libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection 

@section('content')
<div class="container"> 
    <div class="d-flex justify-content-start">
        <h2 class="text-uppercase text-primary ">Nueva oportunidad multioferta</h2>
        
    </div>
    <section class="mt-3 mt-md-4">
        <livewire:admin.opportunities.multi-offer-create>
    </section>
</div> 
@endsection 

@section('scripts')
<script src="{{asset('libs/dropify/js/dropify.min.js')}}"></script>
@endsection 