@extends('admin.index')

@section('before-styles')
<link href="{{asset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/switchery-custom.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('libs/selectize/css/selectize.bootstrap3.css') }}" rel="stylesheet" type="text/css" />
@endsection 

@section('content')
<div class="container"> 
    <div class="d-flex justify-content-between">
        <h2 class="text-uppercase text-primary ">Editar oportunidad contratación</h2>
        <div class="d-flex justify-content-end">
            <a href="{{ route('admin.opportunity.hiring') }}" class="btn-text"><i class="fe-x"></i>
                Volver</a>
        </div>
    </div>
    <section class="mt-3 mt-md-4">
        <livewire:admin.opportunities.hiring-edit :opportunity_id="$opportunity_id">
    </section>
</div> 
@endsection 

@section('scripts')
<script src="{{asset('libs/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('libs/mohithg-switchery/switchery.min.js')}}"></script>
<script src="{{ asset('libs/selectize/js/standalone/selectize.min.js') }}"></script>
<script src="{{ asset('libs/jquery-mask-plugin/jquery.mask.min.js') }}"></script>
<script src="{{ asset('libs/jquery-mask-plugin/form-masks.init.js') }}"></script>
@endsection 