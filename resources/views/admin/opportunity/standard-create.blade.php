@extends('admin.index')

@section('before-styles')
<link href="{{asset('libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection 

@section('content')
<div class="container"> 
    <div class="d-flex justify-content-between">
        <h2 class="text-uppercase text-primary ">Nueva oportunidad estándar</h2>
        <div class="d-flex justify-content-end">
            <a href="{{ route('admin.opportunity.standard') }}" class="btn-text"><i class="fe-x"></i>
                cancelar</a>
        </div>
    </div>
    <section class="mt-3 mt-md-4">
        <livewire:admin.opportunities.standard-create>
    </section>
</div> 
@endsection 

@section('scripts')
<script src="{{asset('libs/dropify/js/dropify.min.js')}}"></script>
@endsection 