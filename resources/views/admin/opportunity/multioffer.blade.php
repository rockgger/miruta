@extends('admin.index')
@section('content')
<div class="container">                         

    <h2 class="text-uppercase text-primary ">oportunidades multiofertas</h2>

    <section class="mt-3 mt-md-4">
        <livewire:admin.opportunities.multi-offer>
    </section>
</div> 
@endsection 