@extends('admin.index')
@section('content')
<div class="container">                         

     
    <section class="d-flex justify-content-between align-items-center">
        <h2 class="text-uppercase text-primary ">Editar grupo de etiquetas</h2> 
        <a href="{{ route('admin.label.index') }}" class="btn-text"><i class="fe-arrow-left"></i>Volver</a>
    </section>

    <section class="mt-3 mt-md-4">
        @livewire('admin.profiles.edit')
    </section>
</div> 
@endsection