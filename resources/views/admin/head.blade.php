<head>
    <meta charset="utf-8" />
    <title>Ruta N Digital</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="Ruta N Medellín" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   
    <link rel="shortcut icon" href="{{asset('image/icon-32x32.png')}}">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,700i,900&display=swap" rel="stylesheet">
    <!-- Jquery Toast css -->
    <link href="{{ asset('libs/jquery-toast-plugin/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    @yield('before-styles')
    @stack('before-styles')
    
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/sweetalert2/sweetalert2.min.css') }}">
    
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/icon-rn.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/rutan.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/rutan-mobile.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    
    @stack('before-alpinejs')
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    
    @yield('styles')
    @stack('styles')

    @livewireStyles

</head>