<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('admin.head')


<body class="loading">

    <!-- Begin page -->
    <div id="wrapper">

        

        <!-- ========== Left Sidebar Start ========== -->
        @include('admin.left-menu')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
        @include('admin.menu-top')
        
        <div class="content-page">
            <!-- Topbar Start -->
            {{-- @include('admin.top-menu') --}}
            <!-- end Topbar -->
            <div class="content">

                <!-- Start Content-->
                @yield('content')
                 <!-- container -->

            </div> <!-- content -->

            <!-- Footer Start -->
            
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- script JS -->
    @include('admin.footer-script')

</body>

</html>