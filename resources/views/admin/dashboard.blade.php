@extends('admin.index')

@section('content')
<div class="container">                         

    <h2 class="text-uppercase text-primary text-dashboard">Dashboard</h2>  
    
    <section class="mt-3">
        <a href="#" class="d-flex align-items-center"><h4 class="text-uppercase text-blak-gray me-2">Secciones fijas</h4> <i class="rn-setting-2"></i></a>
        
        <div class="d-flex align-items-center" x-data="{cdx:0}">
            <div class="mt-3 w-75 overflow-hidden me-3" x-ref="sectionsfix">
                <div class="list-sections-fixer ps-1" x-ref="sectionsover">
                @for($i = 1; $i <= 10; $i++)
                    <div class="card-label mb-2 d-flex text-blak-gray align-items-center justify-content-between w-mn-300 me-3 ">
                        <div class="d-flex align-items-center">
                            <div class="icon-label">
                                <i class="rn-portfolio"></i>
                            </div>                    
                            <div class="d-flex flex-column align-items-start">
                                <span class="title-label">Texto sección fija ({{ $i }})</span>
                            </div>
                        </div>
                    </div>
                @endfor
                    <div class="px-1" x-intersect="cdx = -324"></div>
                </div>
            </div>
            <a href="javascript:void(0);" class="btn-icon" @click.prevent="cdx = cdx + 324; $refs.sectionsfix.scrollTo({ left: cdx, behavior: 'smooth' })"><i class="fe-arrow-right"></i></a>
        </div>
    </section>

    <section class="mt-5">
        <h4 class="text-uppercase text-blak-gray">Actividad reciente</h4>
        <div class="row mt-3">
            @forelse ($activities as $activity)
            <div class="col-md-4 ">
                <div class="card-label card-activitic-news mb-2 d-flex text-blak-gray flex-column align-items-start justify-content-between  op-{{  App\Helpers\Helper::opportunityTypeClass($activity['type_id']) }}">
                    <span class="title-label">{{ $activity['title'] }}</span>
                    <div class="d-flex flex-column">
                        <span class="activity-data-text text-{{  App\Helpers\Helper::opportunityTypeClass($activity['type_id']) }}">Oportunidad {{  App\Helpers\Helper::opportunityType($activity['type_id']) }}</span>
                        <span class="activity-data-date">{{ App\Helpers\Helper::diffForHumans($activity['created_at']) }}</span>
                    </div>
                </div>
            </div>
            @empty
                <div class="col-12">
                    <p class="text-center">No hay actividad reciente.</p>
                </div>
            @endforelse
        </div>
    </section>


    <section class="mt-5">
        <h4 class="text-uppercase text-blak-gray">Oportunidades abiertas</h4>
        <div class="row mt-3">
           
            <div class="col-md-3 ">
                <a href="{{ route('admin.opportunity.standard') }}" class="d-block mb-2 text-center oportunity-hover">
                    <div class="d-flex text-blak-gray flex-column align-items-center">
                        <span class="activity-title mb-0 font-20">Estándar</span>
                        <span class="activity-data-number">{{ $count_standar }}</span>
                         
                    </div>
                    <button class="btn-text">Ir <i class="fe-arrow-right"></i></button>
                </a>
            </div>

            <div class="col-md-3 ">
                <a href="{{ route('admin.opportunity.multioffer') }}" class="d-block mb-2 text-center oportunity-hover">
                    <div class="d-flex text-blak-gray flex-column align-items-center">
                        <span class="activity-title mb-0 font-20">Multioferta</span>
                        <span class="activity-data-number">{{ $count_multi }}</span>
                    </div>
                    <button class="btn-text">Ir <i class="fe-arrow-right"></i></button>
                </a>
            </div>

            <div class="col-md-3 ">
                <a href="{{ route('admin.opportunity.landing') }}" class="d-block mb-2 text-center oportunity-hover">
                    <div class="d-flex text-blak-gray flex-column align-items-center">
                        <span class="activity-title mb-0 font-20">Landing</span>
                        <span class="activity-data-number">{{ $count_landing }}</span>
                    </div>
                    <button class="btn-text">Ir <i class="fe-arrow-right"></i></button>
                </a>
            </div>

            <div class="col-md-3 ">
                <a href="{{ route('admin.opportunity.hiring') }}" class="d-block mb-2 text-center oportunity-hover">
                    <div class="d-flex text-blak-gray flex-column align-items-center">
                        <span class="activity-title mb-0 font-20">Contratación</span>
                        <span class="activity-data-number">{{ $count_hiring }}</span>
                    </div>
                    <button class="btn-text">Ir <i class="fe-arrow-right"></i></button>
                </a>
            </div>
        </div>
    </section>
</div> 
@endsection

@push('before-alpinejs')
    <script defer src="https://unpkg.com/@alpinejs/intersect@3.x.x/dist/cdn.min.js"></script>
@endpush