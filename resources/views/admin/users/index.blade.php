@extends('admin.index')

@section('before-styles')
<link href="{{ asset('libs/selectize/css/selectize.bootstrap3.css') }}" rel="stylesheet" type="text/css" />
@endsection 

@section('content')
<div class="container">                         

    <h2 class="text-uppercase text-primary ">Usuarios</h2>

    <section class="mt-3 mt-md-3">
        <livewire:admin.users.index>
    </section>
</div> 
@endsection 

@section('scripts')
<script src="{{ asset('libs/selectize/js/standalone/selectize.min.js') }}"></script>
@endsection 