@extends('auth.index')

@section("styles")
<link href="{{asset('css/login.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">                         

    <div class="row">
        <div class="col-md-7 img-login bg-img-login" >
            <div class="cont-logo">
                <img src="{{asset('image/logo-rutan.png')}}" alt="Ruta N">
            </div>
        </div>
        <div class="col-md-5">
            <!-- Portlet card -->
            <div class="card card-login">
                <div class="card-body">
                    <h1 class="text-uppercase text-primary">mi ruta</h1>
                    <h3 class="text-uppercase text-greyish-brown">El lugar donde se gesta la innovación</h3>
                    
                    <livewire:auth.login />
                    
                    <div class="footer-login">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="">¿Aún no tienes cuenta? <a href="{{ route('auth.register') }}" ><strong>Registrate.</strong></a></p>
                            </div>
                        </div>
                    </div>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>                       
    
</div> 
@endsection