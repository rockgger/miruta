<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    @include('auth.head')

    <body class="" >
        
        <!-- Begin page -->
        <div id="wrapper">   
            
        
                <div class="content">
                
                <!-- Start Content-->
                    @yield('content')
                <!-- container -->

                </div> <!-- content -->
        </div>
        <!-- END wrapper -->

        
        <!-- script JS -->
        @include('auth.footer-script')
        
    </body>
</html>