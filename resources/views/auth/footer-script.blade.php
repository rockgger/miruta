<!-- Vendor js -->
<script src="{{ asset('js/vendor.min.js') }}"></script>

<script src="{{ asset('libs/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/notification.js') }}"></script>

@yield('scripts')
@stack('scripts')

@livewireScripts
