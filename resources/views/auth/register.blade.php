 @extends('auth.index')

@section("styles")

<link href="{{asset('css/register.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="container-fluid">                         

    <div class="row">
        <div class="col-md-7 img-login bg-img-register" >
            <div class="cont-logo">
                <img src="{{asset('image/logo-rutan.png')}}" alt="Ruta N">
            </div>
        </div>
        <div class="col-md-5">
            <!-- Portlet card -->
            <div class="card card-login">
                <div class="card-body">
                    <h1 class="text-uppercase text-primary">Estás a un paso</h1>
                    <h3 class="text-uppercase text-greyish-brown">del lugar donde se gesta la innovación</h3>
                    
                    <p class="text-apoyo text-uppercase text-primary text-brandon">Crea tu cuenta</p>

                    <livewire:auth.register />

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>                       
    
</div> 
@endsection