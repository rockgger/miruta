<div>
    <!-- contenedor de resultados oportunidad -->
    
    <div class="tap-rn-content col-md-12  mt-4 mb-4" >
        <div class="col-md-12">
            <p><b>Filtrar por:</b></p>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="row" wire:ignore>
                        <!-- <div class="col-md-6 switchery-rn ptb-12">                               
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#fbf1ca" data-size="small"/>
                                            <label for="">Ruta N</label>
                                        </div> -->

                        <div class="col-md-6 switchery-rn ptb-12">
                            <input type="checkbox" data-plugin="switchery" data-color="#fbf1ca" checked
                                data-size="small" data-jackcolor="#f1cd3d" />
                            <label for="">Todo el ecosistema</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-6" >
                    <div class="row text-right">
                        <div class="col-md-6">
                            <p class="label-rn p-edit text-right">Cambiar categoría</p>
                        </div>
                        <div class="col-md-6">
                            <select class="select-rutan" wire:model="category_id">
                                <option value="0" selected >Todas</option>
                                @foreach ($categories_op as $cat)
                                    <option value="{{ $cat['id'] }}" >{{ $cat['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

        </div>
       
            <div class="col-md-12 mt-3" wire:loading.remove wire:target='opportunities, category_id'>
                <div class="row">
                    <div class="col-md-8 mb-2 result-opts">
                        <p>Hemos encontrado <b>{{ $opportunities_total }}</b> oportunidades según tu perfil
                        </p>
                    </div>
                    <div class="col-md-4 switchery-rn  mb-2 text-right" wire:ignore>
                        <input type="checkbox" data-plugin="switchery" checked/>
                                        <label for="">Mostrar todas</label>
                    </div>
                </div>
            </div>
         @if($readyToLoad)
            <div class="col-md-12 mt-2 mb-5 list-result-oports" wire:loading.remove wire:target='opportunities, category_id'>
                <div class="row">
                    @forelse($opportunities as $j => $opportunity)
                    <div class="col-md-12 mb-3 mt-2">
                        <div class="card-list-result">
                            <div class="bc-img" style="background-image: url({{ $opportunity['image'] }})">

                            </div>
                            <div class="bc-body">
                                <h4 class="mb-0">
                                    <div class="text-uppercase mb-1 d-flex justify-content-between flex-column flex-md-row">
                                        <div>{{ $opportunity['title'] }}</div>
                                        <small class="text-muted">Publicado: {{ date('d/m/Y', strtotime($opportunity['created_at'])) }}</small>

                                    </div>
                                    
                                    <div class="d-flex flex-wrap">
                                    @foreach($opportunity['actors'] as $i=> $actor)
                                        <a href="#" class="flat-title d-inline-block">{{ $i>0?',':'' }} {{ $actor['name'] }}</a>
                                    @endforeach
                                    </div>
                                    
                                </h4>
                                <span><b>Estado: </b>{{ $opportunity['openstatus']['name'] }}</span>
                                <p class="mt-2">{{ Str::limit($opportunity['presentation'], 250, '...') }}</p>


                            </div>
                            <div class="bc-foot d-flex justify-content-between align-items-center mb-2  flex-column flex-md-row" >
                                <div class="d-flex flex-wrap">
                                    @foreach($opportunity['categories'] as $category)
                                    <a href="#"
                                        class="btn btn-light btn-rounded waves-effect ml-3">{{ $category['name'] }}</a>
                                    @endforeach    
                                </div>
                                
                                <a href="#"
                                    class="btn btn-primary text-uppercase waves-effect waves-light btn-offert">Ver
                                    oferta</a>
                            </div>
                        </div>
                    </div>
                        
                    @empty
                        <p class="text-center">No hay resultados para mostrar.</p>
                    @endforelse
                    @if($opportunities_total > 0  AND  $opportunities_total > count($opportunities))
                    <div class="col-md-12 mb-3 mt-2 text-center">
                        <a href="#">Cargar más.</a>
                    </div>
                    @endif
                </div>

            </div>

            <div class="col-md-12" wire:loading wire:target='opportunities, category_id'>
                <div class="card-loading" >
                    <div class="spinner-border avatar-md text-contrast m-2" role="status"></div>
                </div>
            </div>

        @else
            <!-- Fin  contenedor de resultados -->
            <div class="col-md-12" >
                <div class="card-loading" >
                    <div class="spinner-border avatar-md text-contrast m-2" role="status"></div>
                </div>
            </div>
        @endif
    </div>

    @push('scripts')
    <script>
        $(document).ready(function(){

            Livewire.emit('loadOpportunities');

        }); 
    </script>
    @endpush
   
</div>
