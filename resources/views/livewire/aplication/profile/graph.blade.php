<div   class="d-flex justify-content-center justify-content-md-end flex-wrap mb-3 mb-sm-0" x-data="{pg:@entangle('ptg')}" 
x-init='$nextTick(()=>{
    $(".pknob").knob({
        "format" : function (value) {
            return value + "%";
        }
    });
})'>
    <div class="me-0 me-sm-3" wire:ignore>
        <input class="pknob"  data-width="95" data-height="95" data-inputColor="#fdc300"  data-fgColor="rgba(45, 178, 160, 0.73)" :value="pg" data-skin="tron"  data-readOnly=true data-thickness=".2" data-min="0" data-max="100"/>
    </div>
    <div class="text-pknob">
        @if($ptg < 100)
        <p class="text-danger">Tú perfil aún se encuentra incompleto.</p>
        <a href="{{ route('app.myprofile') }}" class="btn btn-custom  text-uppercase pe-4 ps-4">Completar perfil</a>
        @else
        <p class="text-success">Perfil completo.</p>
        <a href="{{ route('app.myprofile') }}" class="btn btn-custom  text-uppercase pe-4 ps-4">Mi perfil</a>
        @endif
    </div>

    @push('scripts')
    <!-- KNOB JS -->
    <script src="{{asset('libs/jquery-knob/jquery.knob.min.js')}}"></script>
    <script>
    $(document).ready(function(){
        
    });
    </script>        
    @endpush
</div>
