<div x-data="{openpass:false}">
    <p x-show="!openpass" class="label-rn invisible">Contraseña</p>
    <button x-show="!openpass" type="button" x-on:click="openpass = !openpass" class="btn btn-custom change-pass-class  text-uppercase pr-4 pl-4 mb-3" >Cambiar contraseña</button>

    <div x-show="openpass"  x-transition>
        <div class="form-group">
            <p class="label-rn">Contraseña actual</p>
            <input type="password" wire:model.defer='current_password' class="form-control"
                    placeholder="Ingresa tu contraseña actual">
            @error('current_password')
                <span class="text-danger ml-2 w-100 d-block position-absolute ">{{ $message }}</span>
            @enderror                                               
        </div>

        <div class="form-group position-relative"  x-data="{show:false, icon:false}">
            <p class="label-rn">Contraseña nueva</p>
            <input type="password" class="form-control  password" x-ref="inpassword" wire:model.defer="new_password" name="new_password"
                placeholder="Contraseña" wire:ignore wire:loading.attr="disabled">
            <button type="button" class="btn-input" 
                x-on:click="icon = !icon,  show = !show, $refs.inpassword.setAttribute('type', changeTypePass(show))">
                <template x-if="icon">
                    <i class="fe-eye-off text-secondary"></i>
                </template>

                <template x-if="!icon">
                    <i class="fe-eye text-secondary"></i>
                </template>
            </button>
            @error('new_password')
                <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-group position-relative"  x-data="{show:false, icon:false}">
            <p class="label-rn">Repite la contraseña</p>
            <input type="password" class="form-control  password" x-ref="inpassword" wire:model.defer="repeat_new_password" name="repeat_new_password"
                placeholder="Contraseña" wire:ignore wire:loading.attr="disabled">
            <button type="button" class="btn-input" 
                x-on:click="icon = !icon,  show = !show, $refs.inpassword.setAttribute('type', changeTypePass(show))">
                <template x-if="icon">
                    <i class="fe-eye-off text-secondary"></i>
                </template>

                <template x-if="!icon">
                    <i class="fe-eye text-secondary"></i>
                </template>
            </button>
            @error('repeat_new_password')
                <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-group ">
            <button  type="button"  class="btn btn-custom change-pass-class  " wire:loading.remove wire:target="changePassword"
                wire:click='changePassword'>Cambiar contraseña</button>
            <button class="btn text-uppercase" wire:loading.remove wire:target="changePassword" x-on:click.prevent="openpass = !openpass" >Cancelar</button>

            <button wire:loading wire:target="changePassword" class="btn  btn-custom width-xl" disabled><i
                    class="mdi mdi-spin mdi-loading "></i></button>
        </div>

    </div>

    @push('scripts')
    <script>
        function changeTypePass(show){
            return show ? 'text':'password';
        }
    </script>
    @endpush
</div>
