<div class="row">   
    <div class="col-md-12 mb-4">
        <h3 class="text-uppercase mb-0 ">Mi perfil</h3>                        
    </div>

    <div class="col-md-12 mb-1">
        <p class="my-profile-rn text-uppercase text-brandon">Datos personales</p>                        
    </div>

    <div class="col-md-6">                        
        <img  src="{{$photo}}" class="img-fluid rounded-circle img-my-ruta " id="my-avatar"  alt="avatar" data-bs-toggle="modal" data-bs-target="#changeavatar">
        <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#changeavatar" class="change-avatar d-block d-sm-inline-block"><p class="profile-rn text-capitalize">Cambiar avatar</p></a>
    </div>

    <div class="col-md-6">
        <livewire:aplication.profile.graph :user="$user">
    </div>


    <div class="col-md-12">
        <div class=" " >
            <div class="">                                
                <form class="">
                    <div class="row">                        
                        @if (session()->has('message.error'))
                        <div class="col-md-12">  
                            <div class="alert alert-danger msg fadeIn animated" role="alert">
                                {{ session('message.error') }}
                            </div>
                        </div>
                        @endif
                        
                        <div class="col-md-6 "> 
                            <div class="form-group">
                                <p class="label-rn">Nombres</p>
                                <input type="text" class="form-control input-profile key_login" placeholder="Nombres" maxlength="50" wire:model.defer="name" >
                                @error('name')
                                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 ">
                            <div class="form-group">
                                <p class="label-rn">Apellidos</p>
                                <input type="text" class="form-control input-profile key_login"  placeholder="Apellidos" maxlength="50" wire:model.defer="last_name">
                            </div>
                        </div>

                        <div class="col-md-6 ">
                            <div class="form-group">
                                <p class="label-rn">Correo electrónico</p>
                                <input type="email" class="form-control input-profile key_login" wire:model="email" placeholder="Correo electrónico" maxlength="50" >
                                @error('email')
                                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                            
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <p class="label-rn">Perfil</p>
                                <select class="select-rutan"  wire:model.defer="profile_id" >
                                    <option class="text-muted" disabled >Selecciona un perfil</option>
                                    @foreach ($profiles as $profile)
                                        <option value="{{ $profile['id'] }}">{{ $profile['name'] }}</option>
                                    @endforeach
                                </select>
                                <div class="clearfix"></div>
                                @error('profile_id')
                                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                                
                        

                        <div class="col-md-6 ">
                            <div class="form-group">
                                <p class="label-rn">Celular</p>
                                <input type="text" class="form-control input-profile key_login"  wire:model.defer="cell_phone" placeholder="Número de celular" maxlength="50" >                                                
                            </div>
                        </div>
                            
                        <div class="col-md-6">

                            <livewire:aplication.profile.change-password>
                            
                        </div>

                        <div class="col-md-12 mt-sm-0 mt-xl-4 ">
                            <div class="form-group">
                                <p class="label-rn">Cuéntanos un poco más de ti</p>
                                <textarea row="10" class="form-control input-profile key_login maxlength"  placeholder="Escribe una pequeña bio, contando quién eres y que haces en tu vida cotidiana. Máximo 700 caracteres." maxlength="700" wire:model.defer="biography"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <p class="label-rn">Tipo identificación</p>
                                        <select class="select-rutan"  wire:model.defer="document_type_id" >
                                            <option class="text-muted" disabled >Selecciona un tipo</option>
                                            @foreach ($document_types as $document_type)
                                                <option value="{{ $document_type['id'] }}">{{ $document_type['name'] }}</option>
                                            @endforeach
                                        </select>
                                        <div class="clearfix"></div>
                                        @error('document_type_id')
                                            <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                                        @enderror                                             
                                    </div>
                                </div>

                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <p class="label-rn">Número de identificación</p>
                                        <input type="text" class="form-control input-profile key_login"  wire:model.defer="document_number" placeholder="Ej: 4365412344" maxlength="50" >                                                
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 ">
                            <div class="form-group">
                                <p class="label-rn">País</p>
                                <input type="text" class="form-control input-profile key_login"  wire:model.defer="country" placeholder="Escribe un país" maxlength="50" >                                                
                            </div>
                        </div>

                        <div class="col-md-6 ">
                            <div class="form-group">
                                <p class="label-rn">Ciudad</p>
                                <input type="text" class="form-control input-profile key_login"  wire:model.defer="city" placeholder="Escribe una ciudad" maxlength="50" >                                                
                            </div>
                        </div>

                        

                        
                        @if(count($interes) > 0)
                        <div class="col-md-12 mb-3 mt-3">
                            <p class="my-profile-rn text-uppercase text-brandon mb-0">Preferencias</p>   
                            <p class="text-dark mb-0">Selecciona algunos temas de interés para regalarte información mucho más personalizada.</p>
                        </div>

                        <div class="col-md-12">
                            <div class="row">                                                
                            @foreach($interes as $i => $inte)
                                <div class="col-sm-6 col-md-4  " >
                                    <div class="form-check me-4 mb-3 form-check-rutan ">
                                        <input type="checkbox" wire:model.defer="interes_ids" class="form-check-input" id="checkcategories{{ $inte['id'] }}"  value="{{ $inte['id'] }}">
                                        <label class="form-check-label label-rn" for="checkcategories{{ $inte['id'] }}" >{{ $inte['name'] }}</label>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        @endif

                        <div class="col-md-12  mt-5 mb-5">

                            @if($errors->any())
                                <p class="text-danger">Se presentó un error en uno de los campos requeridos.</p>
                            @endif
                            <button type="button" class="btn-login btn  btn-primary  waves-effect waves-light text-uppercase mb-2 btn-update" wire:click="update" wire:loading.remove wire:target="update" >GUARDAR CAMBIOS</button>

                            <button type="button" disabled class="btn-login btn btn-primary width-lg" wire:loading wire:target="update"><i class="mdi mdi-spin mdi-loading"></i></button>

                            <a href="{{ route('app.home') }}" class="btn btn-light btn-login text-dark   waves-effect waves-light text-uppercase mb-2" wire:loading.remove wire:target="update" >CANCELAR</a>
                        </div>

                    </div>
                    
                </form>
            
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div>


    <x-aplication.modal.avatar :idmodal="'changeavatar'"  >
        <x-slot name="title">
            Cambiar avatar
        </x-slot>
    </x-aplication.modal.avatar>
</div>
