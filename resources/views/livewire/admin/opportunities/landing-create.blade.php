<div x-data="{lang:'es', entities:@entangle('entities_ids').defer, tag_group:@entangle('tag_group'), interes:@entangle('interes_ids').defer, noticias:@entangle('noticias_ids').defer}">

    <div class="row">
        <div class="col-md-6">
            <div class="tap-opportunities me-1" :class="{ 'active': lang == 'es' }" @click="lang='es'">
                Español
            </div>
            <div class="tap-opportunities" :class="{ 'active': lang == 'en' }" @click="lang='en'">
                Inglés
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex justify-content-end">
                <a href="{{ route('admin.opportunity.landing') }}" class="btn-text"><i class="fe-x"></i>
                    cancelar</a>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        {{-- TITLE --}}
        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_es"
                    placeholder="Escribe un título en español para esta oportunidad ">
                @error('title_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_en"
                    placeholder="Escribe un título en Inglés para esta oportunidad ">
                @error('title_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        {{-- IMAGEN --}}
        <div class="col-md-6">
            <div class="form-group" >
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify cover_image"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="cover_image" name="image" data-height="220"/>
                </div>

                @error('cover_image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>

        <div class="col-md-6">
            <div class="row mt-2">
                <div class="col-md-6" x-show="$wire.tag_group_id == null">                      
                    <div class="form-group  ">
                        <label class="label-rn mb-3">Entidades o actores asociados: </label>
                        <select class="select-rutan" wire:model='tag_group_id'>
                            <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                            <template x-for="tag in tag_group">
                                <option :value="tag.id" x-text="tag.name"></option>
                            </template>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-12" x-show="$wire.tag_group_id != null" x-transition:enter>
                    <div class="form-group">
                        <p class="label-rn">Entidades o actores asociados <a href="javascript:void(0);" wire:click="resetGroup('tag_group_id', 'entities_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                        <div class="d-flex flex-wrap">
                                <template x-for="entitie in entities">
                                    <span class="badge-opportinity badge-soft-secondary rounded-pill"><span x-text="entitie.name"></span> <button x-on:click="entities.splice( entities.indexOf(entitie), 1 ); " ><i class="fe-x"></i></button></span>
                                </template>
                            <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadEntities(entities)" data-bs-toggle="modal" data-bs-target="#addEntities">
                                <i class="fe-plus"></i>
                                Agregar entidad
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        {{-- Categorías --}}
        <div class="col-md-12" x-show="$wire.categories_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Categorías de esta oportunidad: </label>
                <select class="select-rutan " wire:model='categories_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-12"  x-show="$wire.categories_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Categorías de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('categories_tag_group_id', 'categories_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($categories as $category)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkcategories'{{ $category['id'] }}"  value="{{ $category['id'] }}" wire:model.defer="categories_ids">
                            <label class="form-check-label label-rn" for="'checkcategories'{{ $category['id'] }}" >{{ $category['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        {{-- dirigida a --}}
        <div class="col-12"  x-show="$wire.dirigida_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Esta oportunidad va dirigida a <a href="javascript:void(0);" wire:click="resetGroup('dirigida_tag_group_id', 'dirigida_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($dirigida as $dg)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkdirigida'{{ $dg['id'] }}"  value="{{ $dg['id'] }}" wire:model.defer="dirigida_ids">
                            <label class="form-check-label label-rn" for="'checkdirigida'{{ $dg['id'] }}" >{{ $dg['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-12" x-show="$wire.dirigida_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Esta oportunidad va dirigida a: </label>
                <select class="select-rutan " wire:model='dirigida_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        {{-- Áreas de interés --}}
        <div class="col-md-6" x-show="$wire.interes_tag_group_id == null">                      
            <div class="form-group  w-mx-270 ">
                <label class="label-rn mb-3">Áreas de interés de esta oportunidad: </label>
                <select class="select-rutan" wire:model='interes_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12" x-show="$wire.interes_tag_group_id != null" x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Áreas de interés de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('interes_tag_group_id', 'interes_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                <div class="d-flex flex-wrap">
                    
                    <template x-for="inte in interes">
                        <span class="badge-opportinity badge-soft-secondary rounded-pill " ><span x-text="inte.name"></span> <button x-on:click="interes.splice( interes.indexOf(inte), 1 ); " ><i class="fe-x"></i></button></span>
                    </template>
                    

                    <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadInteres(interes)" data-bs-toggle="modal" data-bs-target="#addInteres">
                        <i class="fe-plus"></i>
                        Agregar categorías
                    </a>
                </div>
                
            </div>
        </div>
    </div>
    
    {{-- URL --}}
    <div class="row">
        <div class="col-12" >
            <div class="form-group">
                <p class="label-rn">URL del sitio web de la oportunidad</p>
                <input type="text" class="form-control " wire:model.defer="url_web"
                    placeholder="Pega acá la url a donde irán los usuarios para registrarse">
                @error('url_web')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    {{-- Resumen --}}
    <div class="row mt-2">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Descripción corta:</label>
                <textarea  cols="30" rows="10" class="form-control " wire:model.defer="summary_es"
                    placeholder="Escribe una descripción corta de esta oportunidad en español"></textarea>
                @error('summary_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Descripción corta:</label>
                <textarea  cols="30" rows="10" class="form-control " wire:model.defer="summary_en"
                    placeholder="Escribe una descripción corta de esta oportunidad en inglés"></textarea>
                @error('summary_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>


    {{-- En qué consiste --}}
    <div class="row mt-2">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">En qué consiste:</label>
                <textarea  cols="30" rows="10" class="form-control  form-control-h-270" wire:model.defer="description_es"
                    placeholder="Escribe la descripción de esta oportunidad en español"></textarea>
                @error('description_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">En qué consiste:</label>
                <textarea  cols="30" rows="10" class="form-control  form-control-h-270" wire:model.defer="description_en"
                    placeholder="Escribe la descripción de esta oportunidad en inglés"></textarea>
                @error('description_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>


    {{-- Video o imágen de contexto --}}
    <div class="row mt-2 " x-data="{option:@entangle('option_context').defer}">
        <div class="col-12 mb-2 pb-1">
            <p class="label-rn mb-0">Video o imágen de contexto</p>
            <span class="">Agrega un video o una imágen que contextualice la oportunidad</span>
        </div>
        <div class="col-md-6 mb-4 pb-2">
            <div class="tap-opportunities me-1" :class="{ 'active': option == 'video' }" @click="option='video'">
                Video
            </div>
            <div class="tap-opportunities" :class="{ 'active': option == 'image' }" @click="option='image'">
                Imágen
            </div>
        </div>

        <div class="col-12" x-show="option == 'video'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">URL del video</label>
                 <input type="text" class="form-control " wire:model.defer="url_video"
                    placeholder="Agrega una url de youtube o vimeo">
                @error('url_video')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="option == 'image'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-0">Adjuntar imágen</label>
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify context_image"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="context_image" name="context_image" data-height="220"/>
                </div>
                @error('context_image')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>

    {{-- como funciona --}}
    <div class="row" x-show="lang == 'es'" x-transition:enter x-data="{functions:@entangle('funtion_es').defer}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Como funciona:</label>
        </div>
        <template x-for="(funct, index) in functions" >
            <div  class="col-md-12"  x-transition>
                <div class="form-group position-relative" x-transition>
                    <span class="check-function" x-text="index + 1">1</span>
                    <textarea   class="form-control form-control-benefit" :value="funct" x-model.lazy="functions[index]" placeholder="Comienza a escribir en español"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="functions.splice( functions.indexOf(funct), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-12 d-none" >
            <div class="form-group position-relative">
                <span class="check-function" x-text="functions.length + 1">1</span>
                <textarea   class="form-control form-control-benefit" x-ref="newfunctions" placeholder="Comienza a escribir en español"></textarea>
            </div>
        </div>
        <div class="col-md-12 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="functions.push($refs.newfunctions.value); $refs.newfunctions.value = ''">
                    <i class="fe-plus"></i>
                    Agregar otro paso
                </a>
            </div>
        </div>
        
    </div>

    <div class="row" x-show="lang == 'en'" x-transition:enter x-data="{functions:@entangle('funtion_en').defer}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Como funciona:</label>
        </div>
        <template x-for="(funct, index) in functions" >
            <div  class="col-md-12"  x-transition>
                <div class="form-group position-relative" x-transition>
                    <span class="check-function" x-text="index + 1">1</span>
                    <textarea   class="form-control form-control-benefit" :value="funct" x-model.lazy="functions[index]" placeholder="Comienza a escribir en inglés"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="functions.splice( functions.indexOf(funct), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-12 d-none" >
            <div class="form-group position-relative">
                <span class="check-function" x-text="functions.length + 1">1</span>
                <textarea   class="form-control form-control-benefit" x-ref="newfunctions" placeholder="Comienza a escribir en inglés"></textarea>
            </div>
        </div>
        <div class="col-md-12 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="functions.push($refs.newfunctions.value); $refs.newfunctions.value = ''">
                    <i class="fe-plus"></i>
                    Agregar otro paso
                </a>
            </div>
        </div>
        
    </div>



    {{-- Programas y servicios --}}
    <div class="row mt-2" x-show="lang == 'es'" x-transition:enter x-data="{services:@entangle('services_es').defer, idimg:@entangle('id_file_input_es'), urlimg:@entangle('img_url_service_es'), title:'', descrip:''}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Programas y servicios:</label>
        </div>
        <template x-for="(servic, index) in services" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                <div class="form-group position-relative">
                        <div class="upload-img-services">
                            <figure>
                                <img :src="servic.image" x-model.lazy="services[index].image" >
                            </figure>
                        </div>                    
                    <input type="text" class="form-control form-control-alt p-0 mb-3" :value="servic.title" x-model.lazy="services[index].title" placeholder="Título en español">
                    <textarea   class="form-control form-control-benefit p-1" :value="servic.description" x-model.lazy="services[index].description"  placeholder="Descripción en español"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="services.splice( services.indexOf(servic), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 " >
            <div class="form-group position-relative">
                @if ($img_service_es)
                <i class="fe-x closed-benefit bg-transparent text-secondary" wire:click="clearImg" style="right: auto; " title="quitar imagén"></i>
                @endif
                <a href="javascript:void(0);" class="d-inline-flex" x-on:click="$refs.uploadimage.click()">
                    <div class="upload-img-services">
                        <figure>
                            @if ($img_service_es)
                                <img src="{{ $img_service_es->temporaryUrl() }}" wire:loading.remove wire:target='img_service_es' alt="">
                            @else
                                <div class="placeholder-image-default" wire:loading.remove wire:target='img_service_es'><i class="fe-image"></i></div>
                            @endif
                            <div class="placeholder-image-default" wire:loading.flex wire:target='img_service_es'><i class="mdi mdi-spin mdi-loading"></i></div>
                        </figure>
                        <span class="text-brandon text-uppercase">SUBIR ÍCONO</span>
                    </div>
                </a>
                
                @error('img_service_es')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
                <input type="file" class="invisible m-0 p-0 position-absolute" accept="image/*" x-ref="uploadimage"  wire:model="img_service_es"  name="" :id="idimg">
                {{-- <input type="text" class="m-0 p-0" :value="urlimg" x-ref="newimage"> --}}
                <input type="text" class="form-control form-control-alt p-0 mb-3" x-model="title" placeholder="Título en español">
                <textarea   class="form-control form-control-benefit p-1" x-model="descrip"  placeholder="Descripción en español"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="services.push({title:title, description:descrip, image:urlimg}); title = ''; descrip=''; urlimg=''; $wire.clearImg() ">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>

    </div>


    <div class="row mt-2" x-show="lang == 'en'" x-transition:enter x-data="{services:@entangle('services_en').defer, idimg:@entangle('id_file_input_en'), urlimg:@entangle('img_url_service_en'), title:'', descrip:''}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Programas y servicios:</label>
        </div>
        <template x-for="(servic, index) in services" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                <div class="form-group position-relative">
                        <div class="upload-img-services">
                            <figure>
                                <img :src="servic.image" x-model.lazy="services[index].image" >
                            </figure>
                        </div>                    
                    <input type="text" class="form-control form-control-alt p-0 mb-3" :value="servic.title" x-model.lazy="services[index].title" placeholder="Título en inglés">
                    <textarea   class="form-control form-control-benefit p-1" :value="servic.description" x-model.lazy="services[index].description"  placeholder="Descripción en inglés"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="services.splice( services.indexOf(servic), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4" >
            <div class="form-group position-relative">
                @if ($img_service_en)
                <i class="fe-x closed-benefit bg-transparent text-secondary" wire:click="clearImg" style="right: auto; " title="quitar imagén"></i>
                @endif
                <a href="javascript:void(0);" class="d-inline-flex" x-on:click="$refs.uploadimage.click()">
                    <div class="upload-img-services">
                        <figure>
                            @if ($img_service_en)
                                <img src="{{ $img_service_en->temporaryUrl() }}" wire:loading.remove wire:target='img_service_en' alt="">
                            @else
                                <div class="placeholder-image-default" wire:loading.remove wire:target='img_service_en'><i class="fe-image"></i></div>
                            @endif
                            <div class="placeholder-image-default" wire:loading.flex wire:target='img_service_en'><i class="mdi mdi-spin mdi-loading"></i></div>
                        </figure>
                        <span class="text-brandon text-uppercase">SUBIR ÍCONO</span>
                    </div>
                </a>
                
                @error('img_service_en')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
                <input type="file" class="invisible m-0 p-0 position-absolute" accept="image/*" x-ref="uploadimage"  wire:model="img_service_en"  name="" :id="idimg">
                {{-- <input type="text" class="m-0 p-0" :value="urlimg" x-ref="newimage"> --}}
                <input type="text" class="form-control form-control-alt p-0 mb-3" x-model="title" placeholder="Título en inglés">
                <textarea   class="form-control form-control-benefit p-1" x-model="descrip"  placeholder="Descripción en inglés"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="services.push({title:title, description:descrip, image:urlimg}); title = ''; descrip=''; urlimg=''; $wire.clearImg() ">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>

    </div>



    {{-- ACORDEON TITULO --}}
    <div class="row">
        <div class="col-12" >
            <h4 class="text-uppercase text-brandon mb-3">Acordeón</h5>
            <div class="form-group"  x-show="lang == 'es'" x-transition:enter>
                <p class="label-rn">Título para la sección ”Acordeón”</p>
                <input type="text" class="form-control " wire:model.defer="title_accordion_es"
                    placeholder="Agrega un título en español para la sección que tendrá la funcionalidad de Acordeón">
                @error('title_accordion_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group"  x-show="lang == 'en'" x-transition:enter>
                <p class="label-rn">Título para la sección ”Acordeón”</p>
                <input type="text" class="form-control " wire:model.defer="title_accordion_en"
                    placeholder="Agrega un título en inglés para la sección que tendrá la funcionalidad de Acordeón">
                @error('title_accordion_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    {{-- ACORDEONES --}}
    <div class="row" x-show="lang == 'es'" x-transition:enter x-data="{acordeones:@entangle('accordion_es')}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Acordeones</label>
        </div>
        <template x-for="(acord, index) in acordeones" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                {{-- <p x-text="index + 1"></p> --}}
                <div class="form-group position-relative" x-transition>
                    <input type="text" class="form-control form-control-alt p-0 mb-3":value="acord.title" x-model.lazy="acordeones[index].title" placeholder="Título en español">
                    <textarea   class="form-control form-control-benefit  p-1" :value="acord.description" x-model.lazy="acordeones[index].description" placeholder="Descripción en español"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="acordeones.splice( acordeones.indexOf(acord), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group ">
                <input type="text" class="form-control form-control-alt p-0 mb-3" x-ref="newtitle" placeholder="Título en español">
                <textarea   class="form-control form-control-benefit p-1" x-ref="newdescrip"  placeholder="Descripción en español"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="acordeones.push({title:$refs.newtitle.value, description:$refs.newdescrip.value}); $refs.newtitle.value = ''; $refs.newdescrip.value='' ">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>

    </div>

    <div class="row" x-show="lang == 'en'" x-transition:enter x-data="{acordeones:@entangle('accordion_en')}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Acordeones</label>
        </div>
        <template x-for="(acord, index) in acordeones" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                {{-- <p x-text="index + 1"></p> --}}
                <div class="form-group position-relative" x-transition>
                    <input type="text" class="form-control form-control-alt p-0 mb-3":value="acord.title" x-model.lazy="acordeones[index].title" placeholder="Título en inglés">
                    <textarea   class="form-control form-control-benefit  p-1" :value="acord.description" x-model.lazy="acordeones[index].description" placeholder="Descripción en inglés"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="acordeones.splice( acordeones.indexOf(acord), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group ">
                <input type="text" class="form-control form-control-alt p-0 mb-3" x-ref="newtitle" placeholder="Título en inglés">
                <textarea   class="form-control form-control-benefit p-1" x-ref="newdescrip"  placeholder="Descripción en inglés"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="acordeones.push({title:$refs.newtitle.value, description:$refs.newdescrip.value}); $refs.newtitle.value = ''; $refs.newdescrip.value='' ">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>

    </div>



    {{-- beneficios --}}
    <div class="row" x-show="lang == 'es'" x-transition:enter x-data="{benefites:@entangle('benefit_es').defer}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Beneficios:</label>
        </div>
        <template x-for="(benefit, index) in benefites" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                <div class="form-group position-relative" x-transition>
                    <i class="fe-check check-benefit"></i>
                    <textarea   class="form-control form-control-benefit" :value="benefit" x-model.lazy="benefites[index]" placeholder="Escribe un beneficio en español"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="benefites.splice( benefites.indexOf(benefit), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group position-relative">
                <i class="fe-check check-benefit"></i>
                <textarea   class="form-control form-control-benefit" x-ref="newbenefites" placeholder="Escribe un beneficio en español"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="benefites.push($refs.newbenefites.value); $refs.newbenefites.value = ''">
                    <i class="fe-plus"></i>
                   Agregar más beneficios
                </a>
            </div>
        </div>
        
    </div>

    <div class="row" x-show="lang == 'en'" x-transition:enter x-data="{benefites:@entangle('benefit_en').defer}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Beneficios:</label>
        </div>
        <template x-for="(benefit, index) in benefites" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                <div class="form-group position-relative" x-transition>
                    <i class="fe-check check-benefit"></i>
                    <textarea   class="form-control form-control-benefit" :value="benefit" x-model.lazy="benefites[index]" placeholder="Escribe un beneficio en inglés"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="benefites.splice( benefites.indexOf(benefit), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group position-relative">
                <i class="fe-check check-benefit"></i>
                <textarea   class="form-control form-control-benefit" x-ref="newbenefites" placeholder="Escribe un beneficio en inglés"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="benefites.push($refs.newbenefites.value); $refs.newbenefites.value = ''">
                    <i class="fe-plus"></i>
                   Agregar más beneficios
                </a>
            </div>
        </div>
    </div>




    {{-- OTRAS IMG --}}
    <div class="row mt-2">
        <div class="col-md-6">
            <div class="form-group" >
                <label class="label-rn">Añade una imágen para la sección ”Como funciona”</label>
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore style="max-width: 450px;">
                    <input type="file" class="dropify context_image"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="funtion_image"   data-height="220"/>
                </div>

                @error('funtion_image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" >
                <label class="label-rn">Añade una imágen para la sección ”Beneficios”</label>
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore style="max-width: 450px;">
                    <input type="file" class="dropify context_image"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="benefit_image"   data-height="220"/>
                </div>

                @error('benefit_image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>
    </div>


     {{-- NOTICIAS --}}
    <div class="row mt-3">
        <div class="col-md-12" >
            <div class="form-group">
                <p class="label-rn">Asociar noticias</p>

                <div class="row">
                    <template x-for="list in noticias">
                        <div class="col-lg-4 col-md-6">
                            <div class="card-label card-border-news card-news-h mb-3 d-flex text-blak-gray flex-column align-items-start h-auto position-relative listopportunities" >
                                <span class="title-label text-lowercase" x-text="list.title"></span>
                                <span x-text="list.summary"></span>
                                <button title="Agregar" x-on:click="noticias.splice( noticias.indexOf(list), 1 );" ><i class="fe-x text-primary"></i></button></span>
                            </div>
                                
                        </div>

                    </template>

                    <div class="col-lg-4 col-md-6 d-flex ">

                        <a href="javascript:void(0);" class="btn-text-alt " x-on:click="$wire.loadNoticias(noticias)" data-bs-toggle="modal" data-bs-target="#addNoticias">
                            <i class="fe-plus"></i>
                            Agregar noticia
                        </a>
                    </div>

                </div>
                
            </div>
        </div>
    </div>

    {{-- BTN --}}
    <div class="row mt-3 mt-sm-5">
        @if($errors->any())
            <p class="text-danger text-center">Se presentó un error en uno de los campos requeridos.</p>
        @endif
        <div class="col-md-12 text-center" wire:loading.remove wire:target="store">
            <div>
                <button class="btn btn-primary text-uppercase me-2" wire:click="store(13)" >Guardar y publicar</button>            
                <button class="btn btn-custom  text-uppercase" wire:click="store(16)" >Guardar en borrador</button>
            </div>
            <br>
            <a href="{{ route('admin.opportunity.landing') }}" class="btn-text mt-3">Cancelar</a>
        </div>
        <div class="col-md-12 text-center" wire:loading wire:target="store">
            <button type="button" disabled class="btn-login btn btn-primary width-lg px-4 mb-5" ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
        </div>
    </div>


    <x-admin.modal.list-entities :idmodal="'addEntities'"  >
        <x-slot name="title">
            Entidades o actores asociados
        </x-slot>
    </x-admin.modal.list-entities>

    <x-admin.modal.list-interes :idmodal="'addInteres'"  >
        <x-slot name="title">
            Áreas de interés
        </x-slot>
    </x-admin.modal.list-interes>

    <x-admin.modal.list-noticias :idmodal="'addNoticias'"  >
        <x-slot name="title">
            Asociar noticia
        </x-slot>
    </x-admin.modal.list-noticias>

    @push('scripts')
        <script>
            $(document).ready(function() {

                $('.cover_image').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN DE PORTADA</b></p></div>',
                    }
                });

                $('.context_image').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN</b></p></div>',
                    }
                });
            });
        </script>
    @endpush

</div>
