<div x-data="{lang:'es', entities:@entangle('entities_ids').defer, tag_group:@entangle('tag_group'), interes:@entangle('interes_ids').defer, opportunities:@entangle('opportunities_ids').defer}">

    <div class="row">
        <div class="col-md-6">
            <div class="tap-opportunities me-1" :class="{ 'active': lang == 'es' }" @click="lang='es'">
                Español
            </div>
            <div class="tap-opportunities" :class="{ 'active': lang == 'en' }" @click="lang='en'">
                Inglés
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex justify-content-end">
                <a href="{{ route('admin.opportunity.multioffer') }}" class="btn-text"><i class="fe-x"></i>
                    cancelar</a>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        {{-- TITLE --}}
        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_es"
                    placeholder="Escribe un título en español para esta oportunidad ">
                @error('title_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_en"
                    placeholder="Escribe un título en Inglés para esta oportunidad ">
                @error('title_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        {{-- IMAGEN --}}
        <div class="col-md-6">
            <div class="form-group" >
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify cover_image"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="image" name="image" data-height="220"/>
                </div>

                @error('image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>

        {{-- Entidades o actores --}}
        <div class="col-md-6">
            <div class="row mt-2">
                <div class="col-md-6" x-show="$wire.tag_group_id == null">                      
                    <div class="form-group  ">
                        <label class="label-rn mb-3">Entidades o actores asociados: </label>
                        <select class="select-rutan" wire:model='tag_group_id'>
                            <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                            <template x-for="tag in tag_group">
                                <option :value="tag.id" x-text="tag.name"></option>
                            </template>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-12" x-show="$wire.tag_group_id != null" x-transition:enter>
                    <div class="form-group">
                        <p class="label-rn">Entidades o actores asociados <a href="javascript:void(0);" wire:click="resetGroup('tag_group_id', 'entities_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                        <div class="d-flex flex-wrap">
                                <template x-for="entitie in entities">
                                    <span class="badge-opportinity badge-soft-secondary rounded-pill"><span x-text="entitie.name"></span> <button x-on:click="entities.splice( entities.indexOf(entitie), 1 ); " ><i class="fe-x"></i></button></span>
                                </template>
                            <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadEntities(entities)" data-bs-toggle="modal" data-bs-target="#addEntities">
                                <i class="fe-plus"></i>
                                Agregar entidad
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        {{-- Categorías --}}
        <div class="col-md-12" x-show="$wire.categories_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Categorías de esta oportunidad: </label>
                <select class="select-rutan " wire:model='categories_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-12"  x-show="$wire.categories_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Categorías de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('categories_tag_group_id', 'categories_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($categories as $category)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkcategories'{{ $category['id'] }}"  value="{{ $category['id'] }}" wire:model.defer="categories_ids">
                            <label class="form-check-label label-rn" for="'checkcategories'{{ $category['id'] }}" >{{ $category['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        {{-- dirigida a --}}
        <div class="col-12"  x-show="$wire.dirigida_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Esta oportunidad va dirigida a <a href="javascript:void(0);" wire:click="resetGroup('dirigida_tag_group_id', 'dirigida_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($dirigida as $dg)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkdirigida'{{ $dg['id'] }}"  value="{{ $dg['id'] }}" wire:model.defer="dirigida_ids">
                            <label class="form-check-label label-rn" for="'checkdirigida'{{ $dg['id'] }}" >{{ $dg['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-12" x-show="$wire.dirigida_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Esta oportunidad va dirigida a: </label>
                <select class="select-rutan " wire:model='dirigida_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        {{-- Áreas de interés --}}
        <div class="col-md-6" x-show="$wire.interes_tag_group_id == null">                      
            <div class="form-group  w-mx-270 ">
                <label class="label-rn mb-3">Áreas de interés de esta oportunidad: </label>
                <select class="select-rutan" wire:model='interes_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12" x-show="$wire.interes_tag_group_id != null" x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Áreas de interés de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('interes_tag_group_id', 'interes_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                <div class="d-flex flex-wrap">
                    
                    <template x-for="inte in interes">
                        <span class="badge-opportinity badge-soft-secondary rounded-pill " ><span x-text="inte.name"></span> <button x-on:click="interes.splice( interes.indexOf(inte), 1 ); " ><i class="fe-x"></i></button></span>
                    </template>
                    

                    <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadInteres(interes)" data-bs-toggle="modal" data-bs-target="#addInteres">
                        <i class="fe-plus"></i>
                        Agregar categorías
                    </a>
                </div>
                
            </div>
        </div>
    </div>
    

    {{-- Presentación --}}
    <div class="row mt-2">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Presentación:</label>
                <textarea  cols="30" rows="10" class="form-control  form-control-h-270" wire:model.defer="presentation_es"
                    placeholder="Escribe una descripción de la oportunidad en español"></textarea>
                @error('presentation_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Presentación:</label>
                <textarea  cols="30" rows="10" class="form-control  form-control-h-270" wire:model.defer="presentation_en"
                    placeholder="Escribe una descripción de la oportunidad en inglés"></textarea>
                @error('presentation_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>


    {{-- Video o imágen de contexto --}}
    <div class="row mt-2 " x-data="{option:@entangle('option_context').defer}">
        <div class="col-12 mb-2 pb-1">
            <p class="label-rn mb-0">Video o imágen de contexto</p>
            <span class="">Agrega un video o una imágen que contextualice la oportunidad</span>
        </div>
        <div class="col-md-6 mb-4 pb-2">
            <div class="tap-opportunities me-1" :class="{ 'active': option == 'video' }" @click="option='video'">
                Video
            </div>
            <div class="tap-opportunities" :class="{ 'active': option == 'image' }" @click="option='image'">
                Imágen
            </div>
        </div>

        <div class="col-12" x-show="option == 'video'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">URL del video</label>
                 <input type="text" class="form-control " wire:model.defer="url_video"
                    placeholder="Agrega una url de youtube o vimeo">
                @error('url_video')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="option == 'image'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-0">Adjuntar imágen</label>
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify context_image"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="context_image" name="context_image" data-height="220"/>
                </div>
                @error('context_image')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>

    {{-- GRUPO DE OFERTAS --}}
    <div class="row">
        <div class="col-12" >
            <div class="form-group"  x-show="lang == 'es'" x-transition:enter>
                <p class="label-rn">Título del grupo de ofertas</p>
                <input type="text" class="form-control " wire:model.defer="title_group_offer_es"
                    placeholder="Agrega un título en español para el grupo de ofertas que se asociaran">
                @error('title_group_offer_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group"  x-show="lang == 'en'" x-transition:enter>
                <p class="label-rn">Título del grupo de ofertas</p>
                <input type="text" class="form-control " wire:model.defer="title_group_offer_en"
                    placeholder="Agrega un título en inglés para el grupo de ofertas que se asociaran">
                @error('title_group_offer_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    {{-- Descripción del grupo de ofertas --}}
    <div class="row mt-2">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Descripción del grupo de ofertas</label>
                <textarea  cols="30" rows="10" class="form-control " wire:model.defer="description_group_offer_es"
                    placeholder="(Opcional) Agrega una descripción en español para que los usuarios conozcan un poco más de este grupo de ofertas"></textarea>
                @error('description_group_offer_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Descripción del grupo de ofertas</label>
                <textarea  cols="30" rows="10" class="form-control " wire:model.defer="description_group_offer_en"
                    placeholder="(Opcional) Agrega una descripción en inglés para que los usuarios conozcan un poco más de este grupo de ofertas"></textarea>
                @error('description_group_offer_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>




    {{-- Asociar ofertas --}}
    <div class="row mt-3">
        <div class="col-md-12" >
            <div class="form-group">
                <p class="label-rn">Asociar ofertas</p>

                <div class="row">

                    <template x-for="list in opportunities">
                        <div class="col-lg-4 col-md-6">
                            <div class="card-label card-activitic-news mb-3 d-flex text-blak-gray flex-column align-items-start h-auto position-relative listopportunities" :class="{'op-standar':list.type_id == 1, 'op-landing':list.type_id == 4}">
                                <span class="title-label text-lowercase" x-text="list.title"></span>
                                <span class="activity-data-date"><b>Estado: </b> <span x-text="(list.open_status_id == 3)?'Abierta':'Cerrada'"></span></span>
                                <span >Oportunidad <span x-text="(list.type_id == 1)?'Estándar':'landing'"></span></span>
                                <button title="Agregar" x-on:click="opportunities.splice( opportunities.indexOf(list), 1 );" ><i class="fe-x text-primary"></i></button></span>
                            </div>
                                
                        </div>

                    </template>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadOpportunities(opportunities)" data-bs-toggle="modal" data-bs-target="#addLanding">
                            <i class="fe-plus"></i>
                            Agregar oferta
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    {{-- BTN --}}
    <div class="row mt-3 mt-sm-5">
        @if($errors->any())
            <p class="text-danger text-center">Se presentó un error en uno de los campos requeridos.</p>
        @endif
        <div class="col-md-12 text-center" wire:loading.remove wire:target="store">
            <div>
                <button class="btn btn-primary text-uppercase me-2" wire:click="store(13)" >Guardar y publicar</button>            
                <button class="btn btn-custom  text-uppercase" wire:click="store(16)" >Guardar en borrador</button>
            </div>
            <br>
            <a href="{{ route('admin.opportunity.multioffer') }}" class="btn-text mt-3">Cancelar</a>
        </div>
        <div class="col-md-12 text-center" wire:loading wire:target="store">
            <button type="button" disabled class="btn-login btn btn-primary width-lg px-4 mb-5" ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
        </div>
    </div>


    <x-admin.modal.list-entities :idmodal="'addEntities'"  >
        <x-slot name="title">
            Entidades o actores asociados
        </x-slot>
    </x-admin.modal.list-entities>

    <x-admin.modal.list-interes :idmodal="'addInteres'"  >
        <x-slot name="title">
            Áreas de interés
        </x-slot>
    </x-admin.modal.list-interes>

    <x-admin.modal.list-oppotunities-landing :idmodal="'addLanding'"  >
        <x-slot name="title">
            Asociar ofertas
        </x-slot>
    </x-admin.modal.list-oppotunities-landing>

    @push('scripts')
        <script>
            $(document).ready(function() {

                $('.cover_image').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN DE PORTADA</b></p></div>',
                    }
                });

                $('.context_image').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN</b></p></div>',
                    }
                });
            });
        </script>
    @endpush

</div>
