<div x-data="{lang:'es', entities:@entangle('entities_ids').defer, tag_group:@entangle('tag_group'), interes:@entangle('interes_ids').defer, aliados:@entangle('aliados_ids').defer, noticias:@entangle('noticias_ids').defer}">
    

    <div class="row">
        <div class="col-md-6">
            <div class="tap-opportunities me-1" :class="{ 'active': lang == 'es' }" @click="lang='es'">
                Español
            </div>
            <div class="tap-opportunities" :class="{ 'active': lang == 'en' }" @click="lang='en'">
                Inglés
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex flex-wrap justify-content-between">

                <div class="d-flex justify-content-end">
                    <label  class="label-rn m-0 d-inline-flex"><b>{{ ($open)?'Cerrar':'Abrir' }}</b></label>
                    <div class="switchery-rn"  wire:ignore>
                        <label for="status_op" class="label-rn mb-0 me-2 d-inline-flex"><b> oportunidad</b></label>
                        <input type="checkbox" wire:model="open" data-plugin="switchery"   {{ ($open)?'checked':'' }} id="status_op"/>
                    </div>
                    <span class="ms-1">{{ ($open)?'Abierta':'Cerrada' }}</span>
                </div>

                <div class="d-flex justify-content-end">                    
                    <div class="switchery-rn"  wire:ignore>
                        <label for="status_id" class="label-rn mb-0 me-2 d-inline-flex"><b>Estado </b></label>
                        <input type="checkbox" wire:model="status" data-plugin="switchery"   {{ ($status)?'checked':'' }} id="status_id"/>
                    </div>
                     <span class="ms-1">{{ ($status)?'Publicado':'Borrador' }}</span>
                </div>
                
            </div>
        </div>
    </div>

    <div class="row mt-4">
        {{-- TITLE --}}
        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_es"
                    placeholder="Escribe un título en español para esta oportunidad ">
                @error('title_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_en"
                    placeholder="Escribe un título en Inglés para esta oportunidad ">
                @error('title_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>


        {{-- IMAGEN --}}
        <div class="col-md-6">
            <div class="form-group" >
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"  data-default-file="{{ $opportunity['image'] }}"  wire:model.defer="image" name="image" data-height="220"/>
                </div>

                @error('image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>
        {{-- FECHAS --}}
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <p class="label-rn">Fecha de inicio</p>
                        <input type="date" wire:model.defer='start_date' class="form-control input-profile " placeholder="DD/MM/AAAA" >
                        @error('start_date')
                            <span class="text-danger d-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <p class="label-rn">Fecha de vencimiento</p>
                        <input type="date" wire:model.defer='end_date' class="form-control input-profile " placeholder="DD/MM/AAAA" >
                        @error('end_date')
                            <span class="text-danger d-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6" x-show="$wire.tag_group_id == null">                      
                    <div class="form-group  ">
                        <label class="label-rn mb-3">Entidades o actores asociados: </label>
                        <select class="select-rutan" wire:model='tag_group_id'>
                            <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                            <template x-for="tag in tag_group">
                                <option :value="tag.id" x-text="tag.name"></option>
                            </template>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-12" x-show="$wire.tag_group_id != null" x-transition:enter>
                    <div class="form-group">
                        <p class="label-rn">Entidades o actores asociados <a href="javascript:void(0);" wire:click="resetGroup('tag_group_id', 'entities_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                        <div class="d-flex flex-wrap">
                            
                                <template x-for="entitie in entities">
                                    <span class="badge-opportinity badge-soft-secondary rounded-pill"><span x-text="entitie.name"></span> <button x-on:click="entities.splice( entities.indexOf(entitie), 1 ); " ><i class="fe-x"></i></button></span>
                                </template>
                            

                            <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadEntities(entities)" data-bs-toggle="modal" data-bs-target="#addEntities">
                                <i class="fe-plus"></i>
                                Agregar entidad
                            </a>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>

        {{-- URL --}}
        <div class="col-12" >
            <div class="form-group">
                <p class="label-rn">URL del formulario de hubspot</p>
                <input type="text" class="form-control " wire:model.defer="url_forms"
                    placeholder="Pega acá la url del formulario creado en hubspot">
                @error('url_forms')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        {{-- Categorías --}}
        <div class="col-md-12" x-show="$wire.categories_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Categorías de esta oportunidad: </label>
                <select class="select-rutan " wire:model='categories_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-12"  x-show="$wire.categories_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Categorías de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('categories_tag_group_id', 'categories_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap" x-data="{categoriesids:@entangle('categories_ids').defer}">
                    @foreach ($categories as $category)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkcategories'{{ $category['id'] }}"  value="{{ $category['id'] }}" x-model="categoriesids">
                            <label class="form-check-label label-rn" for="'checkcategories'{{ $category['id'] }}" >{{ $category['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        {{-- dirigida a --}}
        <div class="col-12"  x-show="$wire.dirigida_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Esta oportunidad va dirigida a <a href="javascript:void(0);" wire:click="resetGroup('dirigida_tag_group_id', 'dirigida_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap" x-data="{dirigidaids:@entangle('dirigida_ids').defer}">
                    @foreach ($dirigida as $dg)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkdirigida'{{ $dg['id'] }}"  value="{{ $dg['id'] }}" x-model="dirigidaids">
                            <label class="form-check-label label-rn" for="'checkdirigida'{{ $dg['id'] }}" >{{ $dg['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-12" x-show="$wire.dirigida_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Esta oportunidad va dirigida a: </label>
                <select class="select-rutan " wire:model='dirigida_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        {{-- Áreas de interés --}}
        <div class="col-md-6" x-show="$wire.interes_tag_group_id == null">                      
            <div class="form-group  w-mx-270 ">
                <label class="label-rn mb-3">Áreas de interés de esta oportunidad: </label>
                <select class="select-rutan" wire:model='interes_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12" x-show="$wire.interes_tag_group_id != null" x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Áreas de interés de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('interes_tag_group_id', 'interes_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                <div class="d-flex flex-wrap">
                    
                    <template x-for="inte in interes">
                        <span class="badge-opportinity badge-soft-secondary rounded-pill " ><span x-text="inte.name"></span> <button x-on:click="interes.splice( interes.indexOf(inte), 1 ); " ><i class="fe-x"></i></button></span>
                    </template>
                    

                    <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadInteres(interes)" data-bs-toggle="modal" data-bs-target="#addInteres">
                        <i class="fe-plus"></i>
                        Agregar categorías
                    </a>
                </div>
                
            </div>
        </div>

    </div>

    {{-- Presentación --}}
    <div class="row mt-4">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Presentación:</label>
                <textarea  cols="30" rows="10" class="form-control form-control-h-270" wire:model.defer="presentation_es"
                    placeholder="Escribe una descripción de la oportunidad en español"></textarea>
                @error('presentation_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Presentación:</label>
                <textarea  cols="30" rows="10" class="form-control form-control-h-270" wire:model.defer="presentation_en"
                    placeholder="Escribe una descripción de la oportunidad en inglés"></textarea>
                @error('presentation_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>

    {{-- beneficios --}}
    <div class="row" x-show="lang == 'es'" x-transition:enter x-data="{benefites:@entangle('benefit_es').defer}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Beneficios:</label>
        </div>
        <template x-for="(benefit, index) in benefites" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                <div class="form-group position-relative" x-transition>
                    <i class="fe-check check-benefit"></i>
                    <textarea   class="form-control form-control-benefit" :value="benefit" x-model.lazy="benefites[index]" placeholder="Escribe un beneficio en español"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="benefites.splice( benefites.indexOf(benefit), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group position-relative">
                <i class="fe-check check-benefit"></i>
                <textarea   class="form-control form-control-benefit" x-ref="newbenefites" placeholder="Escribe un beneficio en español"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="benefites.push($refs.newbenefites.value); $refs.newbenefites.value = ''">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>
        
    </div>

    <div class="row" x-show="lang == 'en'" x-transition:enter x-data="{benefites:@entangle('benefit_en').defer}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Beneficios:</label>
        </div>
        <template x-for="(benefit, index) in benefites" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                <div class="form-group position-relative" x-transition>
                    <i class="fe-check check-benefit"></i>
                    <textarea   class="form-control form-control-benefit" :value="benefit" x-model.lazy="benefites[index]" placeholder="Escribe un beneficio en inglés"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="benefites.splice( benefites.indexOf(benefit), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group position-relative">
                <i class="fe-check check-benefit"></i>
                <textarea   class="form-control form-control-benefit" x-ref="newbenefites" placeholder="Escribe un beneficio en inglés"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="benefites.push($refs.newbenefites.value); $refs.newbenefites.value = ''">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>
    </div>

    {{-- cronograma actividades --}}
    <div class="row" x-show="lang == 'es'" x-transition:enter x-data="{activities:@entangle('activity_es')}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Cronograma de actividades:</label>
        </div>
        <template x-for="(activity, index) in activities" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                {{-- <p x-text="index + 1"></p> --}}
                <div class="form-group position-relative" x-transition>
                    <input type="text" class="form-control form-control-alt p-0 mb-3":value="activity.title" x-model.lazy="activities[index].title" placeholder="Título en español">
                    <textarea   class="form-control form-control-benefit  p-1" :value="activity.description" x-model.lazy="activities[index].description" placeholder="Descripción en español"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="activities.splice( activities.indexOf(activity), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group ">
                <input type="text" class="form-control form-control-alt p-0 mb-3" x-ref="newtitle" placeholder="Título en español">
                <textarea   class="form-control form-control-benefit p-1" x-ref="newdescrip"  placeholder="Descripción en español"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="activities.push({title:$refs.newtitle.value, description:$refs.newdescrip.value}); $refs.newtitle.value = ''; $refs.newdescrip.value='' ">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>

    </div>

    <div class="row" x-show="lang == 'en'" x-transition:enter x-data="{activities:@entangle('activity_en')}" >
        <div class="col-md-12">
            <label class="label-rn mb-2">Cronograma de actividades:</label>
        </div>
        <template x-for="(activity, index) in activities" >
            <div  class="col-md-6 col-lg-4"  x-transition>
                {{-- <p x-text="index + 1"></p> --}}
                <div class="form-group position-relative" x-transition>
                    <input type="text" class="form-control form-control-alt p-0 mb-3":value="activity.title" x-model.lazy="activities[index].title" placeholder="Título en inglés">
                    <textarea   class="form-control form-control-benefit  p-1" :value="activity.description" x-model.lazy="activities[index].description" placeholder="Descripción en inglés"></textarea>
                    <i class="fe-x closed-benefit" x-on:click="activities.splice( activities.indexOf(activity), 1 );" ></i>
                </div>
            </div>
        </template>
        <div  class="col-md-6 col-lg-4 d-none" >
            <div class="form-group ">
                <input type="text" class="form-control form-control-alt p-0 mb-3" x-ref="newtitle" placeholder="Título en inglés">
                <textarea   class="form-control form-control-benefit p-1" x-ref="newdescrip"  placeholder="Descripción en inglés"></textarea>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 d-flex align-items-center ">
            <div class="form-group">
                <a href="javascript:void(0);" class="btn-text-alt" x-on:click="activities.push({title:$refs.newtitle.value, description:$refs.newdescrip.value}); $refs.newtitle.value = ''; $refs.newdescrip.value='' ">
                    <i class="fe-plus"></i>
                    Agregar item
                </a>
            </div>
        </div>
    </div>


     {{-- NOTICIAS --}}
    <div class="row">
        <div class="col-md-12" >
            <div class="form-group">
                <p class="label-rn">Asociar casos de éxito</p>

                <div class="row">
                    <template x-for="list in noticias">
                        <div class="col-lg-4 col-md-6">
                            <div class="card-label card-border-news card-news-h mb-3 d-flex text-blak-gray flex-column align-items-start h-auto position-relative listopportunities" >
                                <span class="title-label text-lowercase" x-text="list.title"></span>
                                <span x-text="list.summary"></span>
                                <button title="Agregar" x-on:click="noticias.splice( noticias.indexOf(list), 1 );" ><i class="fe-x text-primary"></i></button></span>
                            </div>
                                
                        </div>

                    </template>

                    <div class="col-lg-4 col-md-6 d-flex ">

                        <a href="javascript:void(0);" class="btn-text-alt " x-on:click="$wire.loadNoticias(noticias)" data-bs-toggle="modal" data-bs-target="#addNoticias">
                            <i class="fe-plus"></i>
                            Agregar noticia
                        </a>
                    </div>

                </div>
                
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6" x-show="$wire.aliados_tag_group_id == null">                      
            <div class="form-group  w-mx-270 ">
                <label class="label-rn mb-3">Asociar aliados: </label>
                <select class="select-rutan" wire:model='aliados_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12" x-show="$wire.aliados_tag_group_id != null" x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Asociar aliados  <a href="javascript:void(0);" wire:click="resetGroup('aliados_tag_group_id', 'aliados_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                <div class="d-flex flex-wrap">
                    
                    <template x-for="alia in aliados">
                        <span class="badge-opportinity badge-soft-secondary rounded-pill " ><span x-text="alia.name"></span> <button x-on:click="aliados.splice( aliados.indexOf(alia), 1 ); " ><i class="fe-x"></i></button></span>
                    </template>
                    

                    <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadAliados(aliados)" data-bs-toggle="modal" data-bs-target="#addAliados">
                        <i class="fe-plus"></i>
                        Agregar aliado
                    </a>
                </div>
                
            </div>
        </div>
    </div>






    <div class="row mt-3 mt-sm-5">
        @if($errors->any())
            <p class="text-danger text-center">Se presentó un error en uno de los campos requeridos.</p>
        @endif
        <div class="col-md-12 text-center" wire:loading.remove wire:target="update">            
            <button class="btn btn-primary text-uppercase " wire:click="update" >Guardar cambios</button>
            <br>
            <a href="{{ route('admin.opportunity.standard') }}" class="btn-text mt-3">Cancelar</a>
        </div>
        <div class="col-md-12 text-center" wire:loading wire:target="update">
            <button type="button" disabled class="btn-login btn btn-primary width-lg px-4 mb-5" ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
        </div>
    </div>


    <x-admin.modal.list-entities :idmodal="'addEntities'"  >
        <x-slot name="title">
            Entidades o actores asociados
        </x-slot>
    </x-admin.modal.list-entities>

    <x-admin.modal.list-interes :idmodal="'addInteres'"  >
        <x-slot name="title">
            Áreas de interés
        </x-slot>
    </x-admin.modal.list-interes>

    <x-admin.modal.list-aliados :idmodal="'addAliados'"  >
        <x-slot name="title">
            Aliados
        </x-slot>
    </x-admin.modal.list-aliados>

    <x-admin.modal.list-noticias :idmodal="'addNoticias'"  >
        <x-slot name="title">
            Asociar casos de éxito
        </x-slot>
    </x-admin.modal.list-noticias>

    @push('scripts')
        <script>
            $(document).ready(function() {

                $('.dropify').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN DE PORTADA</b></p></div>',
                    }
                });

                $('[data-plugin="switchery"]').each(function(e,n){
                    new Switchery($(this)[0],  {
                        color: '#fbf1ca', 
                        jackColor: '#f1cd3d',
                        size: 'small',
                        secondaryColor: '#ebebeb',
                        jackSecondaryColor: '#b3b3b3'
                    })
                });
            });
        </script>
    @endpush
</div>
