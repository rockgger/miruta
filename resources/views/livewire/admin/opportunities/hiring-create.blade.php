<div x-data="{lang:'es', entities:@entangle('entities_ids').defer, tag_group:@entangle('tag_group'), interes:@entangle('interes_ids').defer}">

    <div class="row">
        <div class="col-md-6">
            <div class="tap-opportunities me-1" :class="{ 'active': lang == 'es' }" @click="lang='es'">
                Español
            </div>
            <div class="tap-opportunities" :class="{ 'active': lang == 'en' }" @click="lang='en'">
                Inglés
            </div>
        </div>
    </div>
    
    <div class="row mt-4">
        {{-- TITLE --}}
        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_es"
                    placeholder="Escribe un título en español para esta oportunidad ">
                @error('title_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_en"
                    placeholder="Escribe un título en Inglés para esta oportunidad ">
                @error('title_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
        {{-- IMAGEN --}}
        <div class="col-md-6">
            <div class="form-group" >
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="image" name="image" data-height="220"/>
                </div>

                @error('image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <p class="label-rn">Fecha de inicio</p>
                        <input type="date" wire:model.defer='start_date' class="form-control input-profile " placeholder="DD/MM/AAAA" >
                        @error('start_date')
                            <span class="text-danger d-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <p class="label-rn">Fecha de vencimiento</p>
                        <input type="date" wire:model.defer='end_date' class="form-control input-profile " placeholder="DD/MM/AAAA" >
                        @error('end_date')
                            <span class="text-danger d-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6" x-show="$wire.tag_group_id == null">                      
                    <div class="form-group  ">
                        <label class="label-rn mb-3">Entidades o actores asociados: </label>
                        <select class="select-rutan" wire:model='tag_group_id'>
                            <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                            <template x-for="tag in tag_group">
                                <option :value="tag.id" x-text="tag.name"></option>
                            </template>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-12" x-show="$wire.tag_group_id != null" x-transition>
                    <div class="form-group">
                        <p class="label-rn">Entidades o actores asociados <a href="javascript:void(0);" wire:click="resetGroup('tag_group_id', 'entities_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                        <div class="d-flex flex-wrap">
                            
                                <template x-for="entitie in entities">
                                    <span class="badge-opportinity badge-soft-secondary rounded-pill"><span x-text="entitie.name"></span> <button x-on:click="entities.splice( entities.indexOf(entitie), 1 ); " ><i class="fe-x"></i></button></span>
                                </template>
                            

                            <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadEntities(entities)" data-bs-toggle="modal" data-bs-target="#addEntities">
                                <i class="fe-plus"></i>
                                Agregar entidad
                            </a>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>

        {{-- URL --}}
        <div class="col-12" >
            <div class="form-group">
                <p class="label-rn">URL del formulario de hubspot</p>
                <input type="text" class="form-control " wire:model.defer="url_forms"
                    placeholder="Pega acá la url del formulario creado en hubspot">
                @error('url_forms')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        {{-- Categorías --}}
        <div class="col-md-12" x-show="$wire.categories_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Categorías de esta oportunidad: </label>
                <select class="select-rutan " wire:model='categories_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-12"  x-show="$wire.categories_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Categorías de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('categories_tag_group_id', 'categories_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($categories as $category)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkcategories'{{ $category['id'] }}"  value="{{ $category['id'] }}" wire:model.defer="categories_ids">
                            <label class="form-check-label label-rn" for="'checkcategories'{{ $category['id'] }}" >{{ $category['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        {{-- dirigida a --}}
        <div class="col-12"  x-show="$wire.dirigida_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Esta oportunidad va dirigida a <a href="javascript:void(0);" wire:click="resetGroup('dirigida_tag_group_id', 'dirigida_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($dirigida as $dg)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkdirigida'{{ $dg['id'] }}"  value="{{ $dg['id'] }}" wire:model.defer="dirigida_ids">
                            <label class="form-check-label label-rn" for="'checkdirigida'{{ $dg['id'] }}" >{{ $dg['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-12" x-show="$wire.dirigida_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Esta oportunidad va dirigida a: </label>
                <select class="select-rutan " wire:model='dirigida_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        
        {{-- Áreas de interés --}}
        <div class="col-md-6" x-show="$wire.interes_tag_group_id == null">                      
            <div class="form-group  w-mx-270 ">
                <label class="label-rn mb-3">Áreas de interés de esta oportunidad: </label>
                <select class="select-rutan" wire:model='interes_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12" x-show="$wire.interes_tag_group_id != null" x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Áreas de interés de esta oportunidad <a href="javascript:void(0);" wire:click="resetGroup('interes_tag_group_id', 'interes_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">                    
                    <template x-for="inte in interes">
                        <span class="badge-opportinity badge-soft-secondary rounded-pill " ><span x-text="inte.name"></span> <button x-on:click="interes.splice( interes.indexOf(inte), 1 ); " ><i class="fe-x"></i></button></span>
                    </template>
                    <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadInteres(interes)" data-bs-toggle="modal" data-bs-target="#addInteres">
                        <i class="fe-plus"></i>
                        Agregar categorías
                    </a>
                </div>
                
            </div>
        </div>
    </div>


    {{-- NOTIFICACIONES EMAIL--}}
    <div class="col-12" >
        <div class="form-group">
            <p class="label-rn">E-mails a notificar</p>
            <div  wire:ignore x-data="{ notify:@entangle('emails_notofy').defer}" 
            x-init='$nextTick(()=>{$(".selectize-close-btn").selectize({
                    plugins:["remove_button"],
                    persist:!1,
                    create:!0,
                    onItemRemove:function(e){
                        console.log(e);
                        notify.splice( notify.indexOf(e), 1 );
                    },
                    onItemAdd:function(value, $item){
                        notify.push(value);
                    }
                });})'>
                <input type="text" class="form-control selectize-close-btn "                 
                placeholder="Escribe la dirección de E-mail a las que llegarán las hojas de vida de los ofertantes">
            </div>
            @error('emails_notofy')
                <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
            @enderror
        </div>
    </div>

    
    {{-- Tipo de contrato --}}
    <div class="row">
        
        <div class="col-md-8"  x-show="$wire.type_contract_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Tipo de contrato <a href="javascript:void(0);" wire:click="resetGroup('type_contract_tag_group_id', 'type_contract_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($type_contract as $dg)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="radio" name="checktypecontract" class="form-check-input" id="'checktypecontract'{{ $dg['id'] }}"  value="{{ $dg['id'] }}" wire:model.defer="type_contract_ids">
                            <label class="form-check-label label-rn" for="'checktypecontract'{{ $dg['id'] }}" >{{ $dg['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-8" x-show="$wire.type_contract_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Tipo de contrato: </label>
                <select class="select-rutan " wire:model='type_contract_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>


        <div class="col-md-4" >
            <div class="form-group">
                <p class="label-rn">Salario</p>
                <input type="text" class="form-control " wire:model.defer="salary"
                    placeholder="Salario base de la oferta" data-toggle="input-mask" data-mask-format="000000000000000000000">
                @error('salary')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    {{-- Objeto del contrato --}}
    <div class="row mt-4">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Objeto del contrato:</label>
                <textarea  cols="30" rows="10" class="form-control form-control-h-270" wire:model.defer="object_contract_es"
                    placeholder="Escribe una descripción de la oportunidad en español"></textarea>
                @error('object_contract_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <label class="label-rn mb-2">Objeto del contrato:</label>
                <textarea  cols="30" rows="10" class="form-control form-control-h-270" wire:model.defer="object_contract_en"
                    placeholder="Escribe una descripción de la oportunidad en inglés"></textarea>
                @error('object_contract_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>

    {{-- ADJUNTOS --}}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <p class="label-rn">Adjuntar documentos </p>
                <div class="" wire:loading.remove wire:target='attach, attachments, removeAttach'>                    
                    <input type="file" {{-- accept="image/*" --}} multiple class="invisible position-absolute" x-ref="attach" wire:model="attach" name="attach" id="attach">
                    
                    <a href="javascript:void(0);" class="btn-text-alt mb-3" x-on:click="$refs.attach.click();">
                        <i class="fe-upload"></i>
                        Subir documentos
                    </a>
                    <div class="d-flex flex-wrap">
                        @foreach ( $attachments as $a => $att )
                            <div class="position-relative">                                                       
                                <a href="{{ $att['url'] }}" target="_blank" class="position-relative a-file-attach">                                    
                                    <div class="file-attach">
                                        <i class="fe-file-text"></i>
                                    </div>
                                    <small>{{ $att['name'] }}</small>
                                </a>
                                <i class="fe-x closed-benefit" wire:click="removeAttach({{ $a }})" ></i>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div wire:loading wire:target='attach, attachments, removeAttach'>
                    <div class="d-block w-100 p-3">
                        <i class="mdi mdi-spin mdi-loading text-primary font-28"></i>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    
    



    <div class="row mt-3 mt-sm-5">
        @if($errors->any())
            <p class="text-danger text-center">Se presentó un error en uno de los campos requeridos.</p>
        @endif
        <div class="col-md-12 text-center" wire:loading.remove wire:target="store">
            <div>
                <button class="btn btn-primary text-uppercase me-2" wire:click="store(13)" >Guardar y publicar</button>            
                <button class="btn btn-custom  text-uppercase" wire:click="store(16)" >Guardar en borrador</button>
            </div>
            <br>
            <a href="{{ route('admin.opportunity.hiring') }}" class="btn-text mt-3">Cancelar</a>
        </div>
        <div class="col-md-12 text-center" wire:loading wire:target="store">
            <button type="button" disabled class="btn-login btn btn-primary width-lg px-4 mb-5" ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
        </div>
    </div>

    <x-admin.modal.list-entities :idmodal="'addEntities'"  >
        <x-slot name="title">
            Entidades o actores asociados
        </x-slot>
    </x-admin.modal.list-entities>

    <x-admin.modal.list-interes :idmodal="'addInteres'"  >
        <x-slot name="title">
            Áreas de interés
        </x-slot>
    </x-admin.modal.list-interes>

    

    @push('scripts')
        <script>
            $(document).ready(function() {

                $('.dropify').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN DE PORTADA</b></p></div>',
                    }
                }); 
                {{-- $(".selectize-close-btn").selectize({
                    plugins:["remove_button"],
                    persist:!1,
                    create:!0,
                    render:{
                        item:function(e,a){
                            return'<div>"'+a(e.text)+'"</div>'
                        }
                    },
                    onDelete:function(e){
                        return confirm(1<e.length?"Are you sure you want to remove these "+e.length+" items?":'Are you sure you want to remove "'+e[0]+'"?')
                    },
                    onItemAdd:function(value, $item){
                        console.log("onItemAdd: "+value);
                    }
                }); --}}
            });

            
        </script>
    @endpush

</div>
