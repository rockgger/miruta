<div x-data="{}">
    

    <section class="mb-2">
        <h4 class="text-uppercase text-blak-gray mb-0">Banner</h4>
        <span>Más información</span> <a href="javascript:void(0);" class="btn-icon d-inline-flex ms-2" style="height: 1.5rem; width: 1.5rem;" ><i class="fas fa-question font-12"></i></a>
    </section>

    <section class="mb-4 py-3">
        <div class="row">
            <div class="col-md-6">
                <div class="d-flex align-items-end">
                    <figure class="m-0 resource-hero me-sm-3">
                        @if($desktop)
                            <img src="{{ $desktop->temporaryUrl() }}"  alt="Banner desktop">
                        @else
                            <img src="{{ ($url_desktop != null)?$url_desktop->url:'/image/img.jpg' }}" style="{{ ($url_desktop != null)?$url_desktop->url:'object-fit: cover;height: 138px;width: 233px;' }}" alt="Banner desktop">
                        @endif
                    </figure>
                    <div>
                        <i class="fe-monitor text-primary font-22"></i>
                        <p>800px <small ><b>x</b></small> 450px</p>
                        <a href="javascript:void(0);" wire:loading.remove wire:target='desktop'  class="btn btn-custom  text-uppercase" x-on:click="$refs.desktop.click();">Subir banner</a>
                        <a href="javascript:void(0);" wire:loading  wire:target='desktop' class="btn btn-custom  text-uppercase width-lg px-4 " disabled ><i class="mdi mdi-spin mdi-loading"></i></a>
                    </div>
                </div>
                <h5 class="text-uppercase mt-3 ps-5">Desktop</h5>

                @error('desktop')
                
                <div class="alert alert-danger msg fadeIn animated bg-danger text-white d-inline-block" role="alert" >
                    {{ $message }}
                </div>
                @enderror
                <input type="file" accept="image/*" wire:model="desktop" name="desktop" class="invisible position-absolute" x-ref="desktop" id="">

            </div>

            <div class="col-md-6">
                <div class="d-flex align-items-end">
                    <figure class="m-0 resource-hero me-sm-3 hero-home-width-mobile">
                        @if($mobile)
                            <img src="{{ $mobile->temporaryUrl() }}"  alt="Banner mobile">
                        @else
                            <img src="{{ ($url_mobile != null)?$url_mobile->url:'/image/img.jpg' }}" style="{{ ($url_mobile != null)?$url_mobile->url:'object-fit: cover;height: 138px;width: 71px;' }}" alt="Banner mobile">
                        @endif
                    </figure>
                    <div>
                        <i class="fe-monitor text-primary font-22"></i>
                        <p>350px <small ><b>x</b></small> 700px</p>
                        <a href="javascript:void(0);" wire:loading.remove wire:target='mobile'  class="btn btn-custom  text-uppercase" x-on:click="$refs.mobile.click();">Subir banner</a>
                        <a href="javascript:void(0);" wire:loading  wire:target='mobile' class="btn btn-custom  text-uppercase width-lg px-4 " disabled ><i class="mdi mdi-spin mdi-loading"></i></a>
                    </div>
                </div>
                <h5 class="text-uppercase mt-3 ps-1">mobile</h5>

                @error('mobile')
                <div class="alert alert-danger msg fadeIn animated bg-danger text-white d-inline-block" role="alert" >
                    {{ $message }}
                </div>
                @enderror
                <input type="file" accept="image/*" wire:model="mobile" name="mobile" class="invisible position-absolute" x-ref="mobile" id="">

            </div>
        </div>
    </section>

    <div class="row mt-3 ">
        <div class="col-md-12 text-center" wire:loading.remove wire:target="update">            
            <button class="btn btn-primary text-uppercase " wire:click="update" >Guardar cambios</button>
        </div>
        <div class="col-md-12 text-center" wire:loading wire:target="update">
            <button type="button" disabled class="btn-login btn btn-primary width-lg px-4 mb-5" ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
        </div>
    </div>
</div>
