<div x-data="{img:@entangle('resources')}">
    @error('image_hero')
    <div class="alert alert-danger msg fadeIn animated bg-danger text-white" role="alert">
        {{ $message }}
    </div>
    @enderror
    <input type="file" accept="image/*" wire:model="image_hero" name="image_hero" class="invisible position-absolute" x-ref="image" id="">
    <section class="mb-2 d-flex justify-content-between align-items-start">
        <div>
            <h4 class="text-uppercase text-blak-gray mb-0">Hero home</h4>
            <span>Más información</span> <a href="javascript:void(0);" class="btn-icon d-inline-flex ms-2" style="height: 1.5rem; width: 1.5rem;" ><i class="fas fa-question font-12"></i></a>
        </div>
        <div>
            <a href="javascript:void(0);" wire:loading.remove class="btn btn-custom  text-uppercase" x-on:click="$refs.image.click();">AGREGAR HERO</a>

            <a href="javascript:void(0);" wire:loading class="btn btn-custom  text-uppercase width-lg px-4 " disabled ><i
                    class="mdi mdi-spin mdi-loading"></i></a>
        </div>
    </section>

    <section class="mb-4 py-3">
        <template x-if="img.length <= 0">
            <p class="text-center">No hay recursos.</p>
        </template>

        <template x-if="img.length > 0">
            <div class="d-flex flex-wrap">
                <template x-for="ig in img">
                    <div class="p-2 me-2">
                        <figure class="m-0 resource-hero">
                            <div class="hero-deleted"   x-on:click="$dispatch('showSweetAlertQuestion', { 
                                    title: 'Eliminar recurso',
                                    text:'¿Quieres eliminar este recurso del hero home?',
                                    type:'warning',
                                    actionEmit:'deletedResource',
                                    val: ig.id,
                                    });"><i class="fe-trash-2" ></i></div>
                            <img :src="ig.url" alt="Hero home">
                        </figure>
                    </div>
                </template>
            </div>
        </template>

    </section>
</div>
