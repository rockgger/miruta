<div x-data="{lang:'es', tag_group:@entangle('tag_group'), interes:@entangle('interes_ids').defer, categories:@entangle('categories_ids').defer }">

    <div class="row">
        <div class="col-md-6">
            <div class="tap-opportunities me-1" :class="{ 'active': lang == 'es' }" @click="lang='es'">
                Español
            </div>
            <div class="tap-opportunities" :class="{ 'active': lang == 'en' }" @click="lang='en'">
                Inglés
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex justify-content-end">
                <a href="{{ route('admin.news') }}" class="btn-text"><i class="fe-x"></i>
                    cancelar</a>

                <button class="btn btn-custom  text-uppercase" wire:loading.remove wire:target="store" wire:click="store(16)" >Guardar borrador</button>

                <button type="button" wire:loading wire:target="store" disabled class="btn btn-custom  text-uppercase width-lg " ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
            </div>
        </div>
    </div>


    <div class="row mt-4">
        {{-- TITLE --}}
        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_es"
                    placeholder="Escribe un título en español para esta noticia ">
                @error('title_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <input type="text" class="form-control form-control-alt " wire:model.defer="title_en"
                    placeholder="Escribe un título en Inglés para esta noticia ">
                @error('title_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    {{-- RESUMEN DE LA NOTICIA --}}
    <div class="row mt-2">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group">
                
                <textarea  cols="30" rows="10" class="form-control " maxlength="240" wire:model.defer="summary_es"
                    placeholder="Escribe una introducción o resumen de la noticia en español"></textarea>
                @error('summary_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group">
                <textarea  cols="30" rows="10" class="form-control " maxlength="240" wire:model.defer="summary_en"
                    placeholder="Escribe una introducción o resumen de la noticia en inglés"></textarea>
                @error('summary_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>

    

    {{-- CUERPO DE LA NOTICIA --}}
    <div class="row mt-4">

        <div class="col-12" x-show="lang == 'es'" x-transition:enter>
            <div class="form-group"  x-data="{body:@entangle('body_es').defer}" x-init="$nextTick(()=>{
                    $('.summer-editor-es').summernote({
                        placeholder: 'Comienza a escribir el contenido en español',
                        minHeight: 450,
                        lang: 'es-ES',
                        toolbar: [
                            ['font', ['bold', 'underline', 'italic', 'clear']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture', 'video']],
                            ['view', ['fullscreen']]
                        ],
                        callbacks: {
                            onChange: function(contents, $editable) {
                                body = contents;
                            }
                        }
                    });
                    $('.summer-editor-es').summernote('removeFormat');
                })">
                <div wire:ignore>   
                   
                    <textarea  cols="30" rows="10" class="form-control summer-editor-es" 
                    placeholder="Comienza a escribir en español"></textarea>
                </div>
                @error('body_es')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12" x-show="lang == 'en'" x-transition:enter>
            <div class="form-group"  x-data="{body:@entangle('body_en').defer}" x-init="$nextTick(()=>{
                    $('.summer-editor-en').summernote({
                        placeholder: 'Comienza a escribir el contenido en inglés',
                        minHeight: 450,
                        lang: 'es-ES',
                        toolbar: [
                            ['font', ['bold', 'underline', 'italic', 'clear']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture', 'video']],
                            ['view', ['fullscreen']]
                        ],
                        callbacks: {
                            onChange: function(contents, $editable) {
                                body = contents;
                            }
                        }
                    });
                    $('.summer-editor-en').summernote('removeFormat');
                })">
                <div wire:ignore>   
                   
                    <textarea  cols="30" rows="10" class="form-control summer-editor-en" 
                    placeholder="Comienza a escribir en inglés"></textarea>
                </div>
                @error('body_en')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>

    </div>

    <div class="row mt-2">
        {{-- IMAGEN --}}
        <div class="col-md-6">
            <div class="form-group" >
                <small class="text-muted d-block">Optimiza el peso de la imagen antes de subirla. <a href="https://imagecompressor.com/es/" target="_blank"><small>Optimizar</small></a></small>
                <div wire:ignore>
                    <input type="file" class="dropify"   data-max-file-size="1M"  data-allowed-file-extensions="jpg jpeg png" data-errors-position="outside" accept="image/*"    wire:model.defer="image" name="image" data-height="220"/>
                </div>

                @error('image')
                <span class="d-block text-danger" style="width: 100%;">{{$message}}</span>
                @enderror
            </div>
        </div>

        {{-- CATEGORÍAS --}}
        <div class="col-md-6">
            <div class="row mt-2">
                <div class="col-md-6" x-show="$wire.categories_tag_group_id == null">                      
                    <div class="form-group  ">
                        <label class="label-rn mb-3">Categorías: </label>
                        <select class="select-rutan" wire:model='categories_tag_group_id'>
                            <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                            <template x-for="tag in tag_group">
                                <option :value="tag.id" x-text="tag.name"></option>
                            </template>
                        </select>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-12" x-show="$wire.categories_tag_group_id != null" x-transition:enter>
                    <div class="form-group">
                        <p class="label-rn">Categorías <a href="javascript:void(0);" wire:click="resetGroup('categories_tag_group_id', 'categories_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                        <div class="d-flex flex-wrap">
                                <template x-for="category in categories">
                                    <span class="badge-opportinity badge-soft-secondary rounded-pill"><span x-text="category.name"></span> <button x-on:click="categories.splice( categories.indexOf(category), 1 ); " ><i class="fe-x"></i></button></span>
                                </template>
                            <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadCategories(categories)" data-bs-toggle="modal" data-bs-target="#addCategories">
                                <i class="fe-plus"></i>
                                Agregar categoría
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

    </div>

     {{-- dirigida a / IDEAL PARA--}}
    <div class="row mt-4">
   
        <div class="col-12"  x-show="$wire.dirigida_tag_group_id != null"  x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Esta noticia es ideal para: <a href="javascript:void(0);" wire:click="resetGroup('dirigida_tag_group_id', 'dirigida_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>
                <div class="d-flex flex-wrap">
                    @foreach ($dirigida as $dg)
                        <div class="form-check me-4 mb-2 form-check-rutan">
                            <input type="checkbox" class="form-check-input" id="'checkdirigida'{{ $dg['id'] }}"  value="{{ $dg['id'] }}" wire:model.defer="dirigida_ids">
                            <label class="form-check-label label-rn" for="'checkdirigida'{{ $dg['id'] }}" >{{ $dg['name'] }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-md-12" x-show="$wire.dirigida_tag_group_id == null">                      
            <div class="form-group w-mx-270 ">
                <label class="label-rn mb-3">Esta noticia es ideal para:: </label>
                <select class="select-rutan " wire:model='dirigida_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    {{-- ÁREA DE INTERÉS --}}
    <div class="row mt-3">
        
        <div class="col-md-6" x-show="$wire.interes_tag_group_id == null">                      
            <div class="form-group  w-mx-270 ">
                <label class="label-rn mb-3">Áreas de interés de esta noticia: </label>
                <select class="select-rutan" wire:model='interes_tag_group_id'>
                    <option class="text-muted"  selected >Selecciona un grupo de etiqueta</option>
                    <template x-for="tag in tag_group">
                        <option :value="tag.id" x-text="tag.name"></option>
                    </template>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-12" x-show="$wire.interes_tag_group_id != null" x-transition:enter>
            <div class="form-group">
                <p class="label-rn">Áreas de interés de esta noticia <a href="javascript:void(0);" wire:click="resetGroup('interes_tag_group_id', 'interes_ids')" title="Selecciona un grupo de etiqueta" class="btn-text"><i class="fe-repeat"></i></a></p>

                <div class="d-flex flex-wrap">
                    
                    <template x-for="inte in interes">
                        <span class="badge-opportinity badge-soft-secondary rounded-pill " ><span x-text="inte.name"></span> <button x-on:click="interes.splice( interes.indexOf(inte), 1 ); " ><i class="fe-x"></i></button></span>
                    </template>
                    

                    <a href="javascript:void(0);" class="btn-text-alt" x-on:click="$wire.loadInteres(interes)" data-bs-toggle="modal" data-bs-target="#addInteres">
                        <i class="fe-plus"></i>
                        Agregar items
                    </a>
                </div>
                
            </div>
        </div>
    </div>

    {{-- BTN --}}
    <div class="row mt-3 mt-sm-5">
        @if($errors->any())
            <p class="text-danger text-center">Se presentó un error en uno de los campos requeridos.</p>
        @endif
        <div class="col-md-12 text-center" wire:loading.remove wire:target="store">
            <div>
                <button class="btn btn-primary text-uppercase me-2" wire:click="store(13)" >PUBLICAR NOTICIA</button>    
            </div>
            <br>
            <a href="{{ route('admin.news') }}" class="btn-text mt-3">Cancelar</a>
        </div>
        <div class="col-md-12 text-center" wire:loading wire:target="store">
            <button type="button" disabled class="btn-login btn btn-primary width-lg px-4 mb-5" ><i
                    class="mdi mdi-spin mdi-loading"></i></button>
        </div>
    </div>


    <x-admin.modal.list-categories :idmodal="'addCategories'"  >
        <x-slot name="title">
            Categorías
        </x-slot>
    </x-admin.modal.list-categories>

    <x-admin.modal.list-interes :idmodal="'addInteres'"  >
        <x-slot name="title">
            Áreas de interés
        </x-slot>
    </x-admin.modal.list-interes>
    

    @push('scripts')
        <script>
            $(document).ready(function() {

                $('.dropify').dropify({
                    tpl: {
                        message:'<div class="dropify-message"><span class="file-icon" /> <p class="text-brandon" ><b>SUBIR IMÁGEN DE PORTADA</b></p></div>',
                    }
                });

                
            });

        </script>
    @endpush

</div>
 