<div >
    <div class="row" x-data="{open:@entangle('open')}">
        <div class="col-md-6">
            <div class="tap-opportunities me-1" :class="{ 'active': open }" @click="open=true">
                Publicadas
            </div>
            <div class="tap-opportunities" :class="{ 'active': !open }" @click="open=false">
                Borrador
            </div>
        </div>
        <div class="col-md-6 d-flex justify-content-end">
            <a href="{{ route('admin.news.create') }}" class="btn btn-custom  text-uppercase">NUEVA NOTICIA</a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-4">
            <div class="form-group mb-0">
                <input type="search" wire:model.debounce.400ms="search" class="form-control input-profile " placeholder="Buscar" >
                @error('search')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="col-md-8 d-flex justify-content-end align-items-center">
            <label class="label-rn me-2">Orden: </label>        
            <div class="form-group mb-0">
                <select class="select-rutan" wire:model="order" >
                    <option class="text-muted" value="DESC"  selected >Más nuevo</option>
                    <option class="text-muted" value="ASC"  selected >Más antiguos</option>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
    
    </div>



    <section class="mt-4">
        <p class="text-muted">{{ $noticias->total() }} resultados</p>

        @if($noticias->total() > 0)
        <div class="row">       
            @foreach($noticias as $i=>$noticia)
            <div class="col-md-12"  wire:key="op-{{ $noticia->id }}">
                <div class="card-label text-truncate">
                    <div class="d-flex text-blak-gray align-items-center justify-content-between text-truncate">
                        <div class="d-flex align-items-center text-truncate">               
                            <div class="d-flex flex-column align-items-start text-truncate">
                                <span class="title-label ">{{ $noticia->title }} <small class="ms-1 text-muted">Creado {{ App\Helpers\Helper::diffForHumans($noticia->created_at) }}</small></span>
                                <span class="desct-label text-truncate d-block ">Autor: {{ $noticia->author->name }} {{ $noticia->author->last_name }}</span>
                            </div>
                        </div>
                        <div class="dropstart" x-data="{show:false}">
                            <div class="d-flex align-items-center">                            
                                <span class="status-{{ ($noticia->status_id == 13 )?'open':'close' }}"><i class="mdi mdi-checkbox-blank-circle"></i>{{ ($noticia->status_id == 13)?'Publicado':'Borrador' }}</span>
                                <a href="javascript:void(0);" x-on:click="show = ! show"  class="btn-more-vertial " >
                                    <i class="fe-more-vertical" x-show="!show" x-transition></i>
                                    <i class="fe-minus" x-show="show" x-transition></i>
                                </a>
                            </div>
                            <div class="dropdown-menu dropdown-alt" x-show="show" x-transition  @click.outside="show = false">
                                <a class="dropdown-item" x-on:click.prevent="$dispatch('showSweetAlertQuestion', { 
                                    title: 'Eliminar noticia',
                                    text:'¿Quieres eliminar está noticia?',
                                    type:'warning',
                                    actionEmit:'deleteNews',
                                    val: {{ $noticia->id }},
                                    });"  href="javascript:void(0);">Eliminar</a>
                                <span class="text-muted px-2">|</span>
                                <a class="dropdown-item px-3" href="{{ route('admin.news.edit', $noticia->id) }}"  class="btn-text-alt">Editar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="col-12">
                {{ $noticias->links() }}
            </div>   
        </div>
        @else
        <div class="row">
            <div class="col-12">
                <p class="my-5 text-muted text-center">No hay resultados.</p>
            </div>
        </div>
        @endif
    </section>
</div>
