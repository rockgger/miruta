<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label class="form-label">Nombre de este grupo</label>
            <input type="text" class="form-control form-control-alt " wire:model.defer="name_group"  placeholder="Agrega un nombre para este grupo de etiquetas" >
            @error('name_group')
                <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <div class="col-12 text-center mb-3">
        <button class="btn btn-primary text-uppercase" wire:click="save" wire:loading.remove>Crear grupo</button> 
        <button type="button" disabled class="btn-login btn btn-primary width-lg" wire:loading wire:target="save"><i
                    class="mdi mdi-spin mdi-loading"></i></button>
    </div>
</div>
