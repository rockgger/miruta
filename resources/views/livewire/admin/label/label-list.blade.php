<div class="row">

    @if($loading)
        <div class="col-12">
            <div class="card-loading" >
                <div class="spinner-border avatar-md text-contrast m-2" ></div>
            </div>
        </div>
    @else
    <div class="col-md-4">
        <div class="card-label d-flex text-blak-gray align-items-center justify-content-between">
            <div class="d-flex align-items-center">
                <div class="icon-label">
                    <i class="rn-category"></i>
                </div>                    
                <div class="d-flex flex-column align-items-start">
                    <span class="title-label">Perfiles</span>
                    <span class="desct-label">{{ $profile }} etiquetas</span>
                </div>
            </div>
            <a href="{{ route('admin.label.profile.edit') }}" title="Editar grupo" class="btn-more-vertial">
                <i class="fe-more-vertical"></i>
            </a>
        </div>
    </div>
        @foreach ( $labels as $label )
            <div class="col-md-4">
                <div class="card-label d-flex text-blak-gray align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <div class="icon-label">
                            <i class="rn-category"></i>
                        </div>                    
                        <div class="d-flex flex-column align-items-start">
                            <span class="title-label">{{ $label['name'] }}</span>
                            <span class="desct-label">{{ $label['labels_count'] }} etiquetas</span>
                        </div>
                    </div>
                    <a href="{{ route('admin.label.edit', $label['id']) }}" title="Editar grupo" class="btn-more-vertial">
                        <i class="fe-more-vertical"></i>
                    </a>
                </div>
            </div>
        @endforeach
    @endif

    @push("scripts")
    <script>
        //cargar servicios dle usuario
        document.addEventListener('livewire:load', function () {
            Livewire.emit('loadLabels');
        })
        
    </script>
    @endpush
</div>
