<div>
    <div class="modal-body px-md-5 "> 
        <div class="row">
            <div class="col-md-12 "> 
                @if (session()->has('message.success'))
                    <div class="alert alert-success alert-dismissible msg fadeIn animated" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        {{ session('message.success') }}
                    </div>
                @endif
            </div>
            <div class="col-md-12 "> 
                <div class="form-group mb-2 selectize-h-auto">
                    <div  wire:ignore x-data="{ notify:@entangle('add_users').defer}" 
                        x-init='$nextTick(()=>{$(".selectize-close-btn").selectize({
                                plugins:["remove_button"],
                                persist:!1,
                                create:!0,
                                onItemRemove:function(e){
                                    notify.splice( notify.indexOf(e), 1 );
                                },
                                onItemAdd:function(value, $item){
                                    notify.push(value);
                                }
                            });})'>
                        <input type="text" class="form-control selectize-close-btn " placeholder="Escribe la dirección de E-mail de los usuarios a invitar" >
                    </div>
                    
                    <small class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit.</small>
                    @error('add_users')
                        <span class="text-danger w-100 d-block ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer mb-4  flex-column align-items-center justify-content-end p-0">
        <button wire:click="inviteUsers" wire:loading.remove wire:target="inviteUsers" type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">Invitar</button>
        
        <div class="col-12" wire:loading wire:target="inviteUsers">
            <div class="card-loading" style="min-height: auto;">
                <div class="spinner-border avatar-md text-contrast m-0 p-0" ></div>
            </div>
        </div>
    </div>
</div>
