<div>
    <div class="row" x-data="{}">
        <div class="col-md-8">
            <h4 class="text-uppercase">{{ number_format($users_total) }} usuarios registrados</h4>
            <div class="d-flex justify-content-between align-items-center flex-wrap">
            @foreach ($profiles_total as $prf)
                <span class="d-flex align-items-center"><i class="p-05 bg-warning me-1 h-auto rounded d-inline-flex"></i><div><b>{{ $prf['count'] }}</b> {{ $prf['name'] }}</div></span>
            @endforeach
            </div>
        </div>
        <div class="col-md-4 d-flex justify-content-end">
            <a href="javascript:void(0);" class="btn btn-custom text-uppercase"  data-bs-toggle="modal" data-bs-target="#addUsers">Invitar usuarios</a>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group mb-1">
                <input type="search" wire:model.debounce.400ms="search" class="form-control input-profile " placeholder="Buscar" >
                @error('search')
                    <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-center justify-content-end pb-1">
            <label class="label-rn me-2">Perfil: </label>        
            <div class="form-group mb-0">
                <select class="select-rutan"  wire:model="profile_id">
                    <option value="0" selected >Todos</option>
                    @foreach ($profiles as $profile)
                        <option value="{{ $profile['id'] }}">{{ $profile['name'] }}</option>
                    @endforeach
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-4 d-flex justify-content-end align-items-center pb-1">
            <label class="label-rn me-2">Orden: </label>        
            <div class="form-group mb-0">
                <select class="select-rutan" wire:model="order" >
                    <option value="DESC"  selected >Más nuevo</option>
                    <option value="ASC"  selected >Más antiguos</option>
                </select>
                <div class="clearfix"></div>
            </div>
        </div>
    
    </div>

    <div class="row">
        @if($users->total() > 0)
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table mt-4 pb-4 table-users">
                    <thead>
                        <tr>
                            <th class="py-1"><h5 class="text-uppercase m-0">Nombre</h5></th>
                            <th class="py-1"><h5 class="text-uppercase m-0">Perfil</h5></th>
                            <th class="py-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>                            
                            <td>{{ $user->name }} {{ $user->last_name }}</td>
                            <td>{{ $user->profile }}</td>
                            <td class="col-2">
                                <div class="dropstart" x-data="{show:false}">
                                    <div class="d-flex align-items-center justify-content-end">   
                                        <a href="javascript:void(0);" x-on:click="show = ! show"  class="btn-more-vertial-2" >
                                            <i class="fe-more-vertical" x-show="!show" ></i>
                                            <i class="fe-minus" x-show="show" x-transition></i>
                                        </a>
                                    </div>
                                    <div class="dropdown-menu dropdown-alt" x-show="show" x-transition  @click.outside="show = false">
                                        <a class="dropdown-item" x-on:click.prevent=""  href="javascript:void(0);">Eliminar</a>
                                        <span class="text-muted px-2">|</span>
                                        <a class="dropdown-item px-3" href="#"  class="btn-text-alt">Detalles</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-12 mt-3 d-flex justify-content-center">
            {{ $users->onEachSide(5)->links() }}        
        </div>
        @else
            <div class="col-12 text-center">
                <p class="text-center mt-5 text-muted">No hay resultados.</p>
            </div>
        @endif
    </div>


    <x-admin.modal.invite-users :idmodal="'addUsers'"  >
        <x-slot name="title">
            Invitar usuarios
        </x-slot>

        {{-- <x-slot name="btnEvents">
            <button wire:click="inviteUsers" wire:loading.remove wire:target="inviteUsers" type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">Invitar</button>
        </x-slot> --}}

    </x-admin.modal.invite-users>
    


    
</div>
