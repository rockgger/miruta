<div>
    
    <section class="mt-2">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="search" wire:model.debounce.400ms="search" class="form-control input-profile " placeholder="Buscar" >
                    @error('search')
                        <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-md-4">
                {{-- <div class="row">
                    <div class="col-sm-4">
                        <label class="label-rn">Filtrar por: </label>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <select class="select-rutan" >
                                <option class="text-muted"  selected >Seleccionar opción</option>
                            </select>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div> --}}
            </div>

            <div class="col-md-4 text-center">
                <button class="btn btn-custom  text-uppercase" wire:click="restData" data-bs-toggle="modal" data-bs-target="#idcreatelabel" >NUEVO COMPAÑERO</button>
            </div>
        </div>
    </section>


    <section class="">
        <p class="text-muted">Mostrando {{ $teams->total() }} resultados</p>

        <div class="row">
        @if($teams->total() > 0)
            @foreach ($teams as $team)
            <div class="col-md-4">
                <div class="card-label text-truncate">
                    <div class="d-flex text-blak-gray align-items-center justify-content-between text-truncate">
                        <div class="d-flex align-items-center text-truncate">
                            <div class="icon-label overflow-hidden">
                                <img src="{{ $team->photo }}" alt="">
                            </div>                    
                            <div class="d-flex flex-column align-items-start text-truncate">
                                <span class="title-label text-capitalize">{{ $team->name }}</span>
                                <span class="desct-label text-truncate d-block ">{{ $team->role }}</span>
                            </div>
                        </div>
                        <div class="dropstart" x-data="{show:false}">
                            <a href="javascript:void(0);" x-on:click="show = ! show"  class="btn-more-vertial " >
                                <i class="fe-more-vertical" x-show="!show" x-transition></i>
                                <i class="fe-minus" x-show="show" x-transition></i>
                            </a>
                            <div class="dropdown-menu dropdown-alt" x-show="show" x-transition  @click.outside="show = false">
                                <a class="dropdown-item" wire:click="delete({{ $team }})"  href="javascript:void(0);">Eliminar</a>
                                <span class="text-muted px-2">|</span>
                                <a class="dropdown-item px-3" href="javascript:void(0);" wire:click="edit({{ $team }})"  class="btn-text-alt" data-bs-toggle="modal" data-bs-target="#idupdatelabel">Editar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="col-12 d-flex justify-content-center mt-4">
                {{ $teams->links() }}
            </div>
        @else
            <div class="col-12 text-center">
                <p class="text-muted text-center">Sin resultados.</p>
            </div>
        @endif
        </div>
    </section>



    <x-admin.modal.create-team :idmodal="'idcreatelabel'"  :photo="$photo">
        <x-slot name="title">
            NUEVO COMPAÑERO
        </x-slot>

        <x-slot name="btnEvents">
            <button wire:click="store" wire:loading.remove wire:target="store, update" type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">crear</button>
        </x-slot>
    </x-admin.modal.create-team>

    <x-admin.modal.create-team :idmodal="'idupdatelabel'"  :photo="$photo" :phototemp="$photo_temp">
        <x-slot name="title">
            editar COMPAÑERO
        </x-slot>

        <x-slot name="btnEvents">
            <button wire:click="update" wire:loading.remove wire:target="store, update" type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">Guardar</button>
        </x-slot>
    </x-admin.modal.create-team>
</div>
