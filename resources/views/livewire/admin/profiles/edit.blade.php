<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label class="form-label">Nombre de este grupo</label>
            <input type="text" class="form-control form-control-alt" disabled="disabled" value="Perfiles"  placeholder="Agrega un nombre para este grupo de etiquetas" >
        </div>
    </div>

    <div class="col-12">
        <section class="d-flex align-items-center justify-content-between" x-data="{}">
            <span class="text-muted">{{ number_format($profiles->total()) }} etiquetas en total</span>
            <a href="javascript:void(0);" x-on:click="$wire.restData();" class="btn-text-alt" data-bs-toggle="modal" data-bs-target="#idcreatelabel">
                <i class="fe-plus"></i>
                Agregar etiqueta
            </a>
        </section>
    </div>

    <div class="col-12">
    @if($profiles->total() > 0)
        <div class="table-responsive pb-4">
            <table class="table mb-0 mt-3 table-rn ">
                <thead>
                    <tr>
                        <th>Español</th>
                        <th>inglés</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                 @foreach ($profiles as $profile) 
                    <tr>
                        <td>{{ $profile->name_es }}</td>
                        <td>{{ $profile->name_en }}</td>
                        <td style="width: 50px;" class="p-0 dropstart" x-data="{show:false}">
                            <a href="javascript:void(0);" x-on:click="show = ! show"  class="btn-more-vertial " >
                                <i class="fe-more-vertical" x-show="!show" x-transition></i>
                                <i class="fe-minus" x-show="show" x-transition></i>
                            </a>
                            <div class="dropdown-menu dropdown-alt" x-show="show" x-transition  @click.outside="show = false">
                                <a class="dropdown-item" wire:click="deleted({{ $profile }});" href="javascript:void(0);">Eliminar</a>
                                <span class="text-muted px-2">|</span>
                                <a class="dropdown-item px-3" href="javascript:void(0);" wire:click="editProfile({{ $profile }})"  class="btn-text-alt" data-bs-toggle="modal" data-bs-target="#ideditlabel">Editar</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row" wire:loading.remove>
            <div class="col-12 d-flex justify-content-center mt-4">
            {{ $profiles->links() }}
            </div>
        </div>
    @else
        <div class="my-5 text-center" wire:loading.remove>
            <span class="text-muted ">Sin resultados.</span>
        </div>
    @endif
    </div>

    <x-admin.modal.create-label :idlabel="'idcreatelabel'"  >
        <x-slot name="title">
            nueva Etiqueta
        </x-slot>

        <x-slot name="btnEvents">
            <a href="javascript:void(0);" wire:loading.remove wire:click="store(true)" class="btn-text-alt mb-3" >
                <i class="fe-plus"></i>
                Crear y agregar otro
            </a>
            <button wire:click="store" wire:loading.remove type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">crear</button>
        </x-slot>
    </x-admin.modal.create-label>


    <x-admin.modal.create-label :idlabel="'ideditlabel'"  >
        <x-slot name="title">
            Editar Etiqueta
        </x-slot>

        <x-slot name="btnEvents">
            <button wire:click="update" wire:loading.remove type="button" class="btn btn-primary  text-uppercase text-brandon waves-effect">Guardar</button>
        </x-slot>
    </x-admin.modal.create-label>
    
</div>
