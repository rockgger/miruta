<div>
    <form>
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('message.error'))
                    <div class="alert alert-danger msg fadeIn animated" role="alert">
                        {{ session('message.error') }}
                    </div>
                @endif
                <div class="form-group">
                    <input type="text" class="form-control " wire:model.defer="name" name="name" placeholder="Nombres" wire:keydown.enter="register" wire:loading.attr="disabled" wire:target="register">
                    @error('name')
                        <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" class="form-control " wire:model.defer="last_name" name="last_name"
                        placeholder="Apellidos"  wire:keydown.enter="register" wire:loading.attr="disabled" wire:target="register">
                    @error('last_name')
                        <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="email" class="form-control " wire:model.defer="email" name="email"
                        placeholder="Correo electrónico"  wire:keydown.enter="register" wire:loading.attr="disabled" wire:target="register">
                    @error('email')
                        <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group position-relative"  x-data="{show:false, icon:false}">
                    <input type="password" class="form-control  password" x-ref="inpassword" wire:model.defer="password" name="password"
                        placeholder="Contraseña" wire:ignore wire:keydown.enter="register" wire:loading.attr="disabled" wire:target="register">
                    <button type="button" class="btn-input" 
                        x-on:click="icon = !icon,  show = !show, $refs.inpassword.setAttribute('type', changeTypePass(show))">
                        <template x-if="icon">
                            <i class="fe-eye-off text-secondary"></i>
                        </template>

                        <template x-if="!icon">
                            <i class="fe-eye text-secondary"></i>
                        </template>
                    </button>
                    @error('password')
                        <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <p class="label-rn">Selecciona el perfil con el cual te identificas mejor</p>
                    </div>
                    <div class="col-md-6" x-data="{disa:false}">
                        <select class="select-rutan" name="profile"  wire:model.defer="profile_id" x-on:click="disa = true;">
                            <option class="text-muted" :disabled="disa" selected >Selecciona un perfil</option>
                            @foreach ($profiles as $profile)
                                <option value="{{ $profile['id'] }}">{{ $profile['name'] }}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        @error('profile_id')
                            <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

            </div>
            <div class="col-md-12  mt-4"> 
                <button type="button"
                    class="btn-login btn  btn-primary  waves-effect waves-light text-uppercase" wire:click="register" wire:loading.remove
                wire:target="register">REGÍSTRARME</button>

                <button type="button" disabled class="btn-login btn btn-primary width-lg" wire:loading wire:target="register"><i
                    class="mdi mdi-spin mdi-loading"></i></button>
            </div>
            <div class="col-md-12 ">
                <p class="mt-4">¿Ya tienes una cuenta?<a href="{{ route('auth.login') }}" ><strong>
                        Ingresar.</strong></a></p>
            </div>
        </div>
    </form>

    @push('scripts')
    <script>
        function changeTypePass(show){
            return show ? 'text':'password';
        }
    </script>
    @endpush
</div>
