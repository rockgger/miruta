<div x-data="{recover:false}">
    <section x-show="!recover">
        <p class="text-apoyo text-uppercase text-primary text-brandon">Ingresa a tu cuenta</p>
        <form >
            <div class="row">
                <div class="col-md-12">
                    @if (session()->has('message.error'))
                        <div class="alert alert-danger msg fadeIn animated" role="alert">
                            {{ session('message.error') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="email" class="form-control" wire:model.defer="email" name="email" id="email" placeholder="Correo electrónico" wire:keydown.enter="login" wire:loading.attr="disabled" wire:target="login">
                        @error('email')
                            <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" wire:model.defer="password" name="password"  id="password" placeholder="Contraseña" wire:keydown.enter="login" wire:loading.attr="disabled" wire:target="login">
                        @error('password')
                            <span class="text-danger w-100 d-block position-lg-absolute">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <div class="col-md-12 ">
                    <button type="button"
                        class="btn-login btn btn-primary  waves-effect waves-light text-uppercase" wire:click="login" wire:loading.remove
                wire:target="login">INGRESAR</button>

                    <button type="button" disabled class="btn-login btn btn-primary width-lg" wire:loading wire:target="login"><i
                    class="mdi mdi-spin mdi-loading"></i></button>
                </div>
                <div class="col-md-12 ">
                    <p class="mt-4"><a href="" x-on:click.prevent="recover = true">¿Olvidaste tu contraseña?</a></p>
                </div>

            </div>
        </form>
    </section>

    <section x-show="recover" style="display: none;" >
        <p class="text-apoyo text-uppercase text-primary text-brandon">Recuperar contraseña</p>
        <form>
            <div class="row">                            
                <div class="col-md-12">
                    @if (session()->has('message.recover.error'))
                        <div class="alert alert-danger msg fadeIn animated" role="alert">
                            {{ session('message.error') }}
                        </div>
                    @endif
                    <div class="form-group">                                    
                        <input type="email" class="form-control"  placeholder="Ingresa tu correo electrónico" >
                        @error('email_change')
                            <span class="text-danger w-100 d-block position-absolute">{{ $message }}</span>
                        @enderror
                    </div>
                                                    
                </div>
                <div class="col-md-12 text-center">
                    <button type="button" class="btn-login btn btn-primary  waves-effect waves-light  text-uppercase" >Recuperar contraseña</button>
                    <p class="text-center mt-3"><a href="" x-on:click.prevent="recover = false" class="">Volver</a></p> 
                </div>
            </div>
        </form>
    </section>
</div>
