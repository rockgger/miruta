<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Ruta N Digital </title>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('image/icon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,700i,900&display=swap" rel="stylesheet"> 
    <style>

        @font-face {
            font-family: 'Brandon Grotesque';
            src: url('http://199.89.53.76/fonts/BrandonGrotesque-Bold.woff2') format('woff2'), url('http://199.89.53.76/fonts/BrandonGrotesque-Bold.woff') format('woff');
            font-weight: bold;
            font-style: normal;
        }
    </style>
</head>

<body style="
    margin: 0; 
    padding: 0; 
    background-color: #d5f1ed; 
    padding-bottom: 0px; 
    font-family: 'Source Sans Pro', sans-serif; 
    font-size: .875rem;
    font-weight: 400;
    line-height: 1.5;
    color: #6c757d;
    text-align: left;" >
 
    <table border="0" cellpadding="0" cellspacing="0" width="600" style="
    border-collapse: collapse; 
    max-width: 600px;
    width: 100%;
    border-radius: 0;
    margin: 0 auto;
    margin-top: 4.5rem!important;
    margin-bottom: 1.5rem!important;
    box-shadow: 0 0.75rem 6rem rgba(56, 65, 74, .03);
    position: relative;    
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;    
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid #f7f7f7;

    "
    
    >

        <tr>

            <td align="center"  style="padding: 25px 0 30px 0;">
    
                <img src="{{asset('image/logo-rutan.png')}}" alt="Ruta n"  height="58" style="display: block;" />
            
            </td>

        </tr>

        <tr>

            <td align="left" style="padding-left: 63px; padding-right: 63px;">

                <p style="font-size: 18px;
                    font-weight: normal;
                    font-stretch: normal;
                    font-style: normal;
                    line-height: normal;
                    letter-spacing: normal;
                    color: #403f41;
                    margin-bottom: 0!important;
                    font-family: 'Source Sans Pro', sans-serif;"
                    >Hola</p>

            </td>

        </tr>

        <tr >

            <td align="left" style="padding-left: 63px; padding-right: 63px;">

                <p style="font-size: 18px;
                    font-weight: normal;
                    font-stretch: normal;
                    font-style: normal;
                    line-height: normal;
                    letter-spacing: normal;
                    color: #403f41;
                    font-family: 'Source Sans Pro', sans-serif;"
                    >Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>

            </td>

        </tr>

        <tr>

            <td align="center" style="padding-left: 63px; padding-right: 63px; padding-top: .75rem!important; padding-bottom: 1.5rem !important; text-align: center!important;">

                <a href="{{ $url }}" style="
                color: #fff;
                cursor: pointer;
                box-shadow: none;
                text-decoration: none;
                border-radius: 3px;
                font-size: 14px;
                font-weight: 900;
                font-stretch: normal;
                font-style: normal;
                line-height: 28px;
                letter-spacing: 0.5px;
                text-align: center;
                padding: .65rem 0.9rem;
                min-width: 163px;                
                font-family: 'Source Sans Pro', sans-serif;
                background-color: #2db2a0 !important;
                border-color: #2db2a0 !important;
                text-transform: uppercase!important;
                margin-top: 1.75rem!important;
                white-space: nowrap;
                vertical-align: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                border: 1px solid transparent;
                display: inline-block;
                ">Aceptar invitación</a>

            </td>

        </tr>

        <tr style="background-color: #3e3e3e; ">

            <td >

                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 15px 20px;">
    
                    <tr style="">
                    
                        <td  valign="top" width="10" style="padding: 0px 5px;" >                        
                            <a href="#"><img src="{{asset('image/tigo.png')}}" width="100%" alt=""></a>
                        </td>

                        <td  valign="top"  width="10" style="padding: 0px 5px;" >                        
                            <a href="#"><img src="{{asset('image/epm.png')}}"width="100%" alt=""></a>
                        </td>

                        <td  valign="top" width="5" style="padding: 0px 5px;" >                        
                            <a href="#"><img src="{{asset('image/alcaldia.png')}}" width="100%" alt=""></a>
                        </td>
                    
                    <td style="font-size: 0; line-height: 0;" width="60">
                    
                    &nbsp;
                    
                    </td>
                    
                        <td  valign="top" width="5" style="padding: 0px 5px;" >                        
                            <a href="#"><img src="{{asset('image/icons/icon-inst.png')}}" width="100%" alt=""></a>
                        </td>

                        <td  valign="top"  width="5" style="padding: 0px 5px;" >                        
                            <a href="#"><img src="{{asset('image/icons/icon-tw.png')}}"width="100%" alt=""></a>
                        </td>

                        <td  valign="top" width="5" style="padding: 0px 5px;" >                        
                            <a href="#"><img src="{{asset('image/icons/icon-in.png')}}" width="100%" alt=""></a>
                        </td>
                    
                    </tr>
                
                </table>

            </td>

        </tr>

    </table>

    <table border="0" cellpadding="0" cellspacing="0" width="600" style="           
            max-width: 600px;
            width: 100%;            
            margin: 0 auto;            
            position: relative;    
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;    
            min-width: 0;
            word-wrap: break-word;
            text-align: center;
        ">

        <tr>

            <td align="center"  colspan="5" style="padding-bottom: 1.0rem !important;">
    
               <a href="#" style="font-size: 16px;
                font-weight: bold;
                line-height: 1.27;
                letter-spacing: 1.5px;
                font-family: 'Source Sans Pro', sans-serif;
                font-stretch: normal;
                font-style: normal;
                color: #3e3e3e;
                text-decoration: none;
                background-color: transparent;
                "
                >www.rutan.co</a>
            
            </td>

        </tr>

        <tr>

            <td align="center"  style="">
    
               <a href="#" style="
                font-stretch: normal;
                font-style: normal;
                font-size: 15px;
                font-weight: normal;
                color: #3e3e3e;
                line-height: normal;
                letter-spacing: normal;
                text-decoration: none;
                background-color: transparent;
                font-family: 'Source Sans Pro', sans-serif;
                "
                >Tratamiento de datos</a>
            
            </td>

            <td align="center"  style="">
    
               <a href="#" style="
                font-stretch: normal;
                font-style: normal;
                font-size: 15px;
                font-weight: normal;
                color: #3e3e3e;
                line-height: normal;
                letter-spacing: normal;
                text-decoration: none;
                background-color: transparent;
                font-family: 'Source Sans Pro', sans-serif;
                "
                >Política de privacidad</a>
            
            </td>

            <td align="center"  style="">
    
               <a href="#" style="
                font-stretch: normal;
                font-style: normal;
                font-size: 15px;
                font-weight: normal;
                color: #3e3e3e;
                line-height: normal;
                letter-spacing: normal;
                text-decoration: none;
                background-color: transparent;
                font-family: 'Source Sans Pro', sans-serif;
                "
                >Habeas data</a>
            
            </td>

            <td align="center"  style="">
    
               <a href="#" style="
                font-stretch: normal;
                font-style: normal;
                font-size: 15px;
                font-weight: normal;
                color: #3e3e3e;
                line-height: normal;
                letter-spacing: normal;
                text-decoration: none;
                background-color: transparent;
                font-family: 'Source Sans Pro', sans-serif;
                "
                >Contacto</a>
            
            </td>

            <td align="center"  style="">
    
               <a href="#" style="
                font-stretch: normal;
                font-style: normal;
                font-size: 15px;
                font-weight: normal;
                color: #3e3e3e;
                line-height: normal;
                letter-spacing: normal;
                text-decoration: none;
                background-color: transparent;
                font-family: 'Source Sans Pro', sans-serif;
                "
                >FAQs</a>
            
            </td>

        </tr>

    </table>
 
</body>
 
</html>